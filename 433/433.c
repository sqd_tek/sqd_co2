#include "stm32f10x.h"
#include "stdio.h"
#include "stdlib.h"
#include "Delay.h"
#include "433.h"
#include "string.h"

uint8_t Set_Command[6]={0xc0,0x00,0x00,0x00,0x00,0x00};
extern u16 CO2_Top;//co2报警上限值	
extern int cal_crc_false(unsigned char * dat, int count);
extern u16 con_top;
void Freq_433_init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOE, ENABLE);
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8;	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOE, &GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOE, &GPIO_InitStructure);
}

void Freq_433_Mode_Config(void) 
{
		Command_Set;							//串口设置模式
		//while(GPIO_ReadInputDataBit(GPIOE, GPIO_Pin_8));
		Delay_ms(2);
	  USART_SendData(UART5, Set_Command[0]);
		//while(USART_GetFlagStatus(UART5,USART_FLAG_TC)==RESET);
		USART_SendData(UART5, ADDH);
		//while(USART_GetFlagStatus(UART5,USART_FLAG_TC)==RESET);
	  USART_SendData(UART5, ADDL);
		//while(USART_GetFlagStatus(UART5,USART_FLAG_TC)==RESET);
	  USART_SendData(UART5, SPEED_UART9600|SPEED__AIR1K);
		//while(USART_GetFlagStatus(UART5,USART_FLAG_TC)==RESET);
	  USART_SendData(UART5, Fre_433);
		//while(USART_GetFlagStatus(UART5,USART_FLAG_TC)==RESET);
	  USART_SendData(UART5, PWR_SEND_P20dB);
	  //while(USART_GetFlagStatus(UART5,USART_FLAG_TC)==RESET);
		Wireless_Set;
	  Delay_ms(30);						//等待参数配置完成
}

void Freq_433_Send_data(u16 id,char *Cmd) //发送结束标志，给从机阀门信息
{
	uint8_t i,len;
	u8 Start_Send[10];
	u16 con_top_temp;
	typedef union work
	{
			u8 ch[2];
			u16 id_t;
	}data;
	data center;
	center.id_t  = id;

	//len = strlen(Cmd);
	for(i=0;i<4;i++)
	{
		Start_Send[i] = Cmd[i];
	}
//	Start_Send[0] = 'S';
//	Start_Send[1] = 'E';
//	Start_Send[2] = 'N';
//	Start_Send[3] = 'D';
	Start_Send[4] = center.ch[0];
	Start_Send[5] = center.ch[1];
	
	if(con_top >500 && (!(con_top>600)))
	{
		con_top = con_top * 10/8;
		
	}
	else if(con_top >600 && (!(con_top>800)))
	{
		con_top = con_top * 10/7;
		
	}
	center.id_t  = con_top;
	
	Start_Send[6] = center.ch[0];
	Start_Send[7] = center.ch[1];
	
//	if(id ==2 || id == 6 || id == 8)
//	{
//		con_top_temp =con_top - 150;
//		center.id_t  = con_top_temp;
//		Start_Send[6] = center.ch[0];
//		Start_Send[7] = center.ch[1];
//		
//	}
//		if(id == 6)
//	{
//		con_top_temp =con_top - 200;
//		center.id_t  = con_top_temp;
//		Start_Send[6] = center.ch[0];
//		Start_Send[7] = center.ch[1];
//		
//	}
	center.id_t =cal_crc_false(Start_Send,8);
	Start_Send[8] = center.ch[0];
	Start_Send[9] = center.ch[1];
	//unsigned int len = strlen(p);
	

	for(i=0;i<10;i++)
	{
		USART_SendData(UART5,Start_Send[i]);
		while(USART_GetFlagStatus(UART5,USART_FLAG_TC)==RESET);
		//Delay_us(1);
	}
}













