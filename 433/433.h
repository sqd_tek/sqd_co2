#include "stm32f10x.h"
#include "stdio.h"
#include "stdlib.h"


#define SPEED_UART4800 0x10
#define SPEED_UART9600 0x18

#define SPEED__AIR1K  0x00
#define SPEED__AIR2K  0x01
#define SPEED__AIR4K  0x02

#define ADDH  0x12
#define ADDL  0x34

#define Fre_433	0x50

#define PWR_SEND_P20dB 0x7f
#define PWR_SEND_0dB 0x3f
#define PWR_SEND_N20dB 0x00

#define Wireless_Set  GPIO_SetBits(GPIOE,GPIO_Pin_7)
#define Command_Set   GPIO_ResetBits(GPIOE,GPIO_Pin_7)


void Freq_433_init(void);
void Freq_433_Mode_Config(void);
void Freq_433_Send_data(u16 id,char *Cmd);


