#include "stm32f10x.h"
#include "stdio.h"
#include "stdlib.h"

#ifndef __AM2302_H
#define __AM2302_H


#define RCC_AM2301				RCC_APB2Periph_GPIOD
#define GPIO_AM2301_PORT		GPIOD  
#define GPIO_AM2301_PIN			GPIO_Pin_9

#define AM2301_Write_1()     GPIO_SetBits(GPIO_AM2301_PORT ,GPIO_AM2301_PIN)  //д1
#define AM2301_Write_0()     GPIO_ResetBits(GPIO_AM2301_PORT ,GPIO_AM2301_PIN)//д0
#define AM2301_ReadBit()     GPIO_ReadInputDataBit(GPIO_AM2301_PORT ,GPIO_AM2301_PIN) //��DQ��


//extern void Delay_us(__IO u32 nTime);
extern void UART2_SendStr(const uint8_t *str);
extern void UART2_Send_string( uint8_t *Data,uint8_t  Len);//����2���code�ַ���
void AM2301_Port_Config(void);
uint32_t Get_AM2301_Data(void);
extern uint16_t Tem_Past,RH_Past,Tem,RH;

#endif
