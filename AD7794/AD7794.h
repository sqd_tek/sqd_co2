/********************************************************************************
 Author        : LUO LITONG 

 Date          : 2014-9-23

 Hardware      : STM32F103+AD7794	
********************************************************************************/

#ifndef AD7794_H
#define AD7794_H

//----------------------
//Command and Register Map
//----------------------
#define COMMUNICATIONS	0x00
#define STATUS			0x08	// 	 binary:1000, the last 3 bit (000) is same as COMMUNICATIONS register
#define MODE			0x01
#define CONFIGURATION	0x02
#define DATA			0x03
#define ID				0x04
#define IO				0x05
#define OFFSET			0x06
#define FULLSCALE		0x07

//COMMUNICATIONS register
#define WRITE_DISABLE 		0x80
#define WRITE_ENABLE 		0x00

#define READ				0x40
#define WRITE				0x00

#define RS_COMMUNICATIONS	0x00
#define RS_STATUS			0x00			
#define RS_MODE				0x08
#define RS_CONFIGURATION	0x10
#define RS_DATA				0x18
#define RS_ID				0x20
#define RS_IO				0x28
#define RS_OFFSET			0x30
#define RS_FULLSCALE		0x38

#define CREAD_ENABLE		0x04
#define CREAD_DISABLE		0x00
	
//STATUS register	
#define DATA_RDY	0x80
#define ERR			0x40
#define NOXREF		0x20
#define SR4			0x10	// 0, fix
#define SR3			0x08	// 0 for AD7795, 1 for AD7794, fix
#define CH_AIN1	0x0000
#define CH_AIN2	0x0001
#define CH_AIN3	0x0002
#define CH_AIN4	0x0003
#define CH_AIN5	0x0004
#define CH_AIN6	0x0005
#define CH_TEMP	0x0006

		
//MODE register		
#define MD_CONTINUOUS_CONVERSION	0x0000
#define MD_SINGLE_CONVERSION		0x2000
#define MD_IDLE						0x4000
#define MD_POWERDOWN				0x6000
#define MD_INTERNAL_ZS_CALIBRATION	0x8000
#define MD_INTERNAL_FS_CALIBRATION	0xA000
#define MD_SYSTEM_ZS_CALIBRATION	0xC000
#define MD_SYSTEM_FS_CALIBRATION	0xE000

#define PSW_ENABLE	0x1000
#define PSW_DISABLE	0x0000

#define AMP_CM_ENABLE	0x0200
#define AMP_CM_DISABLE	0x0000

#define CLK_INTERNAL_OUTPUT_DISABLE	0x0000
#define CLK_INTERNAL_OUTPUT_ENABLE	0x0040
#define CLK_EXTERNAL_DIVIDE_DISABLE	0x0080
#define CLK_EXTERNAL_DIVIDE_ENABLE	0x00C0

#define CHOP_DISABLE	0x0000
#define CHOP_ENABLE		0x0010

#define FILTER_UPDATE_RATE_470	0x01
#define FILTER_UPDATE_RATE_242	0x02
#define FILTER_UPDATE_RATE_123	0x03
#define FILTER_UPDATE_RATE_62	0x04
#define FILTER_UPDATE_RATE_50	0x05
#define FILTER_UPDATE_RATE_39	0x06
#define FILTER_UPDATE_RATE_33	0x07
#define FILTER_60_UPDATE_RATE_19	0x08
#define FILTER_50_UPDATE_RATE_16	0x09
#define FILTER_6050_UPDATE_RATE_16	0x0A
#define FILTER_6050_UPDATE_RATE_12	0x0B
#define FILTER_6050_UPDATE_RATE_10	0x0C
#define FILTER_6050_UPDATE_RATE_8	0x0D
#define FILTER_6050_UPDATE_RATE_6	0x0E
#define FILTER_6050_UPDATE_RATE_4	0x0F

//CONFIGURATION register
#define VBIAS_DISABLE	0x0000
#define VBIAS_AIN1N		0x4000
#define VBIAS_AIN2N		0x8000
#define VBIAS_AIN3N		0xC000

#define BO_ENABLE		0x2000
#define BO_DISABLE		0x0000

#define UNIPOLAR		0x1000
#define BIPOLAR			0x0000

#define BOOST_ENABLE	0x0800
#define BOOST_DISABLE	0x0000

#define GAIN_1		0x0000
#define GAIN_2		0x0100
#define GAIN_4		0x0200
#define GAIN_8		0x0300
#define GAIN_16		0x0400
#define GAIN_32		0x0500
#define GAIN_64		0x0600
#define GAIN_128	0x0700

#define REFSEL_EXTERNAL_REF1	0x0000
#define REFSEL_EXTERNAL_REF2	0x0040
#define REFSEL_INTERNAL			0x0080

#define REF_DETECT_ENABLE		0x0020
#define REF_DETECT_DIASBLE		0x0000

#define BUF_ENABLE		0x0010
#define BUF_DISABLE		0x0000

#define CHANNEL_AIN1	0x0000
#define CHANNEL_AIN2	0x0001
#define CHANNEL_AIN3	0x0002
#define CHANNEL_AIN4	0x0003
#define CHANNEL_AIN5	0x0004
#define CHANNEL_AIN6	0x0005
#define CHANNEL_TEMP	0x0006
#define CHANNEL_AVDD	0x0007
#define CHANNEL_AIN1N	0x0008
	
//DATA register		
#define DATA_MASK		0x0FFF
	
//ID register:	default 0xXF		
		
//IO register
#define IO_ENABLE	0x40
#define IO_DISABLE	0x00

#define IO2_DATA_SET	0x20
#define IO2_DATA_CLR	0x00
#define IO1_DATA_SET	0x10
#define IO1_DATA_CLR	0x00

#define IEXC_DIR_11_22	0x00
#define IEXC_DIR_12_21	0x40
#define IEXC_DIR_11_21	0x80
#define IEXC_DIR_12_22	0xC0

#define IEXC_DISABLE	0x00
#define IEXC_10UA		0x01
#define IEXC_210UA		0x02
#define IEXC_1MA		0x03
		
//OFFSET register	
		
//FULLSCALE register



/*MOSI*/
#define AD7794_MOSI_Set() GPIO_SetBits(GPIOB, GPIO_Pin_15)  
#define AD7794_MOSI_Reset() GPIO_ResetBits(GPIOB, GPIO_Pin_15) 

/*CS*/
#define AD7794_CS_Set() GPIO_SetBits(GPIOB, GPIO_Pin_12) 
#define AD7794_CS_Reset() GPIO_ResetBits(GPIOB, GPIO_Pin_12) 

/*SCLK*/
#define AD7794_SCLK_Set() GPIO_SetBits(GPIOB, GPIO_Pin_13) 
#define AD7794_SCLK_Reset() GPIO_ResetBits(GPIOB, GPIO_Pin_13) 

/*MISO*/
#define AD7794_MISO_Read() GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_14) 	

//----------------------
//Function declarations
//----------------------
void AD7794_DeInit(void);
void AD7794_Init(void);
void AD7794_WriteRegister(unsigned char RegisterAddress, unsigned int *RegisterBuffer);
void AD7794_ReadRegister(unsigned char RegisterAddress, unsigned int *RegisterBuffer);
void AD7794_ReadResultForSingleConversion(unsigned char DataIndex, unsigned int *DataBuffer);
void AD7794_ReadResultForContinuousConversion(unsigned char StartIndex, unsigned char NumberOfData, unsigned int *DataBuffer);
void AD7794_ReadResultForContinuousRead(unsigned char StartIndex, unsigned char NumberOfData, unsigned int *DataBuffer);
void AD7794_ExitContinuousRead(void);
void AD7794_SoftwareReset(void);
void AD7794_WaitBusy(void);
void AD7794_Channel_Configration(unsigned int channel, unsigned int polar, unsigned int gain, unsigned int mode );

void AD7794_ReadAllRegister(unsigned char Regindex, unsigned int *Regbuffer);
unsigned int AD7794_Calc(unsigned int code);


#endif
