/*
硬件连接：
        CS <---> PB12
      MOSI <---> PB15
      MISO <---> PB14
      SCLK <---> PB13
    STATUS <---> PB9(硬件需要添加的内容)
	  POWEREN<---> PC9
*/
//SPI的初始化过程
//主模式 全双工 CPOL=1 CPHA=1 8位数据帧格式 MSB在前 NSS模式软件模式
//采用模拟spi的方式操作
#include "stm32f10x.h"
#include "AD7794.h"

unsigned int AD7794Registers[9]; 	

static void RCC_Configration(void);
static void GPIO_Configration(void);
static void GPIO_DeConfigration(void);


void AD7794_DeInit(void)
{
    RCC_Configration();
    GPIO_DeConfigration();
}

void AD7794_Init(void)
{
    RCC_Configration();
    GPIO_Configration();
    
    AD7794_MOSI_Set();
    AD7794_CS_Set();
    AD7794_MISO_Read();
    AD7794_SCLK_Set();  
}

/*AD7794校准*/
void AD7794_Calibrate()
{
	
}

/*AD7794采集程序,更新速率16.7hz*/
void AD7794_Channel_Configration(unsigned int channel, unsigned int polar, unsigned int gain, unsigned int mode )
{
	AD7794Registers[COMMUNICATIONS]=WRITE_ENABLE|WRITE|RS_MODE|CREAD_DISABLE;
	AD7794_WriteRegister(COMMUNICATIONS, AD7794Registers);
	AD7794Registers[MODE]= FILTER_6050_UPDATE_RATE_16 | mode;
	AD7794_WriteRegister(MODE, AD7794Registers);

	AD7794Registers[COMMUNICATIONS]=WRITE_ENABLE|WRITE|RS_CONFIGURATION|CREAD_DISABLE;
	AD7794_WriteRegister(COMMUNICATIONS, AD7794Registers);						 
	AD7794Registers[CONFIGURATION]= polar | gain | channel | BUF_ENABLE;//;
	AD7794_WriteRegister(CONFIGURATION, AD7794Registers);
}

void AD7794_WriteRegister(unsigned char RegisterAddress, unsigned  int *RegisterBuffer)
{
    unsigned char i;
    unsigned  int temp;
    
    temp=RegisterBuffer[RegisterAddress];

	  AD7794_CS_Reset();
	  
    switch(RegisterAddress)
	{
		case COMMUNICATIONS:			 			 		
		case IO:
            for(i=0; i<8; i++)
			{
				if((temp&0x80)==0x80)	
					AD7794_MOSI_Set();
				else
					AD7794_MOSI_Reset();
				AD7794_SCLK_Reset();
				temp<<=1;
		
				AD7794_SCLK_Set();	
			}
			break;

		case MODE:					
		case CONFIGURATION:	
			for(i=0; i<16; i++)
			{
				if((temp&0x8000)==0x8000)	
					AD7794_MOSI_Set();
				else
					AD7794_MOSI_Reset();
				AD7794_SCLK_Reset();
				temp<<=1;
		
				AD7794_SCLK_Set();		
			}
			break;
			 			
		case OFFSET:			
		case FULLSCALE:
		    for(i=0; i<24; i++)
			{				
				if((temp&0x800000)==0x800000)	
					AD7794_MOSI_Set();
				else
					AD7794_MOSI_Reset();
				AD7794_SCLK_Reset();
				temp<<=1;
		
				AD7794_SCLK_Set();	
			}
			break;

		default:
			break;
	  }
	
	  AD7794_CS_Set();	
}

void AD7794_ReadRegister(unsigned char RegisterAddress, unsigned  int *RegisterBuffer)
{
	  unsigned char i;
	  unsigned  int temp;
	  
	  temp=0;

	  AD7794_CS_Reset();

	  switch(RegisterAddress)
	  {
		  case STATUS:			 			 		
		  case ID:
              for(i=0; i<8; i++)
			{
				temp<<=1;

				AD7794_SCLK_Reset();
				
				temp+=AD7794_MISO_Read();
		
				AD7794_SCLK_Set();	
			}
		  	  RegisterBuffer[RegisterAddress]=temp;
			  break;

		  case MODE:					
		  case CONFIGURATION:
		  	  
		      for(i=0; i<16; i++)
			{
				temp<<=1;

				AD7794_SCLK_Reset();
				
				temp+=AD7794_MISO_Read();
		
				AD7794_SCLK_Set();	
			}
			  RegisterBuffer[RegisterAddress]=temp;
			  break;
			 			
		  case OFFSET:			
		  case FULLSCALE:
		  case DATA:
		      for(i=0; i<24; i++)
			{
				temp<<=1;

				AD7794_SCLK_Reset();
				
				temp+=AD7794_MISO_Read();
		
				AD7794_SCLK_Set();	
			}		                
			  RegisterBuffer[RegisterAddress]=temp;
			  break;

		  default:
			  break;
	}
	
	AD7794_CS_Set();
}

void AD7794_ReadAllRegister(unsigned char Regindex, unsigned int *Regbuffer)
{
    AD7794_CS_Reset();

	AD7794_WaitBusy();
	
	AD7794_ReadRegister(Regindex, AD7794Registers);
	
	Regbuffer[Regindex]=AD7794Registers[Regindex];    
}

void AD7794_ReadResultForSingleConversion(unsigned char DataIndex, unsigned int *DataBuffer)
{
	AD7794_CS_Reset();
	AD7794_WaitBusy();

	AD7794Registers[COMMUNICATIONS]=WRITE_ENABLE|READ|RS_DATA|CREAD_DISABLE;
	AD7794_WriteRegister(COMMUNICATIONS, AD7794Registers);
	AD7794_ReadRegister(DATA, AD7794Registers);

	DataBuffer[DataIndex]=AD7794Registers[DATA];
}

void AD7794_ReadResultForContinuousConversion(unsigned char StartIndex, unsigned char NumberOfData, unsigned int *DataBuffer)
{
	unsigned char i;

	for(i=0; i<NumberOfData; i++)
	{
		AD7794_CS_Reset();

		AD7794_WaitBusy();
	
		AD7794Registers[COMMUNICATIONS]=WRITE_ENABLE|READ|RS_DATA|CREAD_DISABLE;
		AD7794_WriteRegister(COMMUNICATIONS, AD7794Registers);
		AD7794_ReadRegister(DATA, AD7794Registers);
	
		DataBuffer[StartIndex+i]=AD7794Registers[DATA];
	}
}

void AD7794_ReadResultForContinuousRead(unsigned char StartIndex, unsigned char NumberOfData, unsigned int *DataBuffer)
{
	unsigned char i;

	AD7794Registers[COMMUNICATIONS]=WRITE_ENABLE|READ|RS_DATA|CREAD_ENABLE;
	AD7794_WriteRegister(COMMUNICATIONS, AD7794Registers);

	for(i=0; i<NumberOfData; i++)
	{
		AD7794_CS_Reset();

		AD7794_WaitBusy();
	
		AD7794_ReadRegister(DATA, AD7794Registers);
	
		DataBuffer[StartIndex+i]=AD7794Registers[DATA];
	}
}

void AD7794_ExitContinuousRead(void)
{
	AD7794_CS_Reset();
	AD7794_WaitBusy();
	AD7794Registers[COMMUNICATIONS]=WRITE_ENABLE|READ|RS_DATA|CREAD_DISABLE;
	AD7794_WriteRegister(COMMUNICATIONS, AD7794Registers);
}

unsigned int AD7794_Calc(unsigned int code)
{
    uint64_t calc = 0;
	
	calc = code - 8388608;
	calc *=	2500000;

	calc = calc>>30;
	
	return (unsigned int)calc;    
}

void AD7794_SoftwareReset(void)
{
	  unsigned char i;

	  AD7794_CS_Reset();
	
	  for(i=0;i<32; i++)
	{
		AD7794_SCLK_Reset();	
        AD7794_MOSI_Set();
		AD7794_SCLK_Set();
	}

	  AD7794_CS_Set();
}

void AD7794_WaitBusy(void)
{
    while(GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_14)!=0){;} 
}


static void RCC_Configration(void)
{
    /* Enable peripheral clocks --------------------------------------------------*/

    /* Enable GPIO clock for SPI2 */
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
}

static void GPIO_Configration(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;

    /* Configure AD7794_CS SCK and MOSI pins as GPIO_Mode_Out_PP */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_15;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;    
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_Init(GPIOB, &GPIO_InitStructure);

    /* Configure MISO pins as GPIO_Mode_IN_FLOATING  */    
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_14;    
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING ;
    
    GPIO_Init(GPIOB, &GPIO_InitStructure);  	
	          	
}

static void GPIO_DeConfigration(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;

    /* Configure AD7794_CS SCK and MOSI pins as GPIO_Mode_Out_PP */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_15;   
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD;
	GPIO_Init(GPIOB, &GPIO_InitStructure);

    /* Configure MISO pins as GPIO_Mode_IN_FLOATING  */    
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_14;    
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD ;
    
    GPIO_Init(GPIOB, &GPIO_InitStructure);  	
	          	
}



