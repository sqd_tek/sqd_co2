/*
硬件连接：
        CS <---> PE15
      MOSI <---> PB15
      MISO <---> PB14
      SCLK <---> PB13
    STATUS <---> PB9(硬件需要添加的内容)
	POWEREN<---> PC9
*/
//SPI的初始化过程
//主模式 全双工 CPOL=1 CPHA=1 8位数据帧格式 MSB在前 NSS模式软件模式
//采用模拟spi的方式操作
#include "stm32f10x.h"
#include "AD7794.h"

unsigned  int AD7794Registers[9]; 	
//unsigned  int AD7794Data[128];

static void AD7794_CS_0(void);
static void AD7794_CS_1(void);
static void RCC_Configration(void);
static void GPIO_Configration(void);
static void SPI_Configration(void);
static unsigned char  SPI2_Read_Write(unsigned char data);


void AD7794_Init(void)
{
    RCC_Configration();
    GPIO_Configration();
    SPI_Configration();	    
}

/*AD7794校准*/
void AD7794_Calibrate()
{
	
}

/*AD7794采集程序,更新速率16.7hz*/
void AD7794_Channel_Configration(unsigned int channel, unsigned int polar, unsigned int gain, unsigned int mode )
{
    AD7794Registers[COMMUNICATIONS]=WRITE_ENABLE|WRITE|RS_CONFIGURATION|CREAD_DISABLE;
	AD7794_WriteRegister(COMMUNICATIONS, AD7794Registers);						 
	AD7794Registers[CONFIGURATION]= polar | gain | channel;//0x0400;
	AD7794_WriteRegister(CONFIGURATION, AD7794Registers);

	AD7794Registers[COMMUNICATIONS]=WRITE_ENABLE|WRITE|RS_MODE|CREAD_DISABLE;
	AD7794_WriteRegister(COMMUNICATIONS, AD7794Registers);
	AD7794Registers[MODE]= FILTER_6050_UPDATE_RATE_16 | mode;
	AD7794_WriteRegister(MODE, AD7794Registers);
}

void AD7794_WriteRegister(unsigned char RegisterAddress, unsigned  int *RegisterBuffer)
{
    unsigned  int temp;
    
    temp=RegisterBuffer[RegisterAddress];

	  AD7794_CS_0();
	  
    switch(RegisterAddress)
	{
		case COMMUNICATIONS:			 			 		
		case IO:
            SPI2_Read_Write((unsigned char)temp);
			break;

		case MODE:					
		case CONFIGURATION:	
			SPI2_Read_Write((unsigned char)(temp>>8));
			SPI2_Read_Write((unsigned char)temp);
			break;
			 			
		case OFFSET:			
		case FULLSCALE:
		    SPI2_Read_Write((unsigned char)(temp>>16));		
            SPI2_Read_Write((unsigned char)(temp>>8));
			SPI2_Read_Write((unsigned char)temp);
			break;

		default:
			break;
	  }
	
	  AD7794_CS_1();	
}

void AD7794_ReadRegister(unsigned char RegisterAddress, unsigned  int *RegisterBuffer)
{
	  unsigned  int temp;
	  
	  temp = SPI2->SR;
	  temp = SPI2->DR;
	  temp=0;

	  AD7794_CS_0();

	  switch(RegisterAddress)
	  {
		  case STATUS:			 			 		
		  case ID:
              temp = SPI2_Read_Write(0x00);
		  	  RegisterBuffer[RegisterAddress]=temp;
			  break;

		  case MODE:					
		  case CONFIGURATION:
		      temp = SPI2_Read_Write(0x00);
			  temp = temp<<8;	
			  temp += SPI2_Read_Write(0x00);
			  RegisterBuffer[RegisterAddress]=temp;
			  break;
			 			
		  case OFFSET:			
		  case FULLSCALE:
		  case DATA:
		      temp = SPI2_Read_Write(0x00);
			  temp = temp<<8;	
			  temp += SPI2_Read_Write(0x00);
			  temp = temp<<8;	
			  temp += SPI2_Read_Write(0x00);		                
			  RegisterBuffer[RegisterAddress]=temp;
			  break;

		  default:
			  break;
	}
	
	AD7794_CS_1();
}

void AD7794_ReadResultForSingleConversion(unsigned char DataIndex, unsigned int *DataBuffer)
{
	AD7794_WaitBusy();

	AD7794Registers[COMMUNICATIONS]=WRITE_ENABLE|READ|RS_DATA|CREAD_DISABLE;
	AD7794_WriteRegister(COMMUNICATIONS, AD7794Registers);
	AD7794_ReadRegister(DATA, AD7794Registers);

	DataBuffer[DataIndex]=AD7794Registers[DATA];
}

void AD7794_ReadResultForContinuousConversion(unsigned char StartIndex, unsigned char NumberOfData, unsigned int *DataBuffer)
{
	unsigned char i;

	for(i=0; i<NumberOfData; i++)
	{
		AD7794_CS_0();

		AD7794_WaitBusy();
	
		AD7794Registers[COMMUNICATIONS]=WRITE_ENABLE|READ|RS_DATA|CREAD_DISABLE;
		AD7794_WriteRegister(COMMUNICATIONS, AD7794Registers);
		AD7794_ReadRegister(DATA, AD7794Registers);
	
		DataBuffer[StartIndex+i]=AD7794Registers[DATA];
	}
}

void AD7794_ReadResultForContinuousRead(unsigned char StartIndex, unsigned char NumberOfData, unsigned int *DataBuffer)
{
	unsigned char i;

	AD7794Registers[COMMUNICATIONS]=WRITE_ENABLE|READ|RS_DATA|CREAD_ENABLE;
	AD7794_WriteRegister(COMMUNICATIONS, AD7794Registers);

	for(i=0; i<NumberOfData; i++)
	{
		AD7794_CS_0();

		AD7794_WaitBusy();
	
		AD7794_ReadRegister(DATA, AD7794Registers);
	
		DataBuffer[StartIndex+i]=AD7794Registers[DATA];
	}
}

void AD7794_ExitContinuousRead(void)
{
	AD7794_WaitBusy();
	AD7794Registers[COMMUNICATIONS]=WRITE_ENABLE|READ|RS_DATA|CREAD_DISABLE;
	AD7794_WriteRegister(COMMUNICATIONS, AD7794Registers);
}

void AD7794_SoftwareReset(void)
{
	  unsigned char i;

	  AD7794_CS_0();
	
	  for(i=0;i<4; i++)
	  {
        /* Wait for SPI2 Tx buffer empty */
        while (SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_TXE) == RESET);
        /* Send SPI2 data */
        SPI_I2S_SendData(SPI2, 0xFF);
	  }	

	  AD7794_CS_1();

	  SPI2->SR;
	  SPI2->DR;
}

void AD7794_WaitBusy(void)
{
    while(GPIO_ReadInputDataBit(GPIOB, GPIO_Pin_9)!=0){;} 
}

static void AD7794_CS_0(void)
{
    GPIO_ResetBits(GPIOE, GPIO_Pin_15); 	
}

static void AD7794_CS_1(void)
{
    GPIO_SetBits(GPIOE, GPIO_Pin_15); 	
}

static void RCC_Configration(void)
{
    /* Enable peripheral clocks --------------------------------------------------*/

    /* Enable GPIO clock for SPI2 */
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB | RCC_APB2Periph_GPIOC | RCC_APB2Periph_GPIOE | RCC_APB2Periph_AFIO, ENABLE);
 
    /* Enable SPI2 Periph clock */
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI2, ENABLE);

}

static void GPIO_Configration(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;

    /* Configure SCK and MOSI pins as Alternate Function Push Pull */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13 | GPIO_Pin_15;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;    
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init(GPIOB, &GPIO_InitStructure);

    /* Configure MISO pins as GPIO_Mode_IN_FLOATING  */    
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_14;    
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING ;
    GPIO_Init(GPIOB, &GPIO_InitStructure);
    
    /* Configure AD7794_CS as GPIO_Mode_Out_PP  */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_15;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP ;
    GPIO_Init(GPIOE, &GPIO_InitStructure); 
    
    /* Configure STATUS pin as GPIO_Mode_IN_FLOATING  */    
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;    
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING ;
    GPIO_Init(GPIOB, &GPIO_InitStructure); 

    /* Configure SensorBoard POWER PIN as GPIO_Mode_Out_PP  */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP ;
    GPIO_Init(GPIOC, &GPIO_InitStructure); 	
	          	
}

static void SPI_Configration(void)
{
    SPI_InitTypeDef   SPI_InitStructure;
    
    /* SPI2 Config -------------------------------------------------------------*/
    SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
    SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
    SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
    SPI_InitStructure.SPI_CPOL = SPI_CPOL_High;
    SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge;
    SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
    SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_4;
    SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
    SPI_InitStructure.SPI_CRCPolynomial = 7;
    SPI_Init(SPI2, &SPI_InitStructure);
    
    SPI_Cmd(SPI2, ENABLE);	
}

static unsigned char  SPI2_Read_Write(unsigned char data)
{
    /* Wait for SPI2 Tx buffer empty */
    while (SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_TXE) == RESET);
    /* Send SPI2 data */
    SPI_I2S_SendData(SPI2, data);
    /* Wait for SPI2 data reception */

    while (SPI_I2S_GetFlagStatus(SPI2, SPI_I2S_FLAG_RXNE) == RESET);
    /* Read SPI2 received data */
    return SPI_I2S_ReceiveData(SPI2);    
}
