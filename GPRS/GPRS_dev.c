/*
任务设计方法：
         SIM900A模块的串口配置：
         - BaudRate = 9600 baud  
         - Word Length = 8 Bits
         - One Stop Bit
         - No parity
         - Hardware flow control disabled (RTS and CTS signals)
         - Receive and transmit enabled
硬件连接方式：STM32 USART2
              PA2(USART2 TX)<-------> GPRS RX
              PA3(USART2 RX)<-------> GPRS TX
                        PE3 <-------> GPRS POWER ENABLE (H:open, L:close)
                        PC4 <-------> GPRS POWER KEY
						PA1 <-------> GPRS STATUS
GPRS数据发送方式：USART2 DMA 发送 + FIFO
                  DMA通道：DMA1_Channel5 

GPRS数据接收方式：USART2串口接收中断
      
*/

#include "stm32f10x.h"
#include "stm32f10x_it.h"
#include "string.h"
#include "GPRS_dev.h"
#include "Delay.h"
//#include "debug-uart.h"
#include "Uart_debug.h"

#define GPRS_TXBUFFER_LEN 512
#define GPRS_RXBUFFER_LEN 256
#define GPRS_TXBUFFER_END &GPRS_TxBuffer[GPRS_TXBUFFER_LEN]
 
static  char GPRS_TxBuffer[GPRS_TXBUFFER_LEN];
static  char GPRS_RxBuffer[GPRS_RXBUFFER_LEN];


/*GPRS Command Status*/
unsigned int GPRS_Cmd_status = 0x00;

/*GPRS ERR Status*/
unsigned char GPRS_ERR_Status = 0x00;

/*TCPIOP连接状态*/
unsigned char TCPIP_Status = 0x00;
unsigned char SendOK = 0;



/* Valid data in head to tail-1 */
/* Read position */
static  char * volatile GPRS_TxBuffer_head = GPRS_TxBuffer;
/* Write position */
static  char * volatile GPRS_TxBuffer_tail = GPRS_TxBuffer;

/* GPRS_TxBuffer_head == xmit_buffer_tail means empty so we can only store
   GPRS_TXBUFFER_LEN-1 characters */
 
volatile unsigned char GPRS_Tx_dma_running = 0;
static char * volatile GPRS_Tx_dma_end;


static void GPRS_Tx_Update_DMA(void);
static void RCC_Configuration(void);
static void GPIO_Configuration(void);
static void NVIC_Configuration(void);
static void DMA_Tx_Configuration(char * memorybaseaddr, unsigned short txbuffersize);
static void DMA_Rx_Configuration(void);
static void USART_Configuration (void);
static unsigned int GPRS_USART_Send_Bytes(const char *seq, unsigned int len);
 
/*
USART向SIM900A 发送AT命令
pstr:AT命令字符串
cmd_status:当前命令状态字
err:命令是否发送成功标志
*/
void GPRS_Send_ATCmd(char *pstr, unsigned int cmd_status, unsigned char err, unsigned int delay)
{
	unsigned int len = strlen(pstr);

	/*有命令错误，直接返回*/
	if(GPRS_Cmd_status != NOCMD)
	    return;

	GPRS_Cmd_status = cmd_status;    	
	GPRS_USART_Send_Bytes(pstr, len);

	Delay(delay);
	if(GPRS_Cmd_status != NOCMD)
	{
	    GPRS_ERR_Status = err;
	}	        
}

/*
通过GPRS向服务器发送字符串函数
pstr:发送往串口的字符串指针
*/
void GPRS_Send_Data(char *pstr)
{	
    unsigned int len = strlen(pstr);
	GPRS_USART_Send_Bytes(pstr, len);    
}

  
void USART2_IRQHandler(void)
{	  
    uint16_t temp = 0;

	/*usart接收中断，表示接收到一组数据*/	  
	if(USART_GetITStatus(USART2, USART_IT_IDLE) != RESET)
    {
        /*清除中断标志*/
			
			USART_ClearITPendingBit(USART2,USART_IT_RXNE);
			
		temp = temp;
		temp = USART2->SR;
		temp = USART2->DR;
		    
		/*暂时关闭DMA*/
		DMA_Cmd(DMA1_Channel6, DISABLE);
		    
		/*计算数据帧长度*/
		temp = GPRS_RXBUFFER_LEN - DMA_GetCurrDataCounter(DMA1_Channel6);
		//printf("INT OK\n");
		//printf(GPRS_RxBuffer);
		//GPRS_RxBuffer[temp] = '\0';

		switch(GPRS_Cmd_status)
		{
		    case NOCMD:
			    if((strstr(GPRS_RxBuffer, "CONNECT OK") != NULL)
				 ||(strstr(GPRS_RxBuffer, "ALREADY CONNECT") != NULL))
			    {
				    TCPIP_Status = 1;
					//printf("CONNECT OK\n");
					  UART4_printf(UART4,"\r\n CONNECT OK \r\n");
				}

				if((strstr(GPRS_RxBuffer, "YES") != NULL))
			    {
				    SendOK = 1;
					//printf("SENSOR DATA SEND OK!\n");
						UART4_printf(UART4,"\r\n SENSOR DATA SEND OK! \r\n");
				}
				break;
		    case CPIN:
			    if(strstr(GPRS_RxBuffer, "+CPIN: READY") != NULL)
				{
				    GPRS_Cmd_status = NOCMD;
					//printf("CPIN OK\n"); 
						UART4_printf(UART4,"\r\n CPIN OK \r\n");
				}
			    
				break;
		    case CSQ:
			/*需要解析*/
			    if(strstr(GPRS_RxBuffer, "+CSQ: ") != NULL)
				{
				    GPRS_Cmd_status = NOCMD;
						//printf("CSQ OK\n"); 
						UART4_printf(UART4,"\r\n CSQ OK \r\n");
				}
			    
				break;
		    case CREG:
			/*需要解析*/
			    if(strstr(GPRS_RxBuffer, "+CREG: 0,1") != NULL)
				{
				    GPRS_Cmd_status = NOCMD;
					//printf("CREG OK\n"); 
						UART4_printf(UART4,"\r\n CREG OK\r\n");
				}
			    
				break;
		    case CGATT:
			    if(strstr(GPRS_RxBuffer, "+CGATT: 1") != NULL)
				{
				    GPRS_Cmd_status = NOCMD; 
					//printf("CGATT OK\n");
						UART4_printf(UART4,"\r\n CGATT OK\r\n");
				}
			    
				break;
		    case CSTT:
			case AT:
			case ATE:
			    if(strstr(GPRS_RxBuffer, "OK") != NULL)
				{
				    GPRS_Cmd_status = NOCMD; 
					//printf("CSTT AT ATE OK\n");
						UART4_printf(UART4,"\r\n CSTT AT ATE OK\r\n");
				}
			    
				break;
		    case CIICR:
			    if(strstr(GPRS_RxBuffer, "OK") != NULL)
				{
				    GPRS_Cmd_status = NOCMD;\
					//printf("CIICR OK\n"); 
						UART4_printf(UART4,"\r\n CIICR OK\r\n");
				}
			    
				break;
		    case CIFSR:
			    /*保存ip地址*/
			    if(strstr(GPRS_RxBuffer, "ERR") == NULL)
				{
				    GPRS_Cmd_status = NOCMD;
					//printf("CIFSR OK\n");
						UART4_printf(UART4,"\r\n CIFSR OK\r\n");
				}
				break;
		    case CIPSTART:
			    if(strstr(GPRS_RxBuffer, "OK") != NULL)
				{
				    GPRS_Cmd_status = NOCMD;
					//printf("CIPSTART OK\n");
						UART4_printf(UART4,"\r\n CIPSTART OK\r\n");
				}

			    
				break;
		    case CIPSEND:
			    if(strstr(GPRS_RxBuffer, ">") != NULL)
				{
				    GPRS_Cmd_status = NOCMD;
						//UART4_printf(UART4,"\r\n CIPSEND OK\r\n");
					//printf("CIPSEND OK\n"); 
						
				}
			    
				break;

		    case CTRLZ:
			    if(strstr(GPRS_RxBuffer, "SEND OK") != NULL)
				{
				    GPRS_Cmd_status = NOCMD;
					//printf("CTRLZ OK\n"); 
						UART4_printf(UART4,"\r\n CTRLZ OK\r\n");
				}		    
				break;
		    case CIPCLOSE:
			    if(strstr(GPRS_RxBuffer, "CLOSE OK") != NULL)
				{
				    GPRS_Cmd_status = NOCMD;
						TCPIP_Status = 0;
					//printf("CIPCLOSE OK\n");
						//UART4_printf(UART4,"\r\n CLOSE OK\r\n");
				}
			    
				break;
		    case CIPSHUT:
			    if(strstr(GPRS_RxBuffer, "SHUT OK") != NULL)
				{
				    GPRS_Cmd_status = NOCMD;
					//printf("CIPSHUT OK\n"); 
						//UART4_printf(UART4,"\r\n CIPSHUT OK\r\n");
				}		    
				break;

			default:
			    break;
			    
		}

		/*清空usart接收缓冲区*/
		memset(GPRS_RxBuffer, 0, GPRS_RXBUFFER_LEN);
		    
	    /*重新启动DMA接收*/
	    DMA_Rx_Configuration();
    }
}


	    
static unsigned int GPRS_USART_Send_Bytes(const char *seq, unsigned int len)
{
    /* Since each of the pointers should be read atomically
    there's no need to disable interrupts */
    char *head = GPRS_TxBuffer_head;
    char *tail = GPRS_TxBuffer_tail;
 
    if (tail >= head) 
    {
        /* Free space wraps */
        unsigned int xfer_len = GPRS_TXBUFFER_END - tail;
        unsigned int free = GPRS_TXBUFFER_LEN - (tail - head) - 1;
        if (len > free) len = free;
        if (xfer_len < len) 
        {
            memcpy(tail, seq, xfer_len);
            seq += xfer_len;
            xfer_len = len - xfer_len;
            memcpy(GPRS_TxBuffer, seq, xfer_len);
            tail = GPRS_TxBuffer + xfer_len;
        } 
        else 
        {
            memcpy(tail, seq, len);
            tail += len;
            if (tail == GPRS_TXBUFFER_END) tail = GPRS_TxBuffer;
        }
    } 
    else 
    {
        /* Free space continuous */
        unsigned int free = (head - tail) - 1;
        if (len > free) len = free;
        memcpy(tail, seq, len);
        tail += len;
    }
    GPRS_TxBuffer_tail = tail;
    if (!GPRS_Tx_dma_running) 
    {
        GPRS_Tx_dma_running = 1;
        GPRS_Tx_Update_DMA();
    }
    return len;
}
 
void DMA1_Channel7_IRQHandler(void)
{
    DMA_ClearFlag(DMA1_FLAG_TC7);
    GPRS_TxBuffer_head = GPRS_Tx_dma_end;
    if (GPRS_TxBuffer_tail == GPRS_TxBuffer_head) 
    {
        GPRS_Tx_dma_running = 0;
        return;
    }
    GPRS_Tx_Update_DMA();	
} 
/**
  * @brief  Main program
  * @param  None
  * @retval None
  */
void  GPRS_Init(void)
{         
   GPRS_PWERKEY_Low();
    /* System Clocks Configuration */
    RCC_Configuration();
       
    /* NVIC configuration */
    NVIC_Configuration();

    /* Configure the GPIO ports */
    GPIO_Configuration();

    /* Configure the DMA */
    DMA_Rx_Configuration();

/* USARTy and USARTz configuration -------------------------------------------*/
    USART_Configuration();
 
}

void  GPRS_DeInit(void)
{         
    
	GPIO_InitTypeDef GPIO_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	GPRS_PWERKEY_High();
 
    /* Enable the USART2 DMA Tx Interrupt */
    NVIC_InitStructure.NVIC_IRQChannel = DMA1_Channel7_IRQn ;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelCmd = DISABLE;
    NVIC_Init(&NVIC_InitStructure);

	/* Enable the USART2 DMA Rx Interrupt */
    NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn ;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = DISABLE;
    NVIC_Init(&NVIC_InitStructure);
       
     /* Enable USARTy */
     USART_Cmd(USART2, DISABLE);
     
    /* Enable USART1 DMA TX request */
    USART_DMACmd(USART2, USART_DMAReq_Tx, DISABLE);
	USART_DMACmd(USART2, USART_DMAReq_Rx, DISABLE);
    USART_ITConfig(USART2, USART_IT_IDLE,DISABLE);
    DMA_Cmd(DMA1_Channel6, DISABLE); 
    
    /* Configure USART2 Rx as input floating */  
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
      
	/* Configure POWERKEY as OUT push-pull */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	  
	 /* DMA clock enable */
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, DISABLE);
    /* Enable USART2 Clock */
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, DISABLE); 
}
 
 
 static void GPRS_Tx_Update_DMA (void)
{
    uint16_t dma_send_count;

	if (GPRS_TxBuffer_tail == GPRS_TxBuffer_head) return;
    
    if (GPRS_TxBuffer_head < GPRS_TxBuffer_tail) 
    {
       //DMA_SetCurrDataCounter (DMA1_Channel4, GPRS_TxBuffer_tail - GPRS_TxBuffer_head);
		dma_send_count = GPRS_TxBuffer_tail - GPRS_TxBuffer_head;
        GPRS_Tx_dma_end = GPRS_TxBuffer_tail;    
    } 
    else 
    {
        //DMA_SetCurrDataCounter (DMA1_Channel4, GPRS_TXBUFFER_END - GPRS_TxBuffer_head);
		dma_send_count = GPRS_TXBUFFER_END - GPRS_TxBuffer_head;
        GPRS_Tx_dma_end = GPRS_TxBuffer;
    }

	DMA_Tx_Configuration(GPRS_TxBuffer_head, dma_send_count);
     
    DMA_Cmd(DMA1_Channel7,ENABLE); 
} 

 
 
/**
  * @brief  Configures the different system clocks.
  * @param  None
  * @retval None
  */

static void RCC_Configuration(void)
{    
    /* DMA clock enable */
    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
 
    /* Enable GPIO clock */
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOC | RCC_APB2Periph_AFIO, ENABLE);
 
    /* Enable USART2 Clock */
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE); 
}
 
/**
  * @brief  Configures the different GPIO ports.
  * @param  None
  * @retval None
  */
static void GPIO_Configuration(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;

    /* Configure USART2 Rx as input floating */  
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
      
    /* Configure USART2 Tx as alternate function push-pull */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	/* Configure POWERKEY as OUT push-pull */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	/* Configure STATUS as GPIO_Mode_IN_FLOATING */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	GPIO_ResetBits(GPIOA, GPIO_Pin_1);
}
 
/**
  * @brief  Configures the nested vectored interrupt controller.
  * @param  None
  * @retval None
  */
static void NVIC_Configuration(void)
{
    NVIC_InitTypeDef NVIC_InitStructure;
 
    /* Enable the USART2 DMA Tx Interrupt */
    NVIC_InitStructure.NVIC_IRQChannel = DMA1_Channel7_IRQn ;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);

	/* Enable the USART2 DMA Rx Interrupt */
    NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn ;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}

/**
  * @brief  Configures the DMA.
  * @param  None
  * @retval None
  */
static void DMA_Tx_Configuration(char * memorybaseaddr, unsigned short txbuffersize)
{
    DMA_InitTypeDef DMA_InitStructure;

    /* USART2_Tx_DMA_Channel (triggered by USART2 Tx event) Config */
    DMA_DeInit(DMA1_Channel7);
    DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)(&USART2->DR);
    DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)memorybaseaddr;
    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralDST;
    DMA_InitStructure.DMA_BufferSize = txbuffersize;
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
    DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
    DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
    DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
    DMA_InitStructure.DMA_Priority = DMA_Priority_High;
    DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
    DMA_Init(DMA1_Channel7, &DMA_InitStructure);
    
    DMA_ITConfig(DMA1_Channel7,DMA_IT_TC,ENABLE); 
}

 /**
   * @brief  Configures the DMA.
   * @param  None
   * @retval None
   */
static void DMA_Rx_Configuration(void)
{
    DMA_InitTypeDef DMA_InitStructure;

    /* USART_Rx_DMA_Channel (triggered by USART Rx event) Config */
    DMA_DeInit(DMA1_Channel6);
    DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)(&USART2->DR);
    DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)GPRS_RxBuffer;
    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
    DMA_InitStructure.DMA_BufferSize = GPRS_RXBUFFER_LEN;
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
    DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
    DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
    DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
    DMA_InitStructure.DMA_Priority = DMA_Priority_VeryHigh;
    DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
    DMA_Init(DMA1_Channel6, &DMA_InitStructure);
     
	DMA_Cmd(DMA1_Channel6, ENABLE);  
 }
 
 static void USART_Configuration (void)
 {
 	   /* Configure USART1 */
 	     /* USARTy and USARTz configured as follow:
         - BaudRate = 230400 baud  
         - Word Length = 8 Bits
         - One Stop Bit
         - No parity
         - Hardware flow control disabled (RTS and CTS signals)
         - Receive and transmit enabled
   */	  
     USART_InitTypeDef USART_InitStructure;	 

	 USART_InitStructure.USART_BaudRate = 19200;
     USART_InitStructure.USART_WordLength = USART_WordLength_8b;
     USART_InitStructure.USART_StopBits = USART_StopBits_1;
     USART_InitStructure.USART_Parity = USART_Parity_No;
     USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
     USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
     USART_Init(USART2, &USART_InitStructure);

	 /*使能串口接收中断*/
     USART_ITConfig(USART2, USART_IT_TC,DISABLE); 
     USART_ITConfig(USART2, USART_IT_RXNE,DISABLE); 
     USART_ITConfig(USART2, USART_IT_IDLE,ENABLE);
     
     /* Enable USART1 DMA TX request */
     USART_DMACmd(USART2, USART_DMAReq_Tx, ENABLE);
	 USART_DMACmd(USART2, USART_DMAReq_Rx, ENABLE);
   
     /* Enable USARTy */
     USART_Cmd(USART2, ENABLE);
 }


