#ifndef __GPRS_DEV_H__
#define __GPRS_DEV_H__

/*Command status*/
#define NOCMD    0x00
#define CPIN     0x01
#define CSQ      0x02
#define CREG     0x04
#define CGATT    0x08
#define CSTT     0x10
#define CIICR    0x20
#define CIFSR    0x40
#define CIPSTART 0x80
#define CIPSEND  0x0100
#define CIPCLOSE 0x0200
#define CIPSHUT  0x0400
#define CTRLZ    0x0800
#define AT       0x1000
#define ATE       0x2000

/*Command Err status*/
#define GPRS_ERR_CPIN     0x01
#define GPRS_ERR_CSQ	  0x02
#define GPRS_ERR_CREG	  0x03
#define GPRS_ERR_CGATT	  0x04
#define GPRS_ERR_CSTT	  0x05
#define GPRS_ERR_CIICR	  0x06
#define GPRS_ERR_CIFSR	  0x07
#define GPRS_ERR_CIPSTART 0x08
#define GPRS_ERR_CIPSEND  0x09
#define GPRS_ERR_CIPCLOSE 0x0A
#define GPRS_ERR_CIPSHUT  0x0B
#define GPRS_ERR_CTRLZ    0x0C
#define GPRS_ERR_AT  	  0x0D
#define GPRS_ERR_ATE  	  0x0E
#define GPRS_ERR_NOERR	  0x00

/*等待at命令返回的时间，需要根据实际的时钟频率进行配置*/
#define GPRS_AT_DELAY_FORACK  (unsigned int)120000

#define GPRS_PWERKEY_Low()  GPIO_SetBits(GPIOA, GPIO_Pin_1)
#define GPRS_PWERKEY_High() GPIO_ResetBits(GPIOA, GPIO_Pin_1)  
#define GPRS_STATUS_OK()    GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_4) 


extern unsigned int GPRS_Cmd_status;
extern unsigned char GPRS_ERR_Status;
extern unsigned char TCPIP_Status;
extern unsigned char SendOK;
 
void  GPRS_Init(void);
void  GPRS_DeInit(void);
void GPRS_Send_Data(char *pstr);
void GPRS_Send_ATCmd(char *pstr, unsigned int cmd_status, unsigned char err, unsigned int delay);
//void DMA1_Channel4_IRQHandler(void);
 
 #endif /* __GPRS_DEV_H__ */
