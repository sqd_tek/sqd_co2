#include <stm32f10x.h>
#include "stdio.h"
#include "GPRS_Task.h"
#include "GPRS_dev.h"
#include "Delay.h"

char CtrlZ[3]={0x1A, 0x0D, 0x00};
char connect[50];
#include "Uart_debug.h"
/*
连接TCP/IP连接
返回 1，连接成功
返回 0，连接失败
*/
int GPRS_TCP_Connect(char *IP, char *Port)
{	
	/*是否已经附着GPRS网络*/
	//GPRS_Send_ATCmd("AT+CPIN?\r\n", CPIN, GPRS_ERR_CPIN, 200);//168

	//GPRS_Send_ATCmd("AT+CSQ\r\n", CSQ, GPRS_ERR_CSQ, 200);//138

	//GPRS_Send_ATCmd("AT+CREG?\r\n", CREG, GPRS_ERR_CREG, 200);//140
	GPRS_Send_ATCmd("AT+CIPSHUT\r\n", CIPSHUT, GPRS_ERR_CIPSHUT, 1000);

	GPRS_Send_ATCmd("AT+CGATT?\r\n", CGATT, GPRS_ERR_CGATT, 200);//140


	/*建立TCP连接*/
    GPRS_Send_ATCmd("AT+CSTT\r\n", CSTT, GPRS_ERR_CSTT, 1000);	   //124

    GPRS_Send_ATCmd("AT+CIICR\r\n", CIICR, GPRS_ERR_CIICR, 2000);//1018 5s可行

    GPRS_Send_ATCmd("AT+CIFSR\r\n", CIFSR, GPRS_ERR_CIFSR, 2000);//137

	sprintf(connect, "AT+CIPSTART=\"TCP\",\"%s\",%s\r\n", IP, Port);
	//printf(connect);
	UART4_printf(UART4,connect);

	GPRS_Send_ATCmd(connect, CIPSTART, GPRS_ERR_CIPSTART, 300);//158
	Delay(2000);
	return TCPIP_Status; 	
}

/*
GPRS发送TCP/IP数据
返回1：发送成功
返回0：发送失败
*/

int GPRS_Send(char *pstr)
{
    GPRS_Send_ATCmd("AT+CIPSEND\r\n", CIPSEND, GPRS_ERR_CIPSEND, 4000);//124

	GPRS_Send_Data(pstr);
	Delay(500);

	GPRS_Send_ATCmd(CtrlZ, CTRLZ, GPRS_ERR_CTRLZ, 1000);	//707

	if(GPRS_ERR_Status == GPRS_ERR_NOERR)
	    return 1;
    else
	    return 0; 	        	
}

/*
关闭TCPIP连接
*/
int GPRS_TCP_Close(void)
{
    GPRS_Send_ATCmd("AT+CIPCLOSE\r\n", CIPCLOSE, GPRS_ERR_CIPCLOSE, 500);
    GPRS_Send_ATCmd("AT+CIPSHUT\r\n", CIPSHUT, GPRS_ERR_CIPSHUT, 500);

	if(GPRS_ERR_Status == GPRS_ERR_NOERR)
	    return 1;
    else
	    return 0; 
}


