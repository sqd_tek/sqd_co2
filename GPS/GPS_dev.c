
/*
任务设计方法：
ublox NEO-6M模块的串口配置：
         - BaudRate = 9600 baud  
         - Word Length = 8 Bits
         - One Stop Bit
         - No parity
         - Hardware flow control disabled (RTS and CTS signals)
         - Receive and transmit enabled
硬件连接方式：STM32 USART3
              PB10(USART3 TX)<------->GPS RX
              PB11(USART3 RX)<------->GPS TX
                         PC5 <------->GPS POWER ENABLE (H:open, L:close)
GPS协议：NMEA-0183
GPS数据读取方式：USART3 DMA 接收 + USART3总线空闲中断读取不定长数据帧
       中断：USART_IT_IDLE（判断数据帧结构）
       DMA通道：DMA1_Channel3 
       解析方式：状态机方式
       解析帧结构：GPRMC(标准定为时间，定为状态，纬度，纬度分区，
                          经度，经度分区，相对位移速度，相对位移方向，
                          日期，磁极变量，度数，校验和)
*/

#include "stm32f10x.h"
#include "stm32f10x_it.h"
#include <stdio.h>
#include <string.h>
#include "GPS_dev.h"
#include <debug-uart.h>


/*GPS接收缓冲区长度*/
#define GPS_RXBUFFER_LEN 512  

/*GPS 串口接收缓冲区*/ 
static  char GPS_RxBuffer[GPS_RXBUFFER_LEN];

/*GPS新数据帧处理标志*/ 
volatile unsigned char GPRMC_Flag = 0;

Gps_Info_TypeDef GPS_info;

static void RCC_Configuration(void);
static void GPIO_Configuration(void);
static void NVIC_Configuration(void);
static void DMA_Configuration(void);
static void USART_Configuration (void);

   
void USART3_IRQHandler(void)
{	  
    uint16_t temp = 0;
	char *pgprmc = NULL;
	char *str = NULL;
	char chknum = 0,getchk = 0;
	char dot_number = 0;

	static unsigned char lati_index = 0, longi_index = 0;

	/*usart接收空闲中断，表示接收到一组数据*/	  
	if(USART_GetITStatus(GPS_USART, USART_IT_IDLE) != RESET)
    {
        /*清除中断标志*/
		temp = GPS_USART->SR;
		temp = GPS_USART->DR;
		    
		/*暂时关闭DMA*/
		DMA_Cmd(GPS_DMA_Channel, DISABLE);
		    
		/*计算数据帧长度*/
		temp = GPS_RXBUFFER_LEN - DMA_GetCurrDataCounter(GPS_DMA_Channel);

		printf(GPS_RxBuffer);
		//return;

		/*如果有数据未处理*/
		if(GPRMC_Flag == 1)
		{
			return;
		}
						    
		/*判断帧格式是否正确*/
		GPS_RxBuffer[temp] = '\0';

		if((pgprmc = strstr(GPS_RxBuffer, "$GPRMC")) == NULL)
		{
		    /*清空usart接收缓冲区*/
		    memset(GPS_RxBuffer, 0, GPS_RXBUFFER_LEN);
		    
	        /*重新启动DMA接收*/
	        DMA_Configuration();
			return;
		}

		for(str = pgprmc+1; *str!='*'; str++)
	    {
	        chknum = chknum^(*str);  
	    }
			
		if((str[1]-'0')<=9)
		{
			getchk = (str[1]-'0')<<4;
		}
		else
		{
			getchk =(str[1]-'A'+10)<<4;
		}
		if((str[2]-'0')<=9)
		{
			getchk |= (str[2]-'0');
		}
		else
		{
			getchk |= (str[2]-'A'+10);
		}
		
		if(getchk != chknum)
		{
		   	/*清空usart接收缓冲区*/
		    memset(GPS_RxBuffer, 0, GPS_RXBUFFER_LEN);
		    
	        /*重新启动DMA接收*/
	        DMA_Configuration();
			return;     
		}			
                
		for(dot_number = 0; dot_number<=6; pgprmc++)
		{
		    if(*pgprmc == ',')
			{
			    dot_number++;
				continue;
			}

			switch(dot_number)
			{
			    case 2:
				    GPS_info.Status = *pgprmc;
					break;
				case 3:
				    GPS_info.Latitude[lati_index++] = *pgprmc; 
				    break;    
				case 4:
				    GPS_info.Latitude_NS = *pgprmc;
					break;
				case 5:
				    GPS_info.Longitude[longi_index++] = *pgprmc;
					break;
				case 6:
				    GPS_info.Longitude_EW = *pgprmc;
					break;
				
				default:
				    break;
			}		    
		}

		lati_index = 0;
		longi_index = 0;
		GPRMC_Flag = 1;
		/*清空usart接收缓冲区*/
		memset(GPS_RxBuffer, 0, GPS_RXBUFFER_LEN);
		    
	    /*重新启动DMA接收*/
	    //DMA_Configuration();
    }
}

/**
  * @brief  Main program
  * @param  None
  * @retval None
  */
void  GPS_Init(void)
{         
    /* System Clocks Configuration */
    RCC_Configuration();
       
    /* NVIC configuration */
    NVIC_Configuration();

    /* Configure the GPIO ports */
    GPIO_Configuration();

    /* Configure the DMA */
    DMA_Configuration();

    /* USARTy and USART configuration */
    USART_Configuration();
    
    /* Enable USARTy */
    USART_Cmd(GPS_USART, ENABLE);
}

void  GPS_DeInit(void)
{         
    GPIO_InitTypeDef GPIO_InitStructure;       
    NVIC_InitTypeDef NVIC_InitStructure;
 
    /* Enable the USART Interrupt */
    NVIC_InitStructure.NVIC_IRQChannel = GPS_USART_IRQn;
     NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
     NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;
     NVIC_InitStructure.NVIC_IRQChannelCmd = DISABLE;
     NVIC_Init(&NVIC_InitStructure);
  
    /* Disable USARTy */
    USART_Cmd(GPS_USART, DISABLE);
    
    /* Enable USART DMA RX request */
    USART_DMACmd(GPS_USART, USART_DMAReq_Rx, DISABLE); 
    USART_ITConfig(GPS_USART, USART_IT_IDLE,DISABLE);
    DMA_Cmd(GPS_DMA_Channel,DISABLE);
       
    /* Configure USART1 Rx as input floating */  
    GPIO_InitStructure.GPIO_Pin = GPS_USART_RX | GPS_USART_TX;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPD;
	GPIO_Init(GPS_USART_GPIO, &GPIO_InitStructure);
	GPIO_Init(GPS_USART_GPIO, &GPIO_InitStructure);
	 
	   /* DMA clock enable */
    //RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, DISABLE);
 
     /* Enable USART3 Clock */
    RCC_APB1PeriphClockCmd(GPS_RCC_APBPeriph_USART, DISABLE);
}
  
 
 /**
   * @brief  Configures the different system clocks.
   * @param  None
   * @retval None
   */

 static void RCC_Configuration(void)
 {    
     /* DMA clock enable */
     RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);
 
     /* Enable GPIO clock */
     RCC_APB2PeriphClockCmd(GPS_RCC_APB2Periph_GPIO | RCC_APB2Periph_AFIO, ENABLE);
 
     /* Enable USART3 Clock */
     #if CONFIG_USART1
     RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
     #else
     RCC_APB1PeriphClockCmd(GPS_RCC_APBPeriph_USART, ENABLE);
	 #endif
 }
 
 /**
   * @brief  Configures the different GPIO ports.
   * @param  None
   * @retval None
   */
 static void GPIO_Configuration(void)
 {
     GPIO_InitTypeDef GPIO_InitStructure;
 
     /* Enable the USART1 Pins Software Remapping */
   
     /* Configure USART1 Rx as input floating */  
     GPIO_InitStructure.GPIO_Pin = GPS_USART_RX;
	 GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	 GPIO_Init(GPS_USART_GPIO, &GPIO_InitStructure);
      
     /* Configure USART1 Tx as alternate function push-pull */
	 GPIO_InitStructure.GPIO_Pin = GPS_USART_TX;
	 GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	 GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	 GPIO_Init(GPS_USART_GPIO, &GPIO_InitStructure);

 }
 
 /**
   * @brief  Configures the nested vectored interrupt controller.
   * @param  None
   * @retval None
   */
 static void NVIC_Configuration(void)
 {
     NVIC_InitTypeDef NVIC_InitStructure;

//	 //NVIC_PriorityGroupConfig(NVIC_PriorityGroup_3); 
 
     /* Enable the USART Interrupt */
     NVIC_InitStructure.NVIC_IRQChannel = GPS_USART_IRQn;
     NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 3;
     NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;
     NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
     NVIC_Init(&NVIC_InitStructure);
 }

 /**
   * @brief  Configures the DMA.
   * @param  None
   * @retval None
   */
 static void DMA_Configuration(void)
 {
     DMA_InitTypeDef DMA_InitStructure;
 
     /* USART_Rx_DMA_Channel (triggered by USART Rx event) Config */
     DMA_DeInit(GPS_DMA_Channel);
     DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)(&GPS_USART_BASE_DR);
     DMA_InitStructure.DMA_MemoryBaseAddr = (uint32_t)GPS_RxBuffer;
     DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC;
     DMA_InitStructure.DMA_BufferSize = GPS_RXBUFFER_LEN;
     DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
     DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
     DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
     DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
     DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
     DMA_InitStructure.DMA_Priority = DMA_Priority_Medium;
     DMA_InitStructure.DMA_M2M = DMA_M2M_Disable;
     DMA_Init(GPS_DMA_Channel, &DMA_InitStructure);
     
	 DMA_Cmd(GPS_DMA_Channel,ENABLE);  
 }
 
 static void USART_Configuration (void)
 {
 	   /* Configure USART1 */
 	     /* USARTy and USARTz configured as follow:
         - BaudRate = 230400 baud  
         - Word Length = 8 Bits
         - One Stop Bit
         - No parity
         - Hardware flow control disabled (RTS and CTS signals)
         - Receive and transmit enabled
   */	  
     USART_InitTypeDef USART_InitStructure;	 

	 USART_InitStructure.USART_BaudRate = 9600;
     USART_InitStructure.USART_WordLength = USART_WordLength_8b;
     USART_InitStructure.USART_StopBits = USART_StopBits_1;
     USART_InitStructure.USART_Parity = USART_Parity_No;
     USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
     USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
     USART_Init(GPS_USART, &USART_InitStructure);
     
     /*使能串口空闲中断*/
     USART_ITConfig(GPS_USART, USART_IT_TC,DISABLE); 
     USART_ITConfig(GPS_USART, USART_IT_RXNE,DISABLE); 
     USART_ITConfig(GPS_USART, USART_IT_IDLE,ENABLE);
     
     /* Enable USART DMA RX request */
     USART_DMACmd(GPS_USART, USART_DMAReq_Rx, ENABLE); 
 }


