#ifndef GPS_TASK_h
#define GPS_TASK_h

typedef enum { GPS_TASK_DISABLE = 0, GPS_TASK_ENABLE = !GPS_TASK_DISABLE } GPS_Task_Status;
typedef enum { GPS_POWER_DISABLE = 0, GPS_POWER_ENABLE = !GPS_POWER_DISABLE } GPS_Power_Status;
typedef enum { GPS_DATA_DISABLE = 0, GPS_DATA_ENABLE = !GPS_DATA_DISABLE } GPS_Data_Status;

typedef struct 
{
	GPS_Task_Status  task_status; //任务状态
	GPS_Power_Status power_status;//电源状态
	GPS_Data_Status  data_status; //数据状态
} GPS_Status_Structure;


void GPS_Power_On(void);
void GPS_Power_Off(void);

#endif

