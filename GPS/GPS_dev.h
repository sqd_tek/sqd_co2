 #ifndef __GPS_DEV_H__
 #define __GPS_DEV_H__
 
#define CONFIG_USART1 0

#define CONFIG_USART2 0

#define CONFIG_USART3 1

#define GPS_USART                 USART3
#define GPS_USART_GPIO            GPIOB
#define GPS_DMA_Channel           DMA1_Channel3
#define GPS_USART_TX              GPIO_Pin_10
#define GPS_USART_RX              GPIO_Pin_11
#define GPS_RCC_APB2Periph_GPIO   RCC_APB2Periph_GPIOB
#define GPS_RCC_APBPeriph_USART   RCC_APB1Periph_USART3
#define GPS_USART_IRQn            USART3_IRQn
//#define GPS_USART_IRQHandler      USART3_IRQHandler
#define GPS_USART_BASE_DR         USART3->DR

/*项目采用GPRMC数据帧定位*/
/*$GPRMC,hhmmss,status,latitude,N,longitude,E,spd,cog,ddmmyy,mv,mvE,mode*cs<CR><LF>*/

typedef struct
{
  //unsigned char Time[7];
  //unsigned char Date[10];   //年月日时分秒
  unsigned char Latitude[11];  //GPS纬度
  unsigned char Latitude_NS;
  unsigned char Longitude[12];//GPS经度
  unsigned char Longitude_EW;
  //unsigned char Speed[10];     //速率
  //unsigned char Direct[10];    //方向
  
  //unsigned char  Hour;
  unsigned char Status;         //是否定位，TRUE or FALSE
}Gps_Info_TypeDef;

extern volatile unsigned char GPRMC_Flag;
extern volatile unsigned char GPS_Data_Coming;
extern volatile unsigned char GPS_Data_isNew;
extern Gps_Info_TypeDef GPS_info;
 
void  GPS_Init(void);
void  GPS_DeInit(void);
 
 #endif /* __GPS_DEV_H__ */
