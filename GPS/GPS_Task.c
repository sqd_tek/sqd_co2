/*
任务设计方法：

GPS协议：NMEA-0183
       解析方式：状态机方式
       解析帧结构：GPRMC(标准定为时间，定为状态，纬度，纬度分区，
                          经度，经度分区，相对位移速度，相对位移方向，
                          日期，磁极变量，度数，校验和)
*/
#include "stm32f10x.h"
#include "GPS_Task.h"
#include "GPS_dev.h"


GPS_Status_Structure GPS_Status = {GPS_TASK_DISABLE, GPS_POWER_DISABLE, GPS_DATA_DISABLE};

void GPS_USART_Enable(void);//串口使能
void GPS_USART_Disable(void);//串口失能

