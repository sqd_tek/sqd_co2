#include "stm32f10x.h"
#include "LowPower.h"

extern uint8_t KEY_Wakeup_Flag;

static void SYSCLKConfig_STOP(void);
static void EXTI_Configuration(void);
static void RTC_Configuration(void);
static void NVIC_Configuration(void);


/*低功耗模式初始化*/
void LPM_Init(void)
{
    /* Enable PWR and BKP clock */
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR | RCC_APB1Periph_BKP, ENABLE);
    
    /* Configure EXTI Line to generate an interrupt on rising edge */
    //EXTI_Configuration();
    
    /* Configure RTC clock source and prescaler */
    RTC_Configuration();
    
    /* NVIC configuration */
    //NVIC_Configuration();	
}

/*STOP模式休眠sleep_second 秒的时间*/
void LPM_Set(uint32_t sleep_second)
{
	for(; (sleep_second>0)&&(KEY_Wakeup_Flag==0); sleep_second--)
	{
		/* Wait till RTC Second event occurs */
		RTC_ClearFlag(RTC_FLAG_SEC);
		while(RTC_GetFlagStatus(RTC_FLAG_SEC) == RESET);
		 
		/* Alarm in sleep_second second */
		RTC_SetAlarm(RTC_GetCounter()+ 9);
		/* Wait until last write operation on RTC registers has finished */
		RTC_WaitForLastTask();
		
		/* Request to enter STOP mode with regulator in low power mode*/
		PWR_EnterSTOPMode(PWR_Regulator_LowPower, PWR_STOPEntry_WFI);
	
		/* Reload IWDG counter */
	    IWDG_ReloadCounter();
	} 

     
    /* At this stage the system has resumed from STOP mode -------------------*/
 
    /* Configures system clock after wake-up from STOP: enable HSE, PLL and select 
    PLL as system clock source (HSE and PLL are disabled in STOP mode) */
    SYSCLKConfig_STOP();    	
}

//void EXTI9_5_Config(void)
//{
//    EXTI_InitTypeDef   EXTI_InitStructure;
//    GPIO_InitTypeDef   GPIO_InitStructure;
//    NVIC_InitTypeDef   NVIC_InitStructure;

//   
//   /* Enable GPIOG clock */
//   RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
// 
//   /* Configure PB.06 pin as input floating */
//   GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;
//   GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
//   GPIO_Init(GPIOB, &GPIO_InitStructure);
// 
//   /* Enable AFIO clock */
//   RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
//   /* Connect EXTI8 Line to PG.08 pin */
//   GPIO_EXTILineConfig(GPIO_PortSourceGPIOB, GPIO_PinSource6);
// 
//   /* Configure EXTI6 line */
//   EXTI_InitStructure.EXTI_Line = EXTI_Line6;
//   EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
//   EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
//   EXTI_InitStructure.EXTI_LineCmd = ENABLE;
//   EXTI_Init(&EXTI_InitStructure);
// 
//   /* Enable and set EXTI9_5 Interrupt to the lowest priority */
////   NVIC_InitStructure.NVIC_IRQChannel = EXTI9_5_IRQn;
////   NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
////   NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
////   NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
// 
//   NVIC_Init(&NVIC_InitStructure);
//}

//void EXTI9_5_DeConfig(void)
//{
//     EXTI_InitTypeDef   EXTI_InitStructure;
//     GPIO_InitTypeDef   GPIO_InitStructure;
//     NVIC_InitTypeDef   NVIC_InitStructure;

//   
//   /* Enable GPIOG clock */
//    RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
// 
//   /* Configure PB.06 pin as input floating */
//    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;
//   GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
//   GPIO_Init(GPIOB, &GPIO_InitStructure);
// 
//   /* Enable AFIO clock */
//   RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
//   /* Connect EXTI8 Line to PG.08 pin */
//   GPIO_EXTILineConfig(GPIO_PortSourceGPIOB, GPIO_PinSource6);
// 
//   /* Configure EXTI6 line */
//   EXTI_InitStructure.EXTI_Line = EXTI_Line6;
//   EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
//   EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
//   EXTI_InitStructure.EXTI_LineCmd = DISABLE;
//   EXTI_Init(&EXTI_InitStructure);
// 
//   /* Enable and set EXTI9_5 Interrupt to the lowest priority */
//   NVIC_InitStructure.NVIC_IRQChannel = EXTI9_5_IRQn;
//   NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
//   NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
//   NVIC_InitStructure.NVIC_IRQChannelCmd = DISABLE;
// 
//   NVIC_Init(&NVIC_InitStructure);
//}

/**
   * @brief  Configures system clock after wake-up from STOP: enable HSE, PLL
   *         and select PLL as system clock source.
   * @param  None
   * @retval None
   */
static void SYSCLKConfig_STOP(void)
{
    ErrorStatus HSEStartUpStatus;
	
	/* Enable HSE */
    RCC_HSEConfig(RCC_HSE_ON);
 
    /* Wait till HSE is ready */
    HSEStartUpStatus = RCC_WaitForHSEStartUp();
 
    if(HSEStartUpStatus == SUCCESS)
    {
    
        /* Enable PLL */ 
        RCC_PLLCmd(ENABLE);
 
        /* Wait till PLL is ready */
        while(RCC_GetFlagStatus(RCC_FLAG_PLLRDY) == RESET)
        {
        }
 
        /* Select PLL as system clock source */
        RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);
 
        /* Wait till PLL is used as system clock source */
        while(RCC_GetSYSCLKSource() != 0x08)
        {
        }
    }
}

/**
   * @brief  Configures EXTI Lines.
   * @param  None
   * @retval None
   */
static void EXTI_Configuration(void)
{
    EXTI_InitTypeDef EXTI_InitStructure;
 
    /* Configure EXTI Line17(RTC Alarm) to generate an interrupt on rising edge */
    EXTI_ClearITPendingBit(EXTI_Line17);
    EXTI_InitStructure.EXTI_Line = EXTI_Line17;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init(&EXTI_InitStructure);
}

 
/**
   * @brief  Configures RTC clock source and prescaler.
   * @param  None
   * @retval None
   */
static void RTC_Configuration(void)
{
    /* RTC clock source configuration ------------------------------------------*/
    /* Allow access to BKP Domain */
    PWR_BackupAccessCmd(ENABLE);
 
    /* Reset Backup Domain */
    BKP_DeInit();
   
    /* Enable the LSE OSC */
    RCC_LSEConfig(RCC_LSE_ON);
    /* Wait till LSE is ready */
    while(RCC_GetFlagStatus(RCC_FLAG_LSERDY) == RESET)
    {
    }
 
    /* Select the RTC Clock Source */
    RCC_RTCCLKConfig(RCC_RTCCLKSource_LSE);
 
    /* Enable the RTC Clock */
    RCC_RTCCLKCmd(ENABLE);
    /* RTC configuration -------------------------------------------------------*/
    /* Wait for RTC APB registers synchronisation */
    RTC_WaitForSynchro();
 
    /* Set the RTC time base to 1s */
    RTC_SetPrescaler(32767);  
    /* Wait until last write operation on RTC registers has finished */
    RTC_WaitForLastTask();
 
    /* Enable the RTC Alarm interrupt */
    RTC_ITConfig(RTC_IT_ALR, ENABLE);
    /* Wait until last write operation on RTC registers has finished */
    RTC_WaitForLastTask();
}












/**
   * @brief  Configures NVIC and Vector Table base location.
   * @param  None
   * @retval None
 */
//static void NVIC_Configuration(void)
//{
//    NVIC_InitTypeDef NVIC_InitStructure;
// 
//    /* 2 bits for Preemption Priority and 2 bits for Sub Priority */
//    //NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
// 
//    NVIC_InitStructure.NVIC_IRQChannel = RTCAlarm_IRQn;
//    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
//    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;
//    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
//    NVIC_Init(&NVIC_InitStructure);
//}

// /**
//   * @brief  Configures the SysTick to generate an interrupt each 1 millisecond.
//   * @param  None
//   * @retval None
//   */
//void SysTick_Configuration(void)
//{
//    /* Setup SysTick Timer for 1 msec interrupts  */
//    if (SysTick_Config(SystemCoreClock / 1000))
//    { 
//        /* Capture error */ 
//        while (1);
//    }
//    /* Set SysTick Priority to 3 */
//    NVIC_SetPriority(SysTick_IRQn, 0x0C);
//}
// 
// /**
//   * @brief  Inserts a delay time.
//   * @param  nTime: specifies the delay time length, in milliseconds.
//   * @retval None
//   */
//void Delay(__IO uint32_t nTime)
//{
//    TimingDelay = nTime;
// 
//    while(TimingDelay != 0);
// 
//}
