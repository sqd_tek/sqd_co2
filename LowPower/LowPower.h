#ifndef LOWPOWER_h
#define LOWPOWER_h
#include "stm32f10x.h"

void LPM_Init(void);
void LPM_Set(uint32_t sleep_second);
void EXTI9_5_Config(void);
void EXTI9_5_DeConfig(void);

#endif
