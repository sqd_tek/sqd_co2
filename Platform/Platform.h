#ifndef PLATFORM_H
#define PLATFORM_H

#define Power_GPS_On()          GPIO_SetBits(GPIOA, GPIO_Pin_5) 
#define Power_GPS_Off()         GPIO_ResetBits(GPIOA, GPIO_Pin_5)   

#define Power_GPRS_On()         GPIO_SetBits(GPIOA, GPIO_Pin_0) 
#define Power_GPRS_Off()        GPIO_ResetBits(GPIOA, GPIO_Pin_0) 

#define Power_Sensorboard_On()  GPIO_SetBits(GPIOA, GPIO_Pin_7) 
#define Power_Sensorboard_Off() GPIO_ResetBits(GPIOA, GPIO_Pin_7) 

#define Beep_On()				GPIO_SetBits(GPIOE, GPIO_Pin_6)
#define Beep_Off()				GPIO_ResetBits(GPIOE, GPIO_Pin_6)

#define Core_LED_On()           GPIO_ResetBits(GPIOC, GPIO_Pin_9)
#define Core_LED_Off()          GPIO_SetBits(GPIOC, GPIO_Pin_9)
#define Core_LED_Rate()         (GPIO_ReadOutputDataBit(GPIOC, GPIO_Pin_9)?Core_LED_On():Core_LED_Off())

void PlatForm_Init(void);
void IWDG_Init(void);

#endif
