#include <stm32f10x.h>
#include "Platform.h"
#include "LCD.h"

static void All_GPIO_Configuration(void);


void PlatForm_Init(void)
{
    /*IO初始化*/
	All_GPIO_Configuration();


	//Power_GPS_Off();
	//Power_GPRS_Off();
	//Power_Sensorboard_Off();
//	Power_LCD_Off();

	
}


/*******************************************************************************
       
        全部用到的引脚将在在配置

*******************************************************************************/
static void All_GPIO_Configuration(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
    
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA |  RCC_APB2Periph_GPIOB | RCC_APB2Periph_GPIOC | 
	                       RCC_APB2Periph_GPIOD | RCC_APB2Periph_GPIOE|RCC_APB2Periph_AFIO, ENABLE);
	/**GPRS引脚配置**/
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0|GPIO_Pin_1;						//GPRS_PWR_EN,GPRS-PWRKEY
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	GPIO_ResetBits(GPIOA,GPIO_Pin_0);
	GPIO_ResetBits(GPIOA,GPIO_Pin_1);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4;											//GPRS-STATUS
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	/**GPS引脚配置**/
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5;											//GPS-PWR-EN										
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	GPIO_ResetBits(GPIOA,GPIO_Pin_5);
	
	/**蜂鸣器引脚配置**/
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6;											//蜂鸣器										
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOE, &GPIO_InitStructure);
	GPIO_ResetBits(GPIOE,GPIO_Pin_6);
	
	/**继电器引脚配置**/
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_14|GPIO_Pin_15;											//继电器1,2引脚配置									
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOD, &GPIO_InitStructure);
	Relay_1_off();
	Relay_2_off();
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6|GPIO_Pin_7;											//继电器3,4引脚配置									
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOC, &GPIO_InitStructure);
	Relay_3_off();
	Relay_4_off();
	
	/**内部AD引脚配置**/
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0|GPIO_Pin_1;											//内部温度和电池电压									
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AIN;
	GPIO_Init(GPIOC, &GPIO_InitStructure);
	

	
	/**传感器使能信号**/
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7;											//										
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	GPIO_SetBits(GPIOA,GPIO_Pin_7);
	
	/**温度传感器DS18b20**/
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;											//										
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_OD;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB, &GPIO_InitStructure);
	
    /**温湿度传感器AM2302**/
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;											//										
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_OD;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOD, &GPIO_InitStructure);
	
	/**EEPROM**/
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5;											//I2C-SCK									
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	 GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOE, &GPIO_InitStructure);
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4;											//I2C-SDA									
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_OD;
	GPIO_Init(GPIOE, &GPIO_InitStructure);
	
	/**主板心跳灯**/
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOC, &GPIO_InitStructure);
    GPIO_SetBits(GPIOC,GPIO_Pin_9);
    
    /**出气总阀门开关状态**/
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12 | GPIO_Pin_13;	
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOD, &GPIO_InitStructure);
	
}



void IWDG_Init(void)
{
    uint32_t LsiFreq = 40000;//40k
	
	/* IWDG timeout equal to 4s (the timeout may varies due to LSI frequency
      dispersion) */
    /* Enable write access to IWDG_PR and IWDG_RLR registers */
    IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);
 
    /* IWDG counter clock: LSI/128 */
    IWDG_SetPrescaler(IWDG_Prescaler_256);
 
    /* Set counter reload value to obtain 250ms IWDG TimeOut.
      Counter Reload Value = 16s/IWDG counter clock period
                           = 16s / (LSI/256)
                           = 16s / (LsiFreq/256)
                           = 16*LsiFreq/(256)
                           = LsiFreq/16
    */
   IWDG_SetReload(LsiFreq/16);
 
   /* Reload IWDG counter */
   IWDG_ReloadCounter();
 
  /* Enable IWDG (the LSI oscillator will be enabled by hardware) */
   IWDG_Enable();

}


