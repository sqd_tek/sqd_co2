#ifndef ADXL345_DEV_H_
#define ADXL345_DEV_H_

void ADXL345_init(void);
void ADXL345_ReadXYZ(float *g);

#endif

