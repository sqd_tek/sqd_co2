#include "stm32f10x.h"
#include "Delay.h"
#include "ADXL345_Dev.h"

void SPI1_Init (void)
{
  GPIO_InitTypeDef  GPIO_InitStructure; 
  SPI_InitTypeDef   SPI_InitStructure; 

  /* SPI1 时钟使能 */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO,ENABLE);
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1,ENABLE); 
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOE,ENABLE);

  /* SPI1 SCK(PA5)、MISO(PA6)、MOSI(PA7) 设置 */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5 | GPIO_Pin_6 | GPIO_Pin_7;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;			//口线速度50MHZ
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;	        //复用模式
  GPIO_Init(GPIOA, &GPIO_InitStructure);

  GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_4; //	CS
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(GPIOE, &GPIO_InitStructure);
  GPIO_SetBits(GPIOE, GPIO_Pin_4);			//Gsenser_CS置高 
  
   /* SPI1总线 配置 */ 
  SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;   //全双工  
  SPI_InitStructure.SPI_Mode = SPI_Mode_Master;						   //主模式
  SPI_InitStructure.SPI_DataSize = SPI_DataSize_16b;					   //16位
  SPI_InitStructure.SPI_CPOL = SPI_CPOL_High ;						   //时钟极性 空闲状态时，SCK保持低电平
  SPI_InitStructure.SPI_CPHA = SPI_CPHA_2Edge;						   //时钟相位 数据采样从第一个时钟边沿开始
  SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;							   //软件产生NSS
  SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_256;  //波特率控制 SYSCLK/64
  SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;				   //数据高位在前
  SPI_InitStructure.SPI_CRCPolynomial = 7;							   //CRC多项式寄存器初始值为7 
  SPI_Init(SPI1, &SPI_InitStructure);
  
  /* SPI1 使能 */  
  SPI_Cmd(SPI1,ENABLE);  
 }

//ADXL345的读写函数
uint16_t ADXL345_read_byte(uint16_t add)
{
    GPIO_ResetBits(GPIOE ,GPIO_Pin_4);

    SPI_I2S_SendData(SPI1,(add|0x80)<<8|0x00);
 
    while(SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE)==RESET);
   
    while(SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE)==RESET);
   
    GPIO_SetBits(GPIOE ,GPIO_Pin_4);
   
    return SPI_I2S_ReceiveData(SPI1)&0xFF;
 
}
  
void ADXL345_write_byte(uint16_t add,uint16_t val)
{
    GPIO_ResetBits(GPIOE ,GPIO_Pin_4);

    SPI_I2S_SendData(SPI1,add<<8|val);
 
    while(SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE)==RESET);
   
    while(SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_RXNE)==RESET);
   
    GPIO_SetBits(GPIOE ,GPIO_Pin_4);
    SPI_I2S_ReceiveData(SPI1)&0xFF;
 
}
  
void ADXL345_ReadXYZ(float *g)
{ 
    
	uint16_t BUF[6];   // ′?·?X,Y,Z?áμ?êy?Y
    int16_t temp;
   
    BUF[0] = ADXL345_read_byte(0x32);  
    BUF[1] = ADXL345_read_byte(0x33);
    Delay(1); 
	  
    BUF[2] = ADXL345_read_byte(0x34); 
    BUF[3] = ADXL345_read_byte(0x35);
    Delay(1); 
	   
    BUF[4] = ADXL345_read_byte(0x36);  
    BUF[5] = ADXL345_read_byte(0x37);   
    Delay(1);
   
    temp = (BUF[1] << 8) + BUF[0];
    if(temp < 0)
        temp = -temp;
    g[0] = (float)(temp * 3.9);  //
   
    temp = (BUF[3] << 8) + BUF[2];
    if(temp < 0)
        temp = -temp;
    g[1] = (float)(temp * 3.9);  //
    temp = (BUF[5] << 8) + BUF[4];
    if(temp < 0)
        temp = -temp;
    g[2] = (float)(temp * 3.9);  //
}

//ADXL345初始化配置函数
void ADXL345_init(void)
{
   SPI1_Init();   
 
   //ADXL345_write_byte(0x2D,0x00);
   ADXL345_write_byte(0x31,0x2B);//低电平有效，+-16g，右对齐
   
   
   //ADXL345_write_byte(0x1E,0x00); //X (15.6mg/LSB)
   //ADXL345_write_byte(0x1F,0x00); //Y  (15.6mg/LSB)
   //ADXL345_write_byte(0x20,0x00); //Z(15.6mg/LSB)
   //ADXL345_write_byte(0x21,0x00);  //(1.25ms/LSB)
   //ADXL345_write_byte(0x22,0x00);  // (1.25ms/LSB)
   //ADXL345_write_byte(0x23,0x00);  // (1.25ms/LSB)
 
   
   ADXL345_write_byte(0x27,0x60);  //
   ADXL345_write_byte(0x24,0x20);  // (62.5mg/LSB)
   //ADXL345_write_byte(0x25,0x20);  // (62.5mg/LSB)
   //ADXL345_write_byte(0x26,0x02);  // (1s/LSB)
   

   //ADXL345_write_byte(0x28,0x00);  // (62.5mg/LSB)
   //ADXL345_write_byte(0x29,0x00);  
   //ADXL345_write_byte(0x2A,0x80);  //
   //ADXL345_read_byte(0x2B);    
   //ADXL345_write_byte(0x2C,0x08); 
   //ADXL345_write_byte(0x2D,0x28); //
   ADXL345_write_byte(0x2F,0x00);
   ADXL345_write_byte(0x2E,0x10); 
   ADXL345_write_byte(0x2D,0x08); 
   
   //ADXL345_read_byte(0x30);    //
   //ADXL345_write_byte(0x31,0X0B); //
   //ADXL345_write_byte(0x38,0x00);  //
   //ADXL345_read_byte(0x39);    //
 }

