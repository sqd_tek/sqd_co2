#ifndef __MQ4_H
#define __MQ4_H
#include "stm32f10x.h"

#define M 1
#define N 10

#define ADC1_DR_Address    ((uint32_t)0x4001244C)
#define Beep_On()								GPIO_ResetBits(GPIOE,GPIO_Pin_9)
#define Beep_Off()							GPIO_SetBits(GPIOE,GPIO_Pin_9)

extern void ADC1_Init(void);
extern void MQ4_NVIC_Config(void);
uint8_t Get_Wind_Speed(void);
float BatteryGetVolt(void);
//void filter(void);
#endif
