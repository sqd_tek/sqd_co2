#include <stm32f10x.h>
#include "Platform.h"
#include "config.h"
#include "wind_speed.h"

volatile  uint16_t AD_Value[N]; //用来存放ADC转换结果，也是DMA的目标地址
volatile  u16 After_filter[M];

void MQ4_NVIC_Config(void);

void ADC1_Init(void)
{
	ADC_InitTypeDef ADC_InitStructure;
	DMA_InitTypeDef DMA_InitStructure;

	RCC_ADCCLKConfig(RCC_PCLK2_Div8); //72M/8=9,ADC 最大时间不能超过14M
	/* Enable peripheral clocks ------------------------------------------------*/
	/* Enable DMA1 clock */
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);

	/*------------------使能时钟---------------*/
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1,ENABLE);        



	DMA_DeInit(DMA1_Channel1); //将DMA 的通道1 寄存器重设为缺省值
	DMA_InitStructure.DMA_PeripheralBaseAddr = (u32)ADC1_DR_Address; //DMA外设ADC 基地址
	DMA_InitStructure.DMA_MemoryBaseAddr = (u32)&AD_Value; //DMA 内存基地址
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralSRC; //内存作为数据传输的目的地
	DMA_InitStructure.DMA_BufferSize = N*M; //DMA 通道的DMA 缓存的大小
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable; //外设地址寄存器不变
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable; //内存地址寄存器递增
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord; //数据宽度为16 位
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord; //数据宽度为16 位
	DMA_InitStructure.DMA_Mode = DMA_Mode_Circular; //工作在循环缓存模式
	DMA_InitStructure.DMA_Priority = DMA_Priority_High; //DMA 通道 x 拥有高优先级
	DMA_InitStructure.DMA_M2M = DMA_M2M_Disable; //DMA 通道x 没有设置为内存到内存传输
	DMA_Init(DMA1_Channel1, &DMA_InitStructure); //根据DMA_InitStruct 中指定的参数初始化DMA 的通道




	ADC_DeInit(ADC1); //将外设 ADC1 的全部寄存器重设为缺省值
	/* ADC1 configuration ‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐‐*/
	ADC_InitStructure.ADC_Mode = ADC_Mode_Independent; //ADC 工作模式:ADC1 和ADC2工作在独立模式
	ADC_InitStructure.ADC_ScanConvMode =ENABLE; //模数转换工作在扫描模式
	ADC_InitStructure.ADC_ContinuousConvMode = ENABLE; //模数转换工作在连续转换模式
	ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_None; //外部触发转换关闭
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right; //ADC 数据右对齐
	ADC_InitStructure.ADC_NbrOfChannel = M; //顺序进行规则转换的ADC 通道的数目
	ADC_Init(ADC1, &ADC_InitStructure); //根据ADC_InitStruct 中指定的参数初始化外设ADCx 的寄存器
	/* ADC1 regular channel11 configuration */
	//设置指定ADC 的规则组通道，设置它们的转化顺序和采样时间
	//ADC1,ADC 通道x,规则采样顺序值为y,采样时间为239.5 周期

//	ADC_RegularChannelConfig(ADC1, ADC_Channel_2, 1, ADC_SampleTime_239Cycles5 );//PA2,边缘制冷片温度temper[0]
//	ADC_RegularChannelConfig(ADC1, ADC_Channel_4, 3, ADC_SampleTime_239Cycles5 );//PA4,电池电压
//	ADC_RegularChannelConfig(ADC1, ADC_Channel_5, 2, ADC_SampleTime_239Cycles5 );//PA5,中心制冷片温度temper[1]
	ADC_RegularChannelConfig(ADC1, ADC_Channel_6, 1, ADC_SampleTime_239Cycles5 );//PA0,边缘制冷片温度temper[0]
//	ADC_RegularChannelConfig(ADC1, ADC_Channel_4, 3, ADC_SampleTime_239Cycles5 );//PA4,电池电压
//	ADC_RegularChannelConfig(ADC1, ADC_Channel_5, 2, ADC_SampleTime_239Cycles5 );//PA5,中心制冷片温度temper[1]
	/*
	ADC_RegularChannelConfig(ADC1, ADC_Channel_8, 5, ADC_SampleTime_239Cycles5 );
	ADC_RegularChannelConfig(ADC1, ADC_Channel_9, 6, ADC_SampleTime_239Cycles5 );
	ADC_RegularChannelConfig(ADC1, ADC_Channel_10, 1, ADC_SampleTime_239Cycles5 );//PC0
	ADC_RegularChannelConfig(ADC1, ADC_Channel_11, 2, ADC_SampleTime_239Cycles5 );//PC1
	ADC_RegularChannelConfig(ADC1, ADC_Channel_12, 3, ADC_SampleTime_239Cycles5 );//PC2
	ADC_RegularChannelConfig(ADC1, ADC_Channel_13, 4, ADC_SampleTime_239Cycles5 );//PC3
	/////ADC_RegularChannelConfig(ADC1, ADC_Channel_14, 5, ADC_SampleTime_239Cycles5 );
	//ADC_RegularChannelConfig(ADC1, ADC_Channel_15, 12, ADC_SampleTime_239Cycles5 );//PC5
	*/
	// 开启ADC 的DMA 支持（要实现DMA 功能，还需独立配置DMA 通道等参数）
	ADC_DMACmd(ADC1, ENABLE);
	/* Enable ADC1 */
	ADC_Cmd(ADC1, ENABLE); //使能指定的ADC1
	/* Enable ADC1 reset calibaration register */
	ADC_ResetCalibration(ADC1); //复位指定的ADC1 的校准寄存器
	/* Check the end of ADC1 reset calibration register */
	while(ADC_GetResetCalibrationStatus(ADC1)); //获取ADC1 复位校准寄存器的状态,设置状态则等待
	/* Start ADC1 calibaration */
	ADC_StartCalibration(ADC1); //开始指定ADC1 的校准状态
	/* Check the end of ADC1 calibration */
	while(ADC_GetCalibrationStatus(ADC1)); //获取指定ADC1 的校准程序,设置状态则等待

}

void MQ4_NVIC_Config(void)
{
	NVIC_InitTypeDef NVIC_InitStructure;
	//NVIC_PriorityGroupConfig(NVIC_PriorityGroup_1);
	NVIC_InitStructure.NVIC_IRQChannel = DMA1_Channel1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=1 ;//抢占优先级1
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;		//响应优先级0
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			//IRQ通道使能
	NVIC_Init(&NVIC_InitStructure);	//根据指定的参数初始化VIC寄存器
}

//均值滤波

void filter(void)
{
	//int i=0;
	int sum = 0;
	u8 count;
//	for(i=0;i<M;i++)
//	{
	  for ( count=0;count<N;count++)
	  {
	  	sum += AD_Value[count];
	  }
	  After_filter[M-1]=sum/N;
	  sum=0;
//	}
}

//float BatteryGetVolt(void)
//{
//	return (float)(AD_Value[1] * 3.3 / 4096)*2; 
//}



/*获取ADC 的值，将二进制换算为十进制*/
uint8_t Get_Wind_Speed(void)
{
	float volt;
	uint8_t value;
	filter();
	volt = After_filter[0] * 3.30 *5.33/ 4096;			//电压信号转换为电流信号
	if(volt<4)
						{
							value =0;					//无风
						}
						if((4.0<volt)&&(volt<5.0))
						{
							value =1;				//软风1
						}	
						else if((5.0<volt)&&(volt<6.0))
						{
							value =2;				//软风2
						}
						else if((6.0<volt)&&(volt<7.0))
						{
							value =3;				//微风
						}
						else if((7.0<volt)&&(volt<8.0))
						{
							value =4;			//和风
						}
						else if((8.0<volt)&&(volt<9.0))
						{
							value =5;		//	清劲风
						}
						else if((9.0<volt)&&(volt<10.0))
						{
							value =6;		//强风
						}
						else if((10.5<volt)&&(volt<13.0))
						{
							value =7;		//疾风
						}
						else if((13.0<volt)&&(volt<15.0))
						{
							value =8;		//大风
						}
						else if(volt>15.0)
						{
							value =9;		//烈风
						}
	
	return value;
	
}
