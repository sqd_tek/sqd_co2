#include "stm32f10x.h"
#include "stdio.h"
#include "LCD_Task.h"
#include "LCD.h"
#include "Delay.h"
#include "eeprom.h"
#include "Platform.h"
#include "AD7794.h"
#include "string.h"


uint8_t Login_Flag = 0;
uint32_t KEY_Waittime = 60;//30s无按键动作屏幕休眠
uint8_t Menu;
float Weight_K;
float Pressure_K;
static uint16_t Trig1,Trig2,Count1,Count2;
static uint8_t  Display_Presure;
Gas_Type gas;
Board_Type Board;
uint16_t gas_index;
uint8_t Load_Flag = 0;//重装标志

CO2_data_Type co2;


/**压差方式所用变量**/
/*******压差相关变量*******/
#define Diameter_cm 162							//罐体外径
float Diameter_div4;
float PiDD;
float coff_ka; //当h<=D/4 V(ml)=coff_ka*h
float coff_kb; //当h>D/4 V(ml)=coff_kb*h - coff_b;
float coff_b;
float diffpressure_K;
//static uint32_t diffpressure1;
//static uint32_t diffpressure2;

void coefficient_diffpressure(uint32_t p1, uint32_t p2, uint32_t code1, uint32_t code2, float *diffpressure_k);

/*充装过程中的变量*/
uint32_t W_Load;
uint32_t W_Load_Before;
uint32_t W_Load_After;

uint32_t cur_add_weight =0;
uint32_t cur_add_time =0;	   //当前开始充液的时间
//float cur_full_volume =0;	//当前充满时的流量
uint32_t  cur_full_weight =0;
uint32_t cur_full_time =0;		//当前充满时的时间
//float start_volume =0;	//开始计算总流量时的流量及时间
uint32_t start_weight =0;
uint32_t adjust_time =0;
uint8_t  Load_first_flag=0;
/*流量变量*/
//uint32_t  cur_volume =0;		//当前测量流量
uint32_t   cur_weight =0;
uint32_t  cur_total_weight=0;	//消耗总重量

//uint32_t  cur_time =0;		//当前测量时间
float  total_volume =0.0;	    //当前总消耗流量
uint32_t   total_weight =0;
//uint32_t  total_time =0;	//当前总消耗流量对应的时间
uint32_t cur_total_volume=0;			 //总消耗流量

extern uint8_t LCD_Task;

static uint8_t seting_index = 1;
static char strtemp[9];
//static char padcode[25];

uint8_t Freq_433_send_flag=0;

/*全局变量设置*/

static uint32_t Server_IP;
static uint16_t Server_Port;

/**************/

static uint8_t weight_index = 1;
static uint16_t weight_range_index;
static uint16_t weight_value = 20;//砝码值
static uint16_t weight1;
static uint16_t weight2;

static uint8_t menu2_index;
static char serverip_input[16];
static char port_input[5];
static uint16_t timer_interval_index;

static char pwd_input[7] = "000000";
static uint8_t pwd_index;
uint8_t Solenoid=0;//电磁阀状态 0 关断， 1打开

/*压力*/
static uint8_t pressure_index;
static uint16_t pressure1;
static uint16_t pressure2;

static uint16_t board_id;

//static uint16_t netweight;
static int16_t netweight;
static uint32_t maxvolume ;
 
/**温度设置**/
uint8_t Temp_Alarm=0;
void Temp_Alarm_add(void);
void Temp_Alarm_dec(void);
void LCD_Display_Seting_Temp(void);
void Temp_Alarm_Set(void);

/**湿度设置**/
uint8_t RH_Alarm=0;
void RH_Alarm_add(void);
void RH_Alarm_dec(void);
void LCD_Display_Seting_RH(void);
void RH_Alarm_Set(void);

Time_Display timer_intervel[9] = {   {1, 2, 600, " 5M"},
											{1, 6, 600, " 10M"},
											{1, 12, 1200, " 20M"},
											{2, 2, 1800, " 30M"},
											{2, 6, 3600, " 1H"},
											{2, 12, 10800, " 2H"},
											{3, 2, 21600, " 3H"},
											{3, 6, 43200, " 6H"},
											{3, 12, 86400, " 12H"},
			                            };
			                            
Weight_Range_Display weight_range[6] = {   
	                                        {1, 4, 2000, " 2T"},
											{1, 11, 3000, " 3T"},
											{2, 4, 5000, " 5T"},
											{2, 11, 8000, " 8T"},
											{3, 4, 12000, "12T"},
											{3, 11, 20000, "20T"},
				
			                            };

char Seting_table[8][17]={
        			       "1. Set Weight   ",
						   "2. Set Pressure ",
						   "3. Set Timer    ",
						   "4. Set ID       ",
						   "5. Set Server   ",
                           "6. Set Default  ",                 
                           "7. Solenoid     ",
						   "8. About Product"
		               } ;

char weight_table[6][17]={
        			       "1. T-weight     ",
						   "2. Weight value ",
						   "3. Zero         ",
						   "4. Caibartion   ",
						   //"5. NET Weight   ",
						   "5. Adjustment   ",
						   "6. MaxVolume    "
		               } ;

char pressure_table[2][17]={
						   "1. Pressure1    ",
						   "2. Pressure2    "
						 };

Gas_Type gas_table[5] = {
                      {"LOX:", 0, 0, 0, 0, 0, 0, 0},
          			  {"LAR:", 0, 0, 0, 0, 0, 0, 0 },
					  {"CO2:", 0, 0, 0, 0, 0, 0, 0},
					  {"LN2:", 0, 0, 0, 0, 0, 0, 0},
					  {"LNG:", 0, 0, 0, 0, 0, 0, 0}
		          };

/*menu = 0*/
void gas_index_add(void);
void gas_presure_display(void);
void gas_typesave(void);
void menulist(void);
															    
/*menu = 1*/
void seting_index_add(void);
void seting_index_dec(void);
void menu_back(void);
void into_seting(void);

/*menu = 2*/
void IP_port_index_add(void);
void IP_port_add(void);
void IP_port_active(void);

/*menu = 3*/
void pressure_index_add(void);
void pressure_index_dec(void);
void into_seting_pressure(void);

/*menu = 4*/
void weight_index_add(void);
void weight_index_dec(void);
void into_seting_weight(void);


/*menu = 5*/
void timer_interval_add(void);
void timer_interval_dec(void);
void timer_interval_active(void);

/*menu = 6*/
void boardid_add(void);
void boardid_dec(void);
void boardid_active(void);

/*Menu=7 (set default)按键处理***************************************/
void default_active(void);


/*menu = 8*/
void Solenoid_switch(void);

/*menu = 8*/
void pwd_active(void);
void pwd_value_add(void);
void pwd_index_add(void);
/*menu = 9*/

/*menu = 10*/
void weight_range_add(void);
void weight_range_dec(void);
void weight_range_active(void);

void weight_value_add(void);
void weight_value_dec(void);
void weight_value_active(void);

/*menu = 11 12*/
void weight1_value_add(void);
void weight1_value_dec(void);
void weight2_value_add(void);
void weight2_value_dec(void);
void excute_weight_zero_adj(void);
void excute_weight_weight_adj(void);

//void netweight_sw(void);
void netweight_value_add(void);
void netweight_value_dec(void);
void excute_net_weight(void);

void weight_maxvolume_add(void);
void weight_maxvolume_dec(void);
void weight_maxvolume_active(void);

void pressure1_value_add(void);
void pressure1_value_dec(void);
void pressure1_adj_active(void);
void LCD_Display_Seting_pressure1adj(void);

void pressure2_value_add(void);
void pressure2_value_dec(void);
void pressure2_adj_active(void);
void LCD_Display_Seting_pressure2adj(void);

void KEY_LOAD_Handle(void);
void KEY_UP_Handle(void);
void KEY_DOWN_Handle(void);
void KEY_ENTER_Handle(void);
void KEY_MENU_Handle(void);

void LCD_Display_Seting_ID(void);
void LCD_Display_Seting_Weightweight(void);
void LCD_Display_Seting_maxvolume(void);
void LCD_Display_Seting_Net_Weight(void);
void LCD_Display_Seting_Weight0adj(void);
void LCD_Display_Seting_Weightwadj(void);


/**压差方式校准函数**/
void diffpressure1_adj_active(void);
void diffpressure2_adj_active(void);

/*******************LCD KEY恢复默认*************************/
void LCD_KEY_Deinit(void)
{
    LCD_IO_Deinit();
}
/*******************LCD KEY初始化*************************/
void LCD_KEY_Init(void)
{
    LCD_IOinit_OUT();
	KEY_Init();
	LCD_RST_Reset();
	Delay_us(10);
    LCD_RST_Set();
	Delay_us(10);
    LCD_Initial();
	LCD_Screen_Clear();
}

/***********************************************************/
void KEY_Scan(void)
{
    uint16_t ReadData1,ReadData2;
	static uint8_t times;
	static uint8_t longtimes;
	//static uint8_t num;

		ReadData1= GPIO_ReadInputData(GPIOB);
		ReadData2= GPIO_ReadInputData(GPIOE);
    if(((ReadData1 & 0x0300) == 0x0300)&&((ReadData2 & 0x0003) == 0x0003))
    {
       Trig1 = 0;
			Trig2 = 0;
				Count1 = 0;
				Count2 = 0;
				times = 0;
				longtimes = 0;
		return;
    }
    
    Delay(10);
		ReadData1= GPIO_ReadInputData(GPIOB);
		ReadData2= GPIO_ReadInputData(GPIOE);
   if(((ReadData1 & 0x0300) == 0x0300)&&((ReadData2 & 0x0003) == 0x0003))
    {
       Trig1 = 0;
			Trig2 = 0;
				Count1 = 0;
				Count2 = 0;
				times = 0;
				longtimes = 0;
		return;
    }

	//KEY_Waittime = 60;
	ReadData1 ^= 0x0F00;
  ReadData1 &= 0x0300;
	ReadData2 ^= 0x000F;
	ReadData2 &= 0x0003;
    
    Trig1 = ReadData1&(ReadData1^Count1);
		Trig2 = ReadData2&(ReadData2^Count2);
    Count1 = ReadData1;
		Count2 = ReadData2;

//	if(Count1 & KEY_LOAD)
//	{
//		times++;
//		if(times>4)
//		{
//			 KEY_LOAD_Handle();
//			 times =0;
//		}
//	}
	if(Trig1 & KEY_UP)
	{
	  	KEY_UP_Handle();
			times =0;
	}
  if(Count1 & KEY_UP)
	{
	    times++;
		
		if(times>15)
		{
		    times = 0;
			longtimes++;

			if(Menu == BID)
			{
		        board_id += 10;
				LCD_Display_Seting_ID();
			}
			if(Menu == (WEIGHT_STD+SECOND_BASE))
			{
				if(weight_value < 10100)
				{
				    weight_value += 100;
				}
                else
                {
                    weight_value = 100;
                }   
				     
			    LCD_Display_Seting_Weightweight();  
			}
			if(Menu == (WEIGHT_ZERO+SECOND_BASE))
			{
			  if(longtimes<10)
				{
					if(weight1 < weight_range[Board.weightrangeindex].range)
					{
							weight1 += 10;
					}
					else
					{
							weight1 = 0;
					}
					LCD_Display_Seting_Weight0adj();
				}
				else
				{
					if(weight1 < weight_range[Board.weightrangeindex].range)
					{
							weight1 += 100;
					}
					else
					{
							weight1 = 0;
					}
					LCD_Display_Seting_Weight0adj();
				}
			}
			if(Menu == (WEIGHT_CALI+SECOND_BASE))
			{
			  if(longtimes<10)
				{
					if(weight2 < weight_range[Board.weightrangeindex].range)
					{
							weight2 += 10;
					}
					else
					{
							weight2 = 0;
					}
					LCD_Display_Seting_Weightwadj();
				}
				else
				{
					if(weight2 < weight_range[Board.weightrangeindex].range)
					{
							weight2 += 100;
					}
					else
					{
							weight2 = 0;
					}
					longtimes =0;
					LCD_Display_Seting_Weightwadj();
					
				}
			}

			if(Menu == (WEIGHT_NET+SECOND_BASE))
			{
//				if(netweight < 4990)
//				{
//				    netweight += 10;
//				}
//                else
//                {
//                    netweight = 0;
//                }   
				if(netweight < 4990)
				{
				    netweight += 10;
				}
                else
                {
                    netweight = -5000;
                }       
			    LCD_Display_Seting_Net_Weight();  
			}

			if(Menu == (MAXVOLUME+SECOND_BASE))
			{
			    if(longtimes<10)
				{
					if(maxvolume < (MAXVOLUME_T - MAXVOLUME_STEP_M))
					{
					    maxvolume += MAXVOLUME_STEP_M;
					}
					else
					{
					    maxvolume = MAXVOLUME_B;
					}
				}
				else
				{
					if(maxvolume < (MAXVOLUME_T - MAXVOLUME_STEP_L))
					{
					    maxvolume += MAXVOLUME_STEP_L;
					}
					else
					{
					    maxvolume = MAXVOLUME_B;
					}
				}
				LCD_Display_Seting_maxvolume();
			}
			if(Menu == (PRESSURE1+SECOND_BASE))
			{
			    if(pressure1 < 5900)
				{
				    pressure1 += 100;
				}
				else
				{
				    pressure1 = 0;
				}
				LCD_Display_Seting_pressure1adj();
			}
			if(Menu == (PRESSURE2+SECOND_BASE))
			{
			    if(pressure2 < 5900)
				{
				    pressure2 += 100;
				}
				else
				{
				    pressure2 = 0;
				}
				LCD_Display_Seting_pressure2adj();
			}
//			if(Menu==SIM)
//			{
//					if(Sim_Num_Index==1)
//					{
//						if(longtimes<10)
//						{
//							if(Sim_Num1<999)
//							{
//							Sim_Num1 +=10;
//							}
//							else
//							{
//							Sim_Num1 =130;
//							}
//						}
//						else
//						{
//							if(Sim_Num1<999)
//							{
//							Sim_Num1 +=100;
//							}
//							else
//							{
//							Sim_Num1 =130;
//							}
//						}
//					}
//					if(Sim_Num_Index==2)
//					{
//						if(longtimes<10)
//						{
//						if(Sim_Num2<9999)
//						{
//						Sim_Num2 +=10;
//						}
//						else
//						{
//						Sim_Num2 =1111;
//						}
//						}
//						else
//						{
//						if(Sim_Num2<9999)
//						{
//						Sim_Num2 +=100;
//						}
//						else
//						{
//						Sim_Num2 =1111;
//						}
//						}
//					}
//					if(Sim_Num_Index==3)
//					{
//						if(longtimes<10)
//						{
//						if(Sim_Num3<9999)
//						{
//						Sim_Num3 +=10;
//						}
//						else
//						{
//						Sim_Num3 =1111;
//						}
//						}
//						else
//						{
//						if(Sim_Num3<9999)
//						{
//						Sim_Num3 +=100;
//						}
//						else
//						{
//						Sim_Num3 =1111;
//						}	
//						}
//					}
//					LCD_Sim_Num();
//			}
		}
	}
    
   if(Trig1 & KEY_DOWN)
			{
			    KEY_DOWN_Handle();
					times =0;
			}
    if(Count1 & KEY_DOWN)
	{
		times++;

		if(times>15)
		{
			times = 0;
			longtimes++;

			if(Menu == BID)
			{
				board_id -= 10;
				LCD_Display_Seting_ID();
			}
			if(Menu == (WEIGHT_STD+SECOND_BASE))
			{
				if(weight_value > 100)
				{
					weight_value -= 100;
				}
				else
				{
				    weight_value = 10200;   
				}  	
						     
			    LCD_Display_Seting_Weightweight(); 
		    }
			if(Menu == (WEIGHT_ZERO+SECOND_BASE))
			{
				if(longtimes<10)
				{
					if(weight1>10)
					{
						weight1 -= 10;
					}
					else
					{
						weight1 = weight_range[Board.weightrangeindex].range;
					}
					LCD_Display_Seting_Weight0adj();
				}
				else
				{
					if(weight1>100)
					{
						weight1 -= 100;
					}
					else
					{
						weight1 = weight_range[Board.weightrangeindex].range;
					}
					longtimes =0;
					LCD_Display_Seting_Weight0adj();
				}
			}

			if(Menu == (WEIGHT_CALI+SECOND_BASE))
			{
				if(longtimes<10)
				{
				if(weight2>10)
				{
					weight2 -= 10;
				}
				else
				{
				    weight2 = weight_range[Board.weightrangeindex].range;
				}
				LCD_Display_Seting_Weightwadj();
				}
				else
				{
					if(weight2>100)
					{
					weight2 -= 100;
					}
					else
					{
				    weight2 = weight_range[Board.weightrangeindex].range;
					}
				LCD_Display_Seting_Weightwadj();
				}
			}
			if(Menu == (WEIGHT_NET+SECOND_BASE))
			{
//				if(netweight > 10)
//				{
//					netweight -= 10;
//				}
//				else
//				{
//				    netweight = 5000;   
//				}  
				if(netweight > -4990)
				{
					netweight -= 10;
				}
				else
				{
				    netweight = 5000;   
				}	
						     
			    LCD_Display_Seting_Net_Weight(); 
		    }
			if(Menu == (MAXVOLUME+SECOND_BASE))
			{
			  if(longtimes<10)
				{
					if(maxvolume > (MAXVOLUME_B + MAXVOLUME_STEP_M))
					{
					    maxvolume -= MAXVOLUME_STEP_M;
					}
					else
					{
					    maxvolume = MAXVOLUME_T;
					}
				}
				else
				{
					if(maxvolume > (MAXVOLUME_B + MAXVOLUME_STEP_L))
					{
					    maxvolume -= MAXVOLUME_STEP_L;
					}
					else
					{
					    maxvolume = MAXVOLUME_T;
					}
				}
				LCD_Display_Seting_maxvolume();
			}
		    if(Menu == (PRESSURE1+SECOND_BASE))
			{
			    if(pressure1>100)
				{
				    pressure1 -= 100;
				}
				else
				{
				    pressure1 = 6000;
				}
				LCD_Display_Seting_pressure1adj();
			}
			if(Menu == (PRESSURE2+SECOND_BASE))
			{
			    if(pressure2>100)
				{
				    pressure2 -= 100;
				}
				else
				{
				    pressure2 = 6000;
				}
				LCD_Display_Seting_pressure2adj();
			}
//			if(Menu==SIM)
//			{
//					if(Sim_Num_Index==1)
//					{
//						if(Sim_Num1<999)
//						{
//						Sim_Num1 +=10;
//						}
//						else
//						{
//						Sim_Num1 =130;
//						}
//					}
//					if(Sim_Num_Index==2)
//					{
//						if(Sim_Num2<9999)
//						{
//						Sim_Num2 +=10;
//						}
//						else
//						{
//						Sim_Num2 =1111;
//						}
//					}
//					if(Sim_Num_Index==3)
//					{
//						if(Sim_Num3<9999)
//						{
//						Sim_Num3 +=10;
//						}
//						else
//						{
//						Sim_Num3 =1111;
//						}
//					}
//					LCD_Sim_Num();
//			}
		}
	}
      
   if(Trig2 & KEY_ENTER)
		{
			    KEY_ENTER_Handle(); 
					times =0;
		}
//    if(Count & KEY_ENTER)            	
//	{
//		if(Menu == ABOUT)
//		{
//			if(num <100)
//			{
//			    num++;	
//			}
//			else
//		    {
//		    	num = 0;
//		    	Saveto_Deflaut();
//	    	}	
//		}
      
  if(Trig2 & KEY_MENU)
	{
		KEY_MENU_Handle();
	}
   if(Count2 & KEY_MENU)
	{
		times++;
		if(times >= 200)
		{
			times = 0;
			if(Menu == MENU)
			{
				Login_Flag = 0;		//清除登陆标志
				Menu = LOGIN;       //切换到登陆页面
				LCD_Display_Login();//显示登陆页面	
			}
		}
	}                     
}

/*默认显示页面 */
extern uint16_t CO2_tem;
extern uint16_t tem1,RH1;					//温湿度传感器数据
extern uint8_t I2C_Data[10],ret;		//光照数据
extern uint8_t Freq_433_Slave_Recv_Flag;  
extern char  date[6];
extern u16 wind_rank_display;				//风力传感器数据
extern float Voltage;
void LCD_Display_First()			//初始第一页
{
   char s[7];
//  uint8_t i=0;
   /*显示重量等信息*/
   if(Display_Presure == 0)     
   {
	   LCD_Display_String(0, 13, "KG");
	   LCD_Display_Char(1,13,'L');
	   //LCD_Display_String(2, 13, "NM3");
	   LCD_Display_Char(2,13,'%'); 
		 LCD_Display_String(3, 13, "MPA");
	   LCD_Display_String(0, 0, gas.name);
	   
	   sprintf(s, "%6d", gas.weight);
	   LCD_Display_String(0, 7, s);
	   
	   sprintf(s, "%6d", gas.volume);
	   LCD_Display_String(1, 7, s);
	     
//	   sprintf(s, "%6d", gas.NM3);
//	   LCD_Display_String(2, 7, s);
		 
		 sprintf(s, "%1.2f", gas.MPA);
	   LCD_Display_String(3, 9, s);
	   
	   sprintf(s, "%2.1f", (float)gas.percentage/10.0);
	   LCD_Display_String(2, 9, s);
		 
		 
   }
   /*显示压力信息*/
   else 
   {
       LCD_Display_String(0, 13, "RH%");
	   LCD_Display_String(1, 13, "C");
 	   LCD_Display_String(2, 13, "LX");
		 //LCD_Display_String(3, 6, "Level");
		//LCD_Display_String(3, 13, "LX");
	   LCD_Display_String(0, 0, gas.name);
	   	   
	   sprintf(s, "%4.1f", ((float)RH1/10.0));
	   LCD_Display_String(0, 6, s);
	   
	   sprintf(s, "%4.1f", ((float)tem1/10.0));
	   LCD_Display_String(1, 6, s);
		 
//		 sprintf(s, "%4.1f", Voltage);
//	   LCD_Display_String(2, 1, s);
	     
	   sprintf(s, "%4.1f", (float)((I2C_Data[0]<<8)+I2C_Data[1])/(1.2*2));				//光照
	   LCD_Display_String(2, 7, s);

		 sprintf(s, "%2d LEVEL", wind_rank_display);				//风速
	   LCD_Display_String(3, 7, s);
		 
		 
		 
		 
		 if(Freq_433_Slave_Recv_Flag)
		 {
			 LCD_Screen_Clear();
//			 sprintf(s, "%s", date);
//			 LCD_Display_String(3, 1, s);
			 LCD_Display_String(3, 1, "TEST OK");
			 Delay_ms(500);
//			 for(i=0;i<9;i++)
//			 {
//					date[i] =0;
//			 }
			 Freq_433_Slave_Recv_Flag =0;
		 }
			else
			{
				LCD_Display_String(3, 1, "NODATA");
			}
		

//		 if(Freq_433_send_flag==0)
//		 {
//				LCD_Display_String(3, 1, "NO SEND");
//		 }
   }

   /***重装标志$显示****/
   if(Load_Flag >= 1)
   {
   	   LCD_Display_Char_Reverse(3, 0, '$');
   }
   else
   {
       LCD_Display_Char(3, 0, ' ');
   }
}



void LCD_Display_Seting(void)
{
    menu2_index = 0;    
    

	switch(seting_index)
	{
	    case 1:
		    //LCD_Screen_Clear();
		    LCD_Display_String_Reverse(0, 0, Seting_table[0]);
	        LCD_Display_String(1, 0, Seting_table[1]); 
	        LCD_Display_String(2, 0, Seting_table[2]); 
	        LCD_Display_String(3, 0, Seting_table[3]); 
		    break;
		case 2:
		    //LCD_Screen_Clear();
            LCD_Display_String(0, 0, Seting_table[0]);
	        LCD_Display_String_Reverse(1, 0, Seting_table[1]); 
	        LCD_Display_String(2, 0, Seting_table[2]); 
	        LCD_Display_String(3, 0, Seting_table[3]); 
		    break;
		case 3:
		    //LCD_Screen_Clear();
		    LCD_Display_String(0, 0, Seting_table[0]);
	        LCD_Display_String(1, 0, Seting_table[1]); 
	        LCD_Display_String_Reverse(2, 0, Seting_table[2]);
	        LCD_Display_String(3, 0, Seting_table[3]); 
		    break;
		case 4:
		    //LCD_Screen_Clear();
		    LCD_Display_String(0, 0, Seting_table[0]);
	        LCD_Display_String(1, 0, Seting_table[1]); 
	        LCD_Display_String(2, 0, Seting_table[2]); 
	        LCD_Display_String_Reverse(3, 0, Seting_table[3]); 
		    break;
		case 5:
		    //LCD_Screen_Clear();
		    LCD_Display_String_Reverse(0, 0, Seting_table[4]);
            LCD_Display_String(1, 0, Seting_table[5]);
            LCD_Display_String(2, 0, Seting_table[6]);
            LCD_Display_String(3, 0, Seting_table[7]);
		    break;
        case 6:
		    //LCD_Screen_Clear();
		    LCD_Display_String(0, 0, Seting_table[4]);
            LCD_Display_String_Reverse(1, 0, Seting_table[5]);
            LCD_Display_String(2, 0, Seting_table[6]);
            LCD_Display_String(3, 0, Seting_table[7]);
		    break;
        case 7:
		    //LCD_Screen_Clear();
		    LCD_Display_String(0, 0, Seting_table[4]);
            LCD_Display_String(1, 0, Seting_table[5]);
            LCD_Display_String_Reverse(2, 0, Seting_table[6]);
            LCD_Display_String(3, 0, Seting_table[7]);
		    break;
		case 8:
		    //LCD_Screen_Clear();
		    LCD_Display_String(0, 0, Seting_table[4]);
            LCD_Display_String(1, 0, Seting_table[5]);
            LCD_Display_String(2, 0, Seting_table[6]);
            LCD_Display_String_Reverse(3, 0, Seting_table[7]);
		    break;
		default:
		    break;
	}

}

/***********屏幕固定位置显示整形变量************/

void LCD_Display_int(uint8_t x, uint8_t y, uint32_t uint)
{
    sprintf(strtemp, "%8d", uint);

	LCD_Display_String(x, y, strtemp);

}

/*********************************************/


void LCD_Display_Seting_Server(void)
{
    
    menu2_index = 0;  
  
    //LCD_Screen_Clear();
    LCD_Display_String(0, 0, "Server IP:");
    LCD_Display_String(2, 0, "Port:");

	sprintf(serverip_input, "%03d.%03d.%03d.%03d",((Server_IP>>24)&0xFF),((Server_IP>>16)&0xFF),((Server_IP>>8)&0xFF),(Server_IP & 0xFF));
	sprintf(port_input, "%04d", Server_Port);

	LCD_Display_String(1, 0, serverip_input);
    LCD_Display_String(3, 6, port_input);
	
	LCD_Display_Char_Reverse(1, 0, serverip_input[0]);	
}

/*压力设置菜单页*/
void LCD_Display_Seting_Pressure(void)
{
    switch(pressure_index)
    {

    case 1:
      LCD_Display_String_Reverse(0, 0, pressure_table[0]);
      LCD_Display_String(1, 0, pressure_table[1]); 
	  LCD_Display_String(2, 0, "                "); 
	  LCD_Display_String(3, 0, "                ");
      break;
    case 2:
     LCD_Display_String(0, 0, pressure_table[0]);
     LCD_Display_String_Reverse(1, 0, pressure_table[1]);
	 LCD_Display_String(2, 0, "                "); 
	 LCD_Display_String(3, 0, "                "); 
      break;
    default:
      break;
    }
}

void LCD_Display_Seting_pressure1adj(void)
{
    char str[7];	

	LCD_Display_String(0, 0, "Pressure1:");
	LCD_Display_String(1, 11, "Kpa");
	LCD_Display_String(2, 2, "Adjust?");
	LCD_Display_String(3, 0, "Cancle");
	LCD_Display_String(3, 14, "OK");

	sprintf(str, "%4d", pressure1);
	LCD_Display_String(1, 6, str);
	//LCD_Display_Char_Reverse(1 , 6, pressure1_input[pressure1_index]);
}

void LCD_Display_Seting_pressure2adj(void)
{   
    char str[7];
	LCD_Display_String(0, 0, "Pressure2:");
	LCD_Display_String(1, 11, "Kpa");
	LCD_Display_String(2, 2, "Adjust?");
	LCD_Display_String(3, 0, "Cancle");
	LCD_Display_String(3, 14, "OK");

	sprintf(str, "%4d", pressure2);
	LCD_Display_String(1, 6, str);
	//LCD_Display_Char_Reverse(1 , 6, pressure2_input[pressure2_index]);
}

/*************************************重量相关*******************************************/
/*重量设置菜单页*/
void LCD_Display_Seting_Weight(void)
{
    switch(weight_index)
    {
    case 1:
     LCD_Display_String_Reverse(0, 0, weight_table[0]);
     LCD_Display_String(1, 0, weight_table[1]); 
     LCD_Display_String(2, 0, weight_table[2]); 
     LCD_Display_String(3, 0, weight_table[3]);
      break;
    case 2:
      LCD_Display_String(0, 0, weight_table[0]);
     LCD_Display_String_Reverse(1, 0, weight_table[1]); 
     LCD_Display_String(2, 0, weight_table[2]); 
     LCD_Display_String(3, 0, weight_table[3]);
      break;
    case 3:
      LCD_Display_String(0, 0, weight_table[0]);
     LCD_Display_String(1, 0, weight_table[1]); 
     LCD_Display_String_Reverse(2, 0, weight_table[2]); 
     LCD_Display_String(3, 0, weight_table[3]);
      break;
    case 4:
      LCD_Display_String(0, 0, weight_table[0]);
     LCD_Display_String(1, 0, weight_table[1]); 
     LCD_Display_String(2, 0, weight_table[2]); 
     LCD_Display_String_Reverse(3, 0, weight_table[3]);
      break;
	case 5:
      LCD_Display_String_Reverse(0, 0, weight_table[4]);
     LCD_Display_String(1, 0, weight_table[5]); 
     LCD_Display_String(2, 0, "                "); 
     LCD_Display_String(3, 0, "                ");
      break;
	case 6:
      LCD_Display_String(0, 0, weight_table[4]);
     LCD_Display_String_Reverse(1, 0, weight_table[5]); 
     LCD_Display_String(2, 0, "                "); 
     LCD_Display_String(3, 0, "                ");
      break;
    default:
      break;
    }      
}


/*显示量程选择页面*/
void LCD_Display_Seting_Weightrange(void)
{
    uint8_t i;
	//LCD_Screen_Clear();
	LCD_Display_String(0, 0, "T-weight:       ");
	for(i=0; i<6; i++)
	{
	    LCD_Display_String(weight_range[i].x, weight_range[i].y, weight_range[i].name);
	}
	i = weight_range_index; 
	LCD_Display_String_Reverse(weight_range[i].x, weight_range[i].y, weight_range[i].name);		    
}

void LCD_Display_Seting_Weightweight(void)
{
    char str[6];    

	LCD_Display_String(1, 0, "Weight Value:");
	LCD_Display_String(2, 7, "KG");

	sprintf(str, "%5d", weight_value);
	LCD_Display_String(2, 2, str);
	    
}

void LCD_Display_Seting_Weight0adj(void)
{

	char str[7];	

	LCD_Display_String(0, 0, "Low W:");
	LCD_Display_String(1, 11, "Kg");
	LCD_Display_String(2, 2, "Adjust?");
	LCD_Display_String(3, 0, "Cancle");
	LCD_Display_String(3, 14, "OK");

	sprintf(str, "%5d", weight1);
	LCD_Display_String(1, 6, str);
}

void LCD_Display_Seting_Weightwadj(void)
{
	char str[7];	

	LCD_Display_String(0, 0, "High W:");
	LCD_Display_String(1, 11, "Kg");
	LCD_Display_String(2, 2, "Adjust?");
	LCD_Display_String(3, 0, "Cancle");
	LCD_Display_String(3, 14, "OK");

	sprintf(str, "%5d", weight2);
	LCD_Display_String(1, 6, str);
}

//void LCD_Display_Seting_Net_Weight(void)
//{
//    char pstr[7];
//	LCD_Display_String(0, 1, "Net Weight:");
//	LCD_Display_String(1, 14, "Kg");
//	sprintf(pstr,"%5d", Board.netweight);
//
//	LCD_Display_String(1, 9, pstr);
//
//	if(netweight == 1)
//	{
//	    LCD_Display_String_Reverse(2, 4, "active?");
//	}
//	else
//	{
//	    LCD_Display_String_Reverse(2, 4, "clear? ");
//	}
//	
//	LCD_Display_String(3, 0, "Cancle");
//	LCD_Display_String(3, 14, "OK");
//}

void LCD_Display_Seting_Net_Weight(void)
{
    char pstr[7];
	LCD_Display_String(0, 1, "Adjustment:");
	LCD_Display_String(1, 14, "Kg");
	sprintf(pstr,"%4d", netweight);
	LCD_Display_String(1, 9, "     ");
	LCD_Display_String(1, 9, pstr);
	
	LCD_Display_String(3, 0, "Cancle");
	LCD_Display_String(3, 14, "OK");
}

/*显示最大容积修改页面*/
void LCD_Display_Seting_maxvolume(void)
{
    char str[6];    

	LCD_Display_String(1, 0, "MaxVolume:");
	LCD_Display_String(2, 8, "L");

	sprintf(str, "%6d", maxvolume);
	LCD_Display_String(2, 2, str);
	    
}
/**********************************************************************************************************/

void LCD_Display_Seting_Timer(void)
{
    uint8_t i;
	LCD_Screen_Clear();
	
	for(i=0; i<9; i++)
	{
	    LCD_Display_String(timer_intervel[i].x, timer_intervel[i].y, timer_intervel[i].time);
	}
	i = timer_interval_index; 
	LCD_Display_String_Reverse(timer_intervel[i].x, timer_intervel[i].y, timer_intervel[i].time);		    
}


/*********************set board id**********************************************************/

void LCD_Display_Seting_ID(void)
{
    char str[9];
	
	sprintf(str, "ID:%05d", board_id);

	LCD_Display_String(2, 4, str);

}
/*************************************************************************************************************/
void LCD_Display_Seting_Default(void)
{
    LCD_Display_String(1, 0, "Setting Default?");
	LCD_Display_String(3, 0, "Cancle");
	LCD_Display_String(3, 14, "OK");
}

void LCD_Display_Solenoid(void)
{
    if(Solenoid == 0)
	{
	    LCD_Display_String(1, 4, "CLOSE");
	}
	else
	{
	    LCD_Display_String(1, 4, "OPEN ");
	}

}

void LCD_Display_Login(void)
{
    pwd_index = 0;
	LCD_Screen_Clear();
	LCD_Display_String(1, 0, "Passward:");
	LCD_Display_String(2, 5, pwd_input);
	LCD_Display_Char_Reverse(2, 5, pwd_input[0]);    
}

/******************************************************************************************************************/

void Set_Deflaut(void)
{
    uint16_t temp;

	/*板卡信息 1 配置 非1 未配置*/
    uint16_t Config_Flag;
	
	EE_ReadVariable(VirtAddVarTab[19], &Config_Flag);
	
	if(Config_Flag != 1)
	{
	    Board.serverIP = SERVERIP;
		Board.port = PORT;
		Board.gasindex = GASINDEX;
		Board.boardid = BOARDID;
		Board.intervalindex = INTERVALINDEX;
		Board.weightvalue = WEIGHTVALUE;
		Board.weightzerocode = WEIGHTZERO;

		Board.weight1 = WEIGHT1VALUE;
		Board.weight2 = WEIGHT2VALUE;
		Board.weightweightcode = WEIGHTWEIGHT;
		Board.weightrangeindex = TWEIGHTINDEX;
		Board.netweight = NETWEIGHT;
		Board.maxvolume = MAXVOLUMEVALUE;
		Board.pressure1 = PRESSURE1VALUE;
	    Board.pressure2 = PRESSURE1VALUE;
		Board.pressure1code = PRESSURE1CODE;
		Board.pressure2code = PRESSURE2CODE;
		Board.rootpwd = ROOTPASSWARD;
		Board.guestpwd = GUESTPASSWARD;
		Board.version = VERSION;

		EE_WriteVariable(VirtAddVarTab[0], Board.gasindex);
		EE_WriteVariable(VirtAddVarTab[1], (Board.serverIP>>16)&0xFFFF);
		EE_WriteVariable(VirtAddVarTab[2], (Board.serverIP)&0xFFFF);
		EE_WriteVariable(VirtAddVarTab[3], Board.port);
		EE_WriteVariable(VirtAddVarTab[4], Board.intervalindex);
		EE_WriteVariable(VirtAddVarTab[5], Board.weightrangeindex);	 
		EE_WriteVariable(VirtAddVarTab[6], Board.weightvalue);
		EE_WriteVariable(VirtAddVarTab[7], Board.boardid);
	   
		EE_WriteVariable(VirtAddVarTab[8], (Board.weightzerocode>>16)&0xFFFF);
		EE_WriteVariable(VirtAddVarTab[9], (Board.weightzerocode)&0xFFFF);
	
		EE_WriteVariable(VirtAddVarTab[10], (Board.weightweightcode>>16)&0xFFFF);
		EE_WriteVariable(VirtAddVarTab[11], (Board.weightweightcode)&0xFFFF);
	
		EE_WriteVariable(VirtAddVarTab[12], Board.pressure1);
		EE_WriteVariable(VirtAddVarTab[13], Board.pressure2);
	
		EE_WriteVariable(VirtAddVarTab[14], (Board.pressure1code>>16)&0xFFFF);
		EE_WriteVariable(VirtAddVarTab[15], (Board.pressure1code)&0xFFFF);
	
		EE_WriteVariable(VirtAddVarTab[16], (Board.pressure2code>>16)&0xFFFF);
		EE_WriteVariable(VirtAddVarTab[17], (Board.pressure2code)&0xFFFF);

		EE_WriteVariable(VirtAddVarTab[18], Board.netweight);

//		EE_WriteVariable(VirtAddVarTab[20], (Board.maxvolume>>16)&0xFFFF);
//	    EE_WriteVariable(VirtAddVarTab[21], (Board.maxvolume)&0xFFFF); 

//		EE_WriteVariable(VirtAddVarTab[22], Board.weight1);
//		EE_WriteVariable(VirtAddVarTab[23], Board.weight2);
		
		EE_WriteVariable(VirtAddVarTab[21], (Board.diffpressure1>>16)&0xFFFF); 
		EE_WriteVariable(VirtAddVarTab[22], (Board.diffpressure1)&0xFFFF);
	
		EE_WriteVariable(VirtAddVarTab[23], (Board.diffpressure2>>16)&0xFFFF); 
		EE_WriteVariable(VirtAddVarTab[24], (Board.diffpressure2)&0xFFFF);
	
	
		EE_WriteVariable(VirtAddVarTab[25], (Board.diffpressure1code>>16)&0xFFFF);
		EE_WriteVariable(VirtAddVarTab[26], (Board.diffpressure1code)&0xFFFF);
	
		EE_WriteVariable(VirtAddVarTab[27], (Board.diffpressure2code>>16)&0xFFFF);
		EE_WriteVariable(VirtAddVarTab[28], (Board.diffpressure2code)&0xFFFF);

	    //Saveto_Deflaut();
	    //Config_Deflaut();
		Config_Flag = 1;
		EE_WriteVariable(VirtAddVarTab[19], Config_Flag);	
	} 
	else	
	{
		Board.rootpwd = ROOTPASSWARD;
		Board.guestpwd = GUESTPASSWARD;
		Board.version = VERSION;
	    EE_ReadVariable(VirtAddVarTab[0], &(Board.gasindex));
		EE_ReadVariable(VirtAddVarTab[1], &temp);
		Board.serverIP = temp;
		EE_ReadVariable(VirtAddVarTab[2], &temp);
		Board.serverIP = ((Board.serverIP<<16)|temp);
		EE_ReadVariable(VirtAddVarTab[3], &(Board.port));
		EE_ReadVariable(VirtAddVarTab[4], &(Board.intervalindex));
		EE_ReadVariable(VirtAddVarTab[5], &(Board.weightrangeindex));	 
		EE_ReadVariable(VirtAddVarTab[6], &(Board.weightvalue));
		EE_ReadVariable(VirtAddVarTab[7], &(Board.boardid));
//		Board.boardid =4000;	
		EE_ReadVariable(VirtAddVarTab[8], &temp);
		Board.weightzerocode = temp;
		EE_ReadVariable(VirtAddVarTab[9], &temp);
		Board.weightzerocode = ((Board.weightzerocode<<16)|temp);
	
		EE_ReadVariable(VirtAddVarTab[10], &temp);
		Board.weightweightcode = temp;
		EE_ReadVariable(VirtAddVarTab[11], &temp);
		Board.weightweightcode = ((Board.weightweightcode<<16)|temp);
	
		EE_ReadVariable(VirtAddVarTab[12], &(Board.pressure1));
		EE_ReadVariable(VirtAddVarTab[13], &(Board.pressure2));
	
		EE_ReadVariable(VirtAddVarTab[14], &temp);
		Board.pressure1code = temp;
		EE_ReadVariable(VirtAddVarTab[15], &temp);
		Board.pressure1code = ((Board.pressure1code<<16)|temp);
	
		EE_ReadVariable(VirtAddVarTab[16], &temp);
		Board.pressure2code = temp;
		EE_ReadVariable(VirtAddVarTab[17], &temp);
		Board.pressure2code = ((Board.pressure2code<<16)|temp);

		EE_ReadVariable(VirtAddVarTab[18], &temp);
		Board.netweight = (int16_t)temp;
		
		/**压差传感器上电数据读取**/
		EE_ReadVariable(VirtAddVarTab[21], &temp);
		Board.diffpressure1 = temp;
		EE_ReadVariable(VirtAddVarTab[22], &temp);
		Board.diffpressure1 = ((Board.diffpressure1<<16)|temp);
	
		EE_ReadVariable(VirtAddVarTab[23], &temp);
		Board.diffpressure2 = temp;
		EE_ReadVariable(VirtAddVarTab[24], &temp);
		Board.diffpressure2 = ((Board.diffpressure2<<16)|temp);
	
		EE_ReadVariable(VirtAddVarTab[25], &temp);
		Board.diffpressure1code = temp;
		EE_ReadVariable(VirtAddVarTab[26], &temp);
		Board.diffpressure1code = ((Board.diffpressure1code<<16)|temp);
	
		EE_ReadVariable(VirtAddVarTab[27], &temp);
		Board.diffpressure2code = temp;
		EE_ReadVariable(VirtAddVarTab[28], &temp);
		Board.diffpressure2code = ((Board.diffpressure2code<<16)|temp);
		
	}
	//coefficient_weight(Board.weight1, Board.weight2, Board.weightzerocode, Board.weightweightcode, &Weight_K);
	coefficient_diffpressure(Board.diffpressure1, Board.diffpressure2, Board.diffpressure1code, Board.diffpressure2code, &diffpressure_K);
	coefficient_pressure(Board.pressure1, Board.pressure2, Board.pressure1code, Board.pressure2code, &Pressure_K);
}


void Config_Deflaut(void)
{
  uint8_t base = 40;
  uint16_t temp;

	Board.rootpwd = ROOTPASSWARD;
	Board.guestpwd = GUESTPASSWARD;
	Board.version = VERSION;
  EE_ReadVariable(VirtAddVarTab[base+0], &(Board.gasindex));
	EE_ReadVariable(VirtAddVarTab[base+1], &temp);
	Board.serverIP = temp;
	EE_ReadVariable(VirtAddVarTab[base+2], &temp);
	Board.serverIP = ((Board.serverIP<<16)|temp);
	EE_ReadVariable(VirtAddVarTab[base+3], &(Board.port));
	EE_ReadVariable(VirtAddVarTab[base+4], &(Board.intervalindex));
	EE_ReadVariable(VirtAddVarTab[base+5], &(Board.weightrangeindex));	 
	EE_ReadVariable(VirtAddVarTab[base+6], &(Board.weightvalue));
	EE_ReadVariable(VirtAddVarTab[base+7], &(Board.boardid));    
	EE_ReadVariable(VirtAddVarTab[base+8], &temp);
	Board.weightzerocode = temp;
	EE_ReadVariable(VirtAddVarTab[base+9], &temp);
	Board.weightzerocode = ((Board.weightzerocode<<16)|temp);
	
	EE_ReadVariable(VirtAddVarTab[base+10], &temp);
	Board.weightweightcode = temp;
	EE_ReadVariable(VirtAddVarTab[base+11], &temp);
	Board.weightweightcode = ((Board.weightweightcode<<16)|temp);
	
	EE_ReadVariable(VirtAddVarTab[base+12], &(Board.pressure1));
	EE_ReadVariable(VirtAddVarTab[base+13], &(Board.pressure2));
	
	EE_ReadVariable(VirtAddVarTab[base+14], &temp);
	Board.pressure1code = temp;
	EE_ReadVariable(VirtAddVarTab[base+15], &temp);
	Board.pressure1code = ((Board.pressure1code<<16)|temp);
	
	EE_ReadVariable(VirtAddVarTab[base+16], &temp);
	Board.pressure2code = temp;
	EE_ReadVariable(VirtAddVarTab[base+17], &temp);
	Board.pressure2code = ((Board.pressure2code<<16)|temp);

	EE_ReadVariable(VirtAddVarTab[base+18], &temp);
	Board.netweight = (int16_t)temp;
	
	EE_ReadVariable(VirtAddVarTab[base+20], &temp);
	Board.maxvolume = temp;
	EE_ReadVariable(VirtAddVarTab[base+21], &temp);
	Board.maxvolume = ((Board.maxvolume<<16)|temp);

	EE_ReadVariable(VirtAddVarTab[base+22], &(Board.weight1));
	EE_ReadVariable(VirtAddVarTab[base+23], &(Board.weight2));

  
	EE_WriteVariable(VirtAddVarTab[0], Board.gasindex);
	EE_WriteVariable(VirtAddVarTab[1], (Board.serverIP>>16)&0xFFFF);
	EE_WriteVariable(VirtAddVarTab[2], (Board.serverIP)&0xFFFF);
	EE_WriteVariable(VirtAddVarTab[3], Board.port);
	EE_WriteVariable(VirtAddVarTab[4], Board.intervalindex);
	EE_WriteVariable(VirtAddVarTab[5], Board.weightrangeindex);	 
	EE_WriteVariable(VirtAddVarTab[6], Board.weightvalue);
	EE_WriteVariable(VirtAddVarTab[7], Board.boardid);
   
	EE_WriteVariable(VirtAddVarTab[8], (Board.weightzerocode>>16)&0xFFFF);
	EE_WriteVariable(VirtAddVarTab[9], (Board.weightzerocode)&0xFFFF);

	EE_WriteVariable(VirtAddVarTab[10], (Board.weightweightcode>>16)&0xFFFF);
	EE_WriteVariable(VirtAddVarTab[11], (Board.weightweightcode)&0xFFFF);

	EE_WriteVariable(VirtAddVarTab[12], Board.pressure1);
	EE_WriteVariable(VirtAddVarTab[13], Board.pressure2);

	EE_WriteVariable(VirtAddVarTab[14], (Board.pressure1code>>16)&0xFFFF);
	EE_WriteVariable(VirtAddVarTab[15], (Board.pressure1code)&0xFFFF);

	EE_WriteVariable(VirtAddVarTab[16], (Board.pressure2code>>16)&0xFFFF);
	EE_WriteVariable(VirtAddVarTab[17], (Board.pressure2code)&0xFFFF);

	EE_WriteVariable(VirtAddVarTab[18], Board.netweight);

	EE_WriteVariable(VirtAddVarTab[20], (Board.maxvolume>>16)&0xFFFF);
    EE_WriteVariable(VirtAddVarTab[21], (Board.maxvolume)&0xFFFF); 

	EE_WriteVariable(VirtAddVarTab[22], Board.weight1);
	EE_WriteVariable(VirtAddVarTab[23], Board.weight2);

	coefficient_weight(Board.weight1, Board.weight2, Board.weightzerocode, Board.weightweightcode, &Weight_K);
	coefficient_pressure(Board.pressure1, Board.pressure2, Board.pressure1code, Board.pressure2code, &Pressure_K);
}

void Saveto_Deflaut(void)
{
	uint8_t base = 40;

	LCD_Screen_Clear();
	LCD_Display_String(2, 2, "Saveing...");
	Delay(500);
  
	EE_WriteVariable(VirtAddVarTab[base+0], Board.gasindex);
	EE_WriteVariable(VirtAddVarTab[base+1], (Board.serverIP>>16)&0xFFFF);
	EE_WriteVariable(VirtAddVarTab[base+2], (Board.serverIP)&0xFFFF);
	EE_WriteVariable(VirtAddVarTab[base+3], Board.port);
	EE_WriteVariable(VirtAddVarTab[base+4], Board.intervalindex);
	EE_WriteVariable(VirtAddVarTab[base+5], Board.weightrangeindex);	 
	EE_WriteVariable(VirtAddVarTab[base+6], Board.weightvalue);
	EE_WriteVariable(VirtAddVarTab[base+7], Board.boardid);
   
	EE_WriteVariable(VirtAddVarTab[base+8], (Board.weightzerocode>>16)&0xFFFF);
	EE_WriteVariable(VirtAddVarTab[base+9], (Board.weightzerocode)&0xFFFF);

	EE_WriteVariable(VirtAddVarTab[base+10], (Board.weightweightcode>>16)&0xFFFF);
	EE_WriteVariable(VirtAddVarTab[base+11], (Board.weightweightcode)&0xFFFF);

	EE_WriteVariable(VirtAddVarTab[base+12], Board.pressure1);
	EE_WriteVariable(VirtAddVarTab[base+13], Board.pressure2);

	EE_WriteVariable(VirtAddVarTab[base+14], (Board.pressure1code>>16)&0xFFFF);
	EE_WriteVariable(VirtAddVarTab[base+15], (Board.pressure1code)&0xFFFF);

	EE_WriteVariable(VirtAddVarTab[base+16], (Board.pressure2code>>16)&0xFFFF);
	EE_WriteVariable(VirtAddVarTab[base+17], (Board.pressure2code)&0xFFFF);

	EE_WriteVariable(VirtAddVarTab[base+18], Board.netweight);

	EE_WriteVariable(VirtAddVarTab[base+20], (Board.maxvolume>>16)&0xFFFF);
	EE_WriteVariable(VirtAddVarTab[base+21], (Board.maxvolume)&0xFFFF);

	EE_WriteVariable(VirtAddVarTab[base+22], Board.weight1);
	EE_WriteVariable(VirtAddVarTab[base+23], Board.weight2);

	LCD_Display_String(2, 2, "Save ok!  ");
	Delay(500);
	LCD_Screen_Clear();
	LCD_Display_String(1, 0, "Product Info:");
    LCD_Display_String(2, 0, Board.version);

}
/******************************************************************************************************************/

/******************************************************************************************************************/

void KEY_LOAD_Handle(void)
{
    if(Menu == HEAD_PAGE)
	{
	    if(Load_Flag == 0)
		{
			W_Load_Before = gas.weight;
			Load_Flag = 1;
			LCD_Screen_Clear();
			LCD_Display_String(1, 0, "Filling Data");
			LCD_Display_String(2, 0, "Sending ... ");			
			//发送重装前数据
		}
		if(Load_Flag == 2)
		{
			W_Load_After = gas.weight;
			W_Load = W_Load_After -	W_Load_Before;
			Load_Flag = 3;
			LCD_Screen_Clear();
			LCD_Display_String(1, 0, "Finish Data ");
			LCD_Display_String(2, 0, "Sending ... ");
			//发送重装后数据
		}
		LCD_Task = 0;
		Menu = NOMENU; 
	}
}

void KEY_UP_Handle(void)
{
    switch(Menu)
    {
    case HEAD_PAGE:
	if(Login_Flag ==1)
      gas_index_add();
      break;
    case MENU:
      seting_index_add();
      break;
     case SERVER:
	 if(Login_Flag ==1)
      IP_port_add();
      break;
    case PRESSURE:
	  pressure_index_add();
      break;
    case WEIGHT:
	weight_index_add();
      break;
    case TIMER:
	if(Login_Flag ==1)
	timer_interval_add();
      break;
    case BID:
	if(Login_Flag ==1)
	 boardid_add();
      break;
    case Temp_set:
			Temp_Alarm_add();
      break;
    case RH_set:
			RH_Alarm_add();	
		  break;
    case LOGIN:
	    pwd_value_add();
      break;
	case ABOUT:
	    break;
    case (T_WEIGHT+SECOND_BASE):
	if(Login_Flag ==1) 
	    weight_range_add();
        break;
	case (WEIGHT_STD+SECOND_BASE):
	if(Login_Flag ==1)
	    weight_value_add();
	    break;
	case (WEIGHT_ZERO+SECOND_BASE):
	if(Login_Flag ==1)
	  weight1_value_add();
	  break;
	case (WEIGHT_CALI+SECOND_BASE):
	if(Login_Flag ==1)
	  weight2_value_add();
	  break;
	case (WEIGHT_NET+SECOND_BASE):
	if(Login_Flag ==1)
	//netweight_sw();
	netweight_value_add();
	break;
	case (MAXVOLUME+SECOND_BASE):
	if(Login_Flag ==1)
	    weight_maxvolume_add();
	    break;
    case (PRESSURE1+SECOND_BASE):
	if(Login_Flag ==1)
	    pressure1_value_add(); 
	    break;
	case (PRESSURE2+SECOND_BASE):
	if(Login_Flag ==1)   
	    pressure2_value_add();  
	    break;
	
    default:
      break;
      
    }
}

void KEY_DOWN_Handle(void)
{
    switch(Menu)
    {
    case HEAD_PAGE:
      gas_presure_display();
      break;
    case MENU:
      seting_index_dec();
      break;
      case SERVER:
	  if(Login_Flag ==1)
        IP_port_index_add();
      break;
    case PRESSURE:
	  pressure_index_dec();
      break;
    case WEIGHT:
	weight_index_dec();
      break;
    case TIMER:
	if(Login_Flag ==1)
	timer_interval_dec();
      break;
    case BID:
	if(Login_Flag ==1)
	 boardid_dec();
      break;
    case Temp_set:
			Temp_Alarm_dec();
      break;
		case RH_set:
			RH_Alarm_dec();
			break;
	case LOGIN:
	    pwd_index_add();
	    break;      
    case ABOUT:
      break;
    case (T_WEIGHT+SECOND_BASE):
	if(Login_Flag ==1) 
	weight_range_dec();
      break;
	case (WEIGHT_STD+SECOND_BASE):
	if(Login_Flag ==1)
	weight_value_dec();
	break;
	case (WEIGHT_ZERO+SECOND_BASE):
	if(Login_Flag ==1)
	  weight1_value_dec();
	  break;
	case (WEIGHT_CALI+SECOND_BASE):
	if(Login_Flag ==1)
	  weight2_value_dec();
	  break;
	case (WEIGHT_NET+SECOND_BASE):
	if(Login_Flag ==1)
	//netweight_sw();
	netweight_value_dec();
	break;
	case (MAXVOLUME+SECOND_BASE):
	if(Login_Flag ==1)
	    weight_maxvolume_dec();
	    break;
	case (PRESSURE1+SECOND_BASE):
	if(Login_Flag ==1)
	pressure1_value_dec();	
	    break;
	case (PRESSURE2+SECOND_BASE):
	if(Login_Flag ==1)
	    pressure2_value_dec();
	    break;   
    default:
      break;
	}
      
}

void KEY_ENTER_Handle(void)
{  
  switch(Menu)
   {
    case HEAD_PAGE:
	    if(Login_Flag ==1)
            gas_typesave();
       
			else
			Freq_433_send_flag=1;
			 break;
    case MENU:
      into_seting();
      break;
    case SERVER:
	if(Login_Flag ==1)                        //set server页面 enter键
	  IP_port_active();
      break;
    case PRESSURE:
	  into_seting_pressure();
      break;
    case WEIGHT:
	//if(Login_Flag ==1)
	  into_seting_weight();
      break;
    case TIMER:
	if(Login_Flag ==1)
	  timer_interval_active();
      break;
    case BID:
	if(Login_Flag ==1)
	   boardid_active();
       break;
    case Temp_set:
	if(Login_Flag ==1)
	  Temp_Alarm_Set();
      break;
	case RH_set:
		RH_Alarm_Set();
	    break;
    case LOGIN:
	    pwd_active();
	    break;  
    case ABOUT:
      break;
    case (T_WEIGHT+SECOND_BASE):
	if(Login_Flag ==1)
	  weight_range_active();
      break;
	case (WEIGHT_STD+SECOND_BASE):
    if(Login_Flag ==1)
	  //weight_value_active();
	   break;
	case (WEIGHT_ZERO+SECOND_BASE):
	if(Login_Flag ==1)
	  excute_weight_zero_adj();
	break;
	case (WEIGHT_CALI+SECOND_BASE):
	if(Login_Flag ==1)
	  excute_weight_weight_adj();
	break;
	case (WEIGHT_NET+SECOND_BASE):
	if(Login_Flag ==1)
	  excute_net_weight();
	break;
	case (MAXVOLUME+SECOND_BASE):
	if(Login_Flag ==1)
	    weight_maxvolume_active();
	    break;
	case (PRESSURE1+SECOND_BASE):
	if(Login_Flag ==1)
	pressure1_adj_active();
	break;
	case (PRESSURE2+SECOND_BASE):
	if(Login_Flag ==1)
	pressure2_adj_active();
	break;
    default:
      break;
    }
      
}

void KEY_MENU_Handle(void)
{
    switch(Menu)
    {
    case HEAD_PAGE:                        //显示气体信息页面menu键
      menulist();                   //进入菜单页面
      break;
    case MENU:                        //menu页面 menu键
	  Power_Sensorboard_On();	   //打开sensor板开关
      Menu = HEAD_PAGE;                    //返回上级菜单
      LCD_Screen_Clear();          //清屏
      LCD_Display_First();         //返回显示采集信息页面
      break;
    case SERVER:                        //set server 页面menu键
    case PRESSURE:
    case WEIGHT:
    case TIMER:
    case BID:
    case Temp_set:
		case RH_set:
    case ABOUT:
      menulist();
      break;
	case (T_WEIGHT+SECOND_BASE):
	case (WEIGHT_STD+SECOND_BASE):
	case (WEIGHT_ZERO+SECOND_BASE):
	case (WEIGHT_CALI+SECOND_BASE):
	case (WEIGHT_NET+SECOND_BASE):
	case (MAXVOLUME+SECOND_BASE):
	Menu = WEIGHT;
	LCD_Display_Seting_Weight();
	break;
	case (PRESSURE1+SECOND_BASE):
	case (PRESSURE2+SECOND_BASE):
	    Menu = PRESSURE;
		LCD_Display_Seting_Pressure();
	break;
    
    default:
      break;
      
    }
}

/*Menu=0按键处理***************************************/
/*up键按下*/
void gas_index_add(void)
{
    gas_index++;

	if(gas_index ==5)
	{
	    gas_index = 0;
	}

    gas = gas_table[gas_index];
}

/*down键按下*/
void gas_presure_display(void)
{
    Display_Presure ^= 0x01;
	
	LCD_Screen_Clear();	
}

/*enter键按下*/
void gas_typesave(void)
{
    Board.gasindex = gas_index;
	EE_WriteVariable(VirtAddVarTab[0], Board.gasindex);    
}

/*menu键按下*/
void menulist(void)
{
    Menu = MENU;

    Power_GPS_Off();
	Power_GPRS_Off();
	//Power_Sensorboard_Off();
	//LCD_Screen_Clear();
	LCD_Display_Seting();
}

/*Menu=1按键处理***************************************/



/*up键按下*/
void seting_index_add(void)
{
	seting_index++;
	if(seting_index == 9)
	{
	    seting_index = 1;
	}
        
        LCD_Display_Seting();
}

/*down键按下*/
void seting_index_dec(void)
{
	seting_index--;
	if(seting_index == 0)
	{
	    seting_index = 8;
	}
        
        LCD_Display_Seting();
}


/*enter键按下*/
void into_seting(void)
{
	Menu = MENU + seting_index;

    LCD_Screen_Clear();
	switch(Menu)
	{
	    case SERVER:
		Server_IP   = Board.serverIP;
		Server_Port = Board.port;
		LCD_Display_Seting_Server();
		break;
		
		case PRESSURE:
		pressure_index = 1;
		LCD_Display_Seting_Pressure();
		break; 
		
		case WEIGHT:
		weight_index = 1;
		LCD_Display_Seting_Weight();
		break; 
		
		case TIMER:

		timer_interval_index = Board.intervalindex;//当前的设置
		LCD_Display_Seting_Timer();
		break; 
		
		case BID:
		board_id = Board.boardid;
		LCD_Display_Seting_ID();
		break;
               
        case Temp_set:
		LCD_Display_Seting_Temp();
		break;
               
        case RH_set:
		    LCD_Display_Seting_RH();
		    break;
				
		case ABOUT:
        LCD_Display_String(1, 0, "Product Info:");
        LCD_Display_String(2, 0, Board.version);
		break; 
		
		default:
		break;  
	}
	        
}

/*Menu=2(set server)按键处理***************************************/

/*up*/
void IP_port_add(void)
{
  uint8_t i = menu2_index;  
  if(i<15)
    {
        serverip_input[i]++;
        if(serverip_input[i] == 0x3A)
        {
            serverip_input[i] = 0x30;
        }
		LCD_Display_Char_Reverse(1, menu2_index, serverip_input[i]);
        
    }
    else
    {
      i = i-15;  
      port_input[i]++;
      if(port_input[i] == 0x3A)
      {
          port_input[i] = 0x30;
      } 
	  LCD_Display_Char_Reverse(3, i+6, port_input[i]);      
    }
}

/*down*/
void IP_port_index_add(void)
{
  if(menu2_index<15)
  {
  LCD_Display_Char(1, menu2_index, serverip_input[menu2_index]); 
  }
  else
  {
      LCD_Display_Char(3, menu2_index-9, port_input[menu2_index-15]);
  }
  menu2_index++; 
  
  if((menu2_index == 3)||(menu2_index == 7)||(menu2_index == 11))
  {
      menu2_index ++; 
  }
  if(menu2_index == 19)
  {
      menu2_index = 0;
  }  
  
  if(menu2_index<15)
  {
  LCD_Display_Char_Reverse(1, menu2_index, serverip_input[menu2_index]); 
  }
  else
  {
      LCD_Display_Char_Reverse(3, menu2_index-9, port_input[menu2_index-15]);
  }
}

void IP_port_active(void)
{
      uint32_t array[4];
	  Menu = MENU;                    //返回上层菜单
      
      sscanf(serverip_input, "%d.%d.%d.%d", &array[0], &array[1], &array[2], &array[3]);
      Server_IP = (array[0]<<24)|(array[1]<<16)|(array[2]<<8)|array[3];
	  sscanf(port_input, "%d", &array[0]);
	  Server_Port = (uint16_t)array[0];
      //sscanf(port_input, "%d", &Server_Port);
      
      Board.serverIP = Server_IP;
	  Board.port = Server_Port; 
	  
	  EE_WriteVariable(VirtAddVarTab[1], (uint16_t)(Board.serverIP>>16));//存储到eeprom
      EE_WriteVariable(VirtAddVarTab[2], (uint16_t)(Board.serverIP));
      EE_WriteVariable(VirtAddVarTab[3], Board.port);
      
      LCD_Display_Seting();        //显示菜单页面
}

/*Menu=3 (set pressure)按键处理***************************************/

/*up键按下*/
void pressure_index_add(void)
{
	pressure_index++;
	if(pressure_index ==3)
	{
	    pressure_index = 1;
	}
        
    LCD_Display_Seting_Pressure();
}

/*down键按下*/
void pressure_index_dec(void)
{
	pressure_index--;
	if(pressure_index == 0)
	{
	    pressure_index = 2;
	}
        
        LCD_Display_Seting_Pressure();
}


/*enter键按下*/
void into_seting_pressure(void)
{
	Menu = (PRESSURE1+SECOND_BASE) + pressure_index - 1;;

    LCD_Screen_Clear();
	switch(pressure_index)
	{
		case 1:
		pressure1 = Board.pressure1;
		LCD_Display_Seting_pressure1adj();
		break; 
		
		case 2:
		pressure2 = Board.pressure2;
		LCD_Display_Seting_pressure2adj();
		break; 
		
		default:
		break;  
	}	        
}



/*Menu=4 (set weight)按键处理***************************************/

/*up键按下*/
void weight_index_add(void)
{
	weight_index++;
	if(weight_index == 7)
	{
	    weight_index = 1;
	}
        
        LCD_Display_Seting_Weight();
}

/*down键按下*/
void weight_index_dec(void)
{
	weight_index--;
	if(weight_index == 0)
	{
	    weight_index = 6;
	}
        
        LCD_Display_Seting_Weight();
}


/*enter键按下*/
void into_seting_weight(void)
{
	Menu = (T_WEIGHT+SECOND_BASE) + weight_index - 1;;

    LCD_Screen_Clear();
	switch(weight_index)
	{
	    case 1:
		weight_range_index = Board.weightrangeindex;
		LCD_Display_Seting_Weightrange();
		break;
		
		case 2:
		weight_value = Board.weightvalue;
			LCD_Display_Seting_Weightweight();
		break; 
		
		case 3:
		weight1 = Board.weight1;
			LCD_Display_Seting_Weight0adj();
		break; 
		
		case 4:
		weight2 = Board.weight2;
		LCD_Display_Seting_Weightwadj();
		break;

		case 5:
		//netweight = Board.netweight;
		netweight = (int16_t)(Board.netweight);
		LCD_Display_Seting_Net_Weight();
		break;

		case 6:
		maxvolume = Board.maxvolume;
		LCD_Display_Seting_maxvolume();
		break;
		
		default:
		break;  
	}	        
}

/*Menu=5 (set timer)按键处理***************************************/
/*up*/
void timer_interval_add(void)
{
	timer_interval_index++;
	if(timer_interval_index == 9)
	{
	    timer_interval_index = 0;
	}   
	     
    LCD_Display_Seting_Timer();    
}

/*down*/
void timer_interval_dec(void)
{
    if(timer_interval_index == 0)
	{
	    timer_interval_index = 8;
	}
	else
	{
	    timer_interval_index--;   
	}  	
	     
    LCD_Display_Seting_Timer();   
}

/*enter*/
void timer_interval_active(void)
{
    Board.intervalindex = timer_interval_index;
	EE_WriteVariable(VirtAddVarTab[4], Board.intervalindex);
	
	Menu = MENU;
	LCD_Display_Seting();    
}


/*Menu=6 (set id)按键处理***************************************/
void boardid_add(void)
{
    if(board_id == 65535)
	{
		board_id = 1;
	}
	else
	{
	    board_id++;
	}

	LCD_Display_Seting_ID();
}

void boardid_dec(void)
{
    if(board_id == 1)
	{
		board_id = 65535;
	}
	else
	{
	    board_id--;
	}

	LCD_Display_Seting_ID();
}

void boardid_active(void)
{
    Board.boardid = board_id;
	EE_WriteVariable(VirtAddVarTab[7], Board.boardid);
	
	Menu = MENU;
	LCD_Display_Seting();
}

/*Menu=7 (set default)按键处理***************************************/
/*温度报警设置函数*/
void LCD_Display_Seting_Temp(void)
{
    char str[7];
	LCD_Screen_Clear();					
//	if(Temp_Alarm<0)
//	{
//		temp = abs(Temp_Alarm);
//		sprintf(str, "Temp:-%02dC", temp);
//	}
//	else
//	{
//		sprintf(str, "Temp:%02dC", Temp_Alarm);
//	}
	sprintf(str, "Temp:%02dC", Temp_Alarm);
	LCD_Display_String(1, 1, str);
}



void Temp_Alarm_add(void)
{
	if(Temp_Alarm<35)
	{
		Temp_Alarm +=1;
	}
	else
	{
		Temp_Alarm =5;
	}
	LCD_Display_Seting_Temp();
}

void Temp_Alarm_dec(void)
{
	if(Temp_Alarm>5)
	{
		Temp_Alarm -=1;
	}
	else
	{
		Temp_Alarm =35;
	}
	LCD_Display_Seting_Temp();
}

void Temp_Alarm_Set()
{
//	EE_WriteVariable(VirtAddVarTab[39], Temp_Alarm);			//掉电保护
//	Soid_Time_Flag =1;
//	Soid_Time_Backup = Soid_Time;
	Menu = MENU;
	LCD_Display_Seting();//返�
}


/*Menu=8 (SOLENIOD)按键处理***************************************/
/*温度报警设置函数*/
void LCD_Display_Seting_RH(void)
{
    char str[7];
	LCD_Screen_Clear();					
//	if(Temp_Alarm<0)
//	{
//		temp = abs(Temp_Alarm);
//		sprintf(str, "Temp:-%02dC", temp);
//	}
//	else
//	{
//		sprintf(str, "Temp:%02dC", Temp_Alarm);
//	}
	sprintf(str, "RH:%02dC", RH_Alarm);
	LCD_Display_String(1, 1, str);
}



void RH_Alarm_add(void)
{
	if(RH_Alarm<90)
	{
		RH_Alarm +=1;
	}
	else
	{
		RH_Alarm =10;
	}
	LCD_Display_Seting_RH();
}

void RH_Alarm_dec(void)
{
	if(RH_Alarm>10)
	{
		RH_Alarm -=1;
	}
	else
	{
		RH_Alarm =90;
	}
	LCD_Display_Seting_RH();
}

void RH_Alarm_Set()
{
//	EE_WriteVariable(VirtAddVarTab[39], Temp_Alarm);			//掉电保护
//	Soid_Time_Flag =1;
//	Soid_Time_Backup = Soid_Time;
	Menu = MENU;
	LCD_Display_Seting();//返�
}

/*Menu=8 (Login)按键处理***************************************/
void pwd_value_add(void)
{
    uint8_t i = pwd_index;  

    //LCD_Display_Char_(2, pwd_index, pwd_input[i]);
	pwd_input[i]++;
    if(pwd_input[i] == 0x3A)
    {
        pwd_input[i] = 0x30;
    }
	LCD_Display_Char_Reverse(2, 5+pwd_index, pwd_input[i]);
        
}

/*down*/
void pwd_index_add(void)
{

  LCD_Display_Char(2, 5+pwd_index, pwd_input[pwd_index]); 

  pwd_index++; 
  
  if(pwd_index == 6)
  {
      pwd_index = 0; 
  }
  
  LCD_Display_Char_Reverse(2, 5+pwd_index, pwd_input[pwd_index]); 
}

void pwd_active(void)
{
    uint8_t i;

	if(strstr(Board.rootpwd, pwd_input)!= NULL)
	{
	    LCD_Display_String(3, 1, "Root success!");
		Delay(500);
		LCD_Screen_Clear();
		Login_Flag = 1; 
		Menu = HEAD_PAGE;   
	}
	else if(strstr(Board.guestpwd, pwd_input)!= NULL)
	{
		LCD_Display_String(3, 1, "Guest success!");
		Delay(500);
		LCD_Screen_Clear();
		Menu = HEAD_PAGE; 
	}
	else
	{
	    LCD_Display_String(3, 0, "Passward error!");
	} 

	for(i=0; i<6; i++)
	{
	    pwd_input[i] = '0';
	}
	pwd_input[6] = '\0';   
}


/*Menu=8 (About product)按键处理***************************************/
/*只支持Menu键*/


/*Menu=9 (weight range seting)按键处理***************************************/
/*up*/
void weight_range_add(void)
{
	weight_range_index++;
	if(weight_range_index == 6)
	{
	    weight_range_index = 0;
	}   
	     
    LCD_Display_Seting_Weightrange();    
}

/*down*/
void weight_range_dec(void)
{
    if(weight_range_index == 0)
	{
	    weight_range_index = 5;
	}
	else
	{
	    weight_range_index--;   
	}  	
	     
    LCD_Display_Seting_Weightrange();   
}

/*enter*/
void weight_range_active(void)
{
    Board.weightrangeindex = weight_range_index;
	EE_WriteVariable(VirtAddVarTab[5], Board.weightrangeindex);
	
	Menu = WEIGHT;
	LCD_Display_Seting_Weight();    
}

/*Menu=10 (砝码值设置)按键处理***************************************/
/*up*/
void weight_value_add(void)
{
	if(weight_value < 10200)
	{
	    weight_value += 10;
	} 
      else
      {
          weight_value = 10;
      }  
	     
    LCD_Display_Seting_Weightweight();    
}

/*down*/
void weight_value_dec(void)
{
    if(weight_value > 10)
	{
	    weight_value -= 10;
	}
	else
	{
	    weight_value = 10200;   
	}  	
	     
    LCD_Display_Seting_Weightweight();   
}

/*enter*/
void weight_value_active(void)
{
    Board.weightvalue = weight_value;
	EE_WriteVariable(VirtAddVarTab[6], Board.weightvalue);
	
	coefficient_weight(0, Board.weightvalue, Board.weightzerocode, Board.weightweightcode, &Weight_K);
	Menu = WEIGHT;
	LCD_Display_Seting_Weight();    
}

/*Menu=11 (zero adjust)按键处理***************************************/

void weight1_value_add(void)
{
    if(weight1 < weight_range[Board.weightrangeindex].range)
	{
	    weight1 += 1;
	}
	else
	{
	    weight1 = 0;
	}
	LCD_Display_Seting_Weight0adj();
        
}

/*down*/
void weight1_value_dec(void)
{
    if(weight1 > 0)
	{
	    weight1 -= 1;
	}
	else
	{
	    weight1 = weight_range[Board.weightrangeindex].range;
	}
	LCD_Display_Seting_Weight0adj();  
}


void excute_weight_zero_adj(void)
{	
	uint8_t i;	
	uint32_t adcode[2];
	
	Board.weight1 = weight1;

	EE_WriteVariable(VirtAddVarTab[22], Board.weight1); 
		
	LCD_Screen_Clear();	  
	LCD_Display_String(3, 3, "Adjust...");

	Power_Sensorboard_On();
	Delay(100);

	AD7794_Init();
	Delay(300);
	AD7794_SoftwareReset();

	adcode[0] = 0;
	for(i=0; i<8; i++)
	{
		adcode[1] = 0;
		AD7794_Channel_Configration(CHANNEL_AIN6, BIPOLAR, GAIN_128, MD_SINGLE_CONVERSION);
		AD7794_ReadResultForSingleConversion(1, adcode);
		adcode[0] += adcode[1];
	}
	adcode[0] >>= 3;

	//Power_Sensorboard_Off();

	Board.weightzerocode = adcode[0];

	EE_WriteVariable(VirtAddVarTab[8], (Board.weightzerocode>>16)&0xFFFF);
	EE_WriteVariable(VirtAddVarTab[9], (Board.weightzerocode)&0xFFFF); 
	
	coefficient_weight(Board.weight1, Board.weight2, Board.weightzerocode, Board.weightweightcode, &Weight_K);
	Menu = WEIGHT;
	LCD_Display_Seting_Weight(); 
}

/*Menu=12 (Weight adjust)按键处理***************************************/
void weight2_value_add(void)
{
    if(weight2 < weight_range[Board.weightrangeindex].range)
	{
	    weight2 += 1;
	}
	else
	{
	    weight2 = 0;
	}
	LCD_Display_Seting_Weightwadj();
        
}

/*down*/
void weight2_value_dec(void)
{
    if(weight2 > 0)
	{
	    weight2 -= 1;
	}
	else
	{
	    weight2 = weight_range[Board.weightrangeindex].range;
	}
	LCD_Display_Seting_Weightwadj();  
}
void excute_weight_weight_adj(void)
{
	uint8_t i;	
	uint32_t adcode[2];	
	Board.weight2 = weight2;

	EE_WriteVariable(VirtAddVarTab[23], Board.weight2); 
		
	LCD_Screen_Clear();	  
	LCD_Display_String(3, 3, "Adjust...");

	Power_Sensorboard_On();
	Delay(100);

	AD7794_Init();
	Delay(300);
	AD7794_SoftwareReset();

	adcode[0] = 0;
	for(i=0; i<8; i++)
	{
		adcode[1] = 0;
		AD7794_Channel_Configration(CHANNEL_AIN6, BIPOLAR, GAIN_128, MD_SINGLE_CONVERSION);
		AD7794_ReadResultForSingleConversion(1, adcode);
		adcode[0] += adcode[1];
	}
	adcode[0] >>= 3;

	//Power_Sensorboard_Off();

	Board.weightweightcode =adcode[0];

	EE_WriteVariable(VirtAddVarTab[10], (Board.weightweightcode>>16)&0xFFFF);
	EE_WriteVariable(VirtAddVarTab[11], (Board.weightweightcode)&0xFFFF); 
	
	coefficient_weight(Board.weight1, Board.weight2, Board.weightzerocode, Board.weightweightcode, &Weight_K);
	Menu = WEIGHT;
	LCD_Display_Seting_Weight(); 
} 
/*Menu=11 (zero adjust)按键处理***************************************/

//void netweight_sw(void)
//{
//    if(netweight == 0)
//	{
//	    netweight = 1;
//	}
//	else
//	{
//	    netweight = 0;
//	}
//	LCD_Display_Seting_Net_Weight();
//        
//}
//
//void excute_net_weight(void)
//{
//    uint8_t i;
//	uint32_t adcode[2];
//
//	LCD_Screen_Clear();
//	LCD_Display_String(3, 3, "active...");
//
//	if(netweight == 1)
//	{
//		Power_Sensorboard_On();
//		//Delay(300);
//		AD7794_Init();
//		Delay(500);
//		AD7794_SoftwareReset();
//	
//		for(i=0; i<3; i++)
//		{
//		AD7794_Channel_Configration(CHANNEL_AIN6, BIPOLAR, GAIN_128, MD_SINGLE_CONVERSION);
//		AD7794_ReadResultForSingleConversion(0, adcode);
//		}
//	
//		caculate_weight(adcode[0], 0, Board.weightzerocode, Weight_K);	
//		Board.netweight = gas.weight;
//		Power_Sensorboard_Off();
//		}
//	else
//	{
//		Board.netweight = 0;
//	}
//	EE_WriteVariable(VirtAddVarTab[18], (Board.netweight)&0xFFFF); 
//	
//	Menu = WEIGHT;
//	LCD_Display_Seting_Weight();   
//}

void netweight_value_add(void)
{
	if(netweight < 5000)
	{
	    netweight += 1;
	} 
      else
      {
          //netweight = 0;
		  netweight = -5000;
      }  
	     
    LCD_Display_Seting_Net_Weight();    
}

/*down*/
void netweight_value_dec(void)
{
    //if(netweight > 0)
	if(netweight > -5000)
	{
	    netweight -= 1;
	}
	else
	{
	    netweight = 5000;   
	}  	
	     
    LCD_Display_Seting_Net_Weight();   
}
void excute_net_weight(void)
{
	LCD_Screen_Clear();
	LCD_Display_String(3, 3, "active...");

	Board.netweight = netweight;
	EE_WriteVariable(VirtAddVarTab[18], (netweight)&0xFFFF); 
	
	Menu = WEIGHT;
	LCD_Display_Seting_Weight();   
}

/*Menu (最大容积设置)按键处理***************************************/
/*up*/
void weight_maxvolume_add(void)
{
	if(maxvolume < MAXVOLUME_T)
	{
	    maxvolume += MAXVOLUME_STEP_S;
	} 
      else
      {
          maxvolume = MAXVOLUME_B;
      }  
	     
    LCD_Display_Seting_maxvolume();    
}

/*down*/
void weight_maxvolume_dec(void)
{
    if(maxvolume > MAXVOLUME_B)
	{
	    maxvolume -= MAXVOLUME_STEP_S;
	}
	else
	{
	    maxvolume = MAXVOLUME_T;   
	}  		     
    LCD_Display_Seting_maxvolume();   
}

/*enter*/
void weight_maxvolume_active(void)
{
    Board.maxvolume = maxvolume;

	EE_WriteVariable(VirtAddVarTab[20], (Board.maxvolume>>16)&0xFFFF);
	EE_WriteVariable(VirtAddVarTab[21], (Board.maxvolume)&0xFFFF); 

	Menu = WEIGHT;
	LCD_Display_Seting_Weight();    
}

/* (Pressure1 adj)按键处理***************************************/

void pressure1_value_add(void)
{
    if(pressure1 < 6000)
	{
	    pressure1 += 10;
	}
	else
	{
	    pressure1 = 0;
	}
	LCD_Display_Seting_pressure1adj();
        
}

/*down*/
void pressure1_value_dec(void)
{
    if(pressure1 > 10)
	{
	    pressure1 -= 10;
	}
	else
	{
	    pressure1 = 6000;
	}
	LCD_Display_Seting_pressure1adj();  
}

void pressure1_adj_active(void)
{
	uint8_t i;
	
	uint32_t adcode1[3];
	
	//Board.pressure1 = pressure1;

	EE_WriteVariable(VirtAddVarTab[12], Board.pressure1);
	
//	LCD_Screen_Clear();	  
//	LCD_Display_String(3, 3, "Adjust...");

	Power_Sensorboard_On();
	Delay(100);

	AD7794_Init();
	Delay(300);
	AD7794_SoftwareReset();

	for(i=0;i<3;i++)
	{
	AD7794_Channel_Configration(CHANNEL_AIN1, BIPOLAR, GAIN_64, MD_SINGLE_CONVERSION);
	AD7794_ReadResultForSingleConversion(i, adcode1);
	}
	//Power_Sensorboard_Off();
	//for
	Board.pressure1code =adcode1[1];

	EE_WriteVariable(VirtAddVarTab[14], (Board.pressure1code>>16)&0xFFFF);
	EE_WriteVariable(VirtAddVarTab[15], (Board.pressure1code)&0xFFFF); 
	
    coefficient_pressure(Board.pressure1, Board.pressure2, Board.pressure1code, Board.pressure2code, &Pressure_K);

	//Menu = PRESSURE;
	//LCD_Display_Seting_Pressure();
}

/* (Pressure2 adj)按键处理***************************************/

void pressure2_value_add(void)
{
    if(pressure2 < 6000)
	{
	    pressure2 += 10;
	}
	else
	{
	    pressure2 = 0;
	}
	LCD_Display_Seting_pressure2adj();
        
}

/*down*/
void pressure2_value_dec(void)
{

    if(pressure2 > 10)
	{
	    pressure2 -= 10;
	}
	else
	{
	    pressure2 = 6000;
	}
	LCD_Display_Seting_pressure2adj();  
}

void pressure2_adj_active(void)
{
	uint8_t i;
	uint32_t adcode2[2];
	
	//Board.pressure2 = pressure2;
	EE_WriteVariable(VirtAddVarTab[13], Board.pressure2);

//	LCD_Screen_Clear();
//	LCD_Display_String(3, 3, "Adjust...");

	Power_Sensorboard_On();
	Delay(100);

	AD7794_Init();
	Delay(300);

	AD7794_SoftwareReset();

	for(i=0;i<3;i++)
	{

	AD7794_Channel_Configration(CHANNEL_AIN1, BIPOLAR, GAIN_64, MD_SINGLE_CONVERSION);
	AD7794_ReadResultForSingleConversion(1, adcode2);
	}
	//Power_Sensorboard_Off();

	Board.pressure2code =adcode2[1];

	EE_WriteVariable(VirtAddVarTab[16], (Board.pressure2code>>16)&0xFFFF);
	EE_WriteVariable(VirtAddVarTab[17], (Board.pressure2code)&0xFFFF); 
	
  coefficient_pressure(Board.pressure1, Board.pressure2, Board.pressure1code, Board.pressure2code, &Pressure_K);

	//Menu = PRESSURE;
	//LCD_Display_Seting_Pressure();
}

//
void diffpressure1_adj_active(void)
{
	uint8_t i;	
	uint32_t adcode[32];
	
	//Board.diffpressure1 = diffpressure1;

	EE_WriteVariable(VirtAddVarTab[21], (Board.diffpressure1>>16)&0xFFFF); 
	EE_WriteVariable(VirtAddVarTab[22], (Board.diffpressure1)&0xFFFF);
	
//	LCD_Screen_Clear();	  
//	LCD_Display_String(3, 3, "Adjust...");

	Power_Sensorboard_On();
	Delay(100);

	AD7794_Init();
	Delay(300);
	AD7794_SoftwareReset();

//#ifdef DIFFPRESSURE_GAIN64
//	AD7794_Channel_Configration(DIFFPRESSURE_CH, BIPOLAR, GAIN_64, MD_CONTINUOUS_CONVERSION);
//#else
	AD7794_Channel_Configration(CHANNEL_AIN6, BIPOLAR, GAIN_128, MD_CONTINUOUS_CONVERSION);
//#endif
	AD7794_ReadResultForContinuousConversion(0, 32, adcode);

	for(i=1; i<32; i++)
	{
		adcode[0]+= adcode[i] ;
	}
	adcode[0] >>= 5;

	//Power_Sensorboard_Off();

	Board.diffpressure1code =adcode[0];

	EE_WriteVariable(VirtAddVarTab[25], (Board.diffpressure1code>>16)&0xFFFF);
	EE_WriteVariable(VirtAddVarTab[26], (Board.diffpressure1code)&0xFFFF); 
	
   coefficient_diffpressure(Board.diffpressure1, Board.diffpressure2, Board.diffpressure1code, Board.diffpressure2code, &diffpressure_K);

}

void diffpressure2_adj_active(void)
{
	uint8_t i;
	
	uint32_t adcode[32];
	
	//Board.diffpressure2 = diffpressure2;

	EE_WriteVariable(VirtAddVarTab[23], (Board.diffpressure2>>16)&0xFFFF); 
	EE_WriteVariable(VirtAddVarTab[24], (Board.diffpressure2)&0xFFFF);
	
//	LCD_Screen_Clear();	  
//	LCD_Display_String(3, 3, "Adjust...");

	Power_Sensorboard_On();
	Delay(100);

	AD7794_Init();
	Delay(300);
	AD7794_SoftwareReset();

//#ifdef DIFFPRESSURE_GAIN64
//	AD7794_Channel_Configration(DIFFPRESSURE_CH, BIPOLAR, GAIN_64, MD_CONTINUOUS_CONVERSION);
//#else
	AD7794_Channel_Configration(CHANNEL_AIN6, BIPOLAR, GAIN_128, MD_CONTINUOUS_CONVERSION);
//#endif	
	AD7794_ReadResultForContinuousConversion(0, 32, adcode);

	for(i=1; i<32; i++)
	{
		adcode[0]+=adcode[i] ;
	}
	adcode[0] >>= 5;

	//Power_Sensorboard_Off();

	Board.diffpressure2code =adcode[0];

	EE_WriteVariable(VirtAddVarTab[27], (Board.diffpressure2code>>16)&0xFFFF);
	EE_WriteVariable(VirtAddVarTab[28], (Board.diffpressure2code)&0xFFFF); 
	
  coefficient_diffpressure(Board.diffpressure1, Board.diffpressure2, Board.diffpressure1code, Board.diffpressure2code, &diffpressure_K);

}


/******************************************系数的计算**********************************************/
void coefficient_weight(uint16_t w1, uint16_t w2, uint32_t code1, uint32_t code2, float *weight_k)
{

    uint16_t w_small;
    uint16_t w_big;
    uint32_t w_code1;
    uint32_t w_code2;
    uint32_t delta_code;
    uint16_t delta_w;
    
    if(w2>w1)
    {
        w_big = w2;
        w_small = w1;
        
        w_code1 = code1;
        w_code2 = code2;
    }
    else
    {
        w_big = w1;
        w_small = w2;
        
        w_code1 = code2;
        w_code2 = code1;       
    }
    
    delta_w = w_big - w_small;
    delta_code = w_code2 - w_code1;

	(*weight_k) = (float)delta_code/(float)delta_w;						  
}

/******************************************系数的计算**********************************************/
void coefficient_diffpressure(uint32_t p1, uint32_t p2, uint32_t code1, uint32_t code2, float *diffpressure_k)
{

    uint32_t p_small;
    uint32_t p_big;
    uint32_t p_code1;
    uint32_t p_code2;
    uint32_t delta_code;
    uint32_t delta_p;
    
    if(p2>p1)
    {
        p_big = p2;
        p_small = p1;
        
        p_code1 = code1;
        p_code2 = code2;
    }
    else
    {
        p_big = p1;
        p_small = p2;
        
        p_code1 = code2;
        p_code2 = code1;       
    }
    
    delta_p = p_big - p_small;
    delta_code = p_code2 - p_code1;

	(*diffpressure_k) = (float)delta_code/(float)delta_p;
	
	//diffpressure_diameter = Board.diameter;
	//Diameter_cm = (float)diffpressure_diameter/10.0;//单位cm
	Diameter_div4 = Diameter_cm/4.0;//单位cm
	
	PiDD = PI*Diameter_cm*Diameter_cm;
	
	coff_ka = PiDD/6.0; //当h<=D/4 V(ml)=coff_ka*h
	coff_kb = PiDD/4.0; //当h>D/4 V(ml)=coff_kb*h - coff_b;
	coff_b  = Diameter_cm*PiDD/48.0;					  
}




void coefficient_pressure(uint16_t p1, uint16_t p2, uint32_t code1, uint32_t code2, float *pressure_k)
{

    uint16_t p_small;
    uint16_t p_big;
    uint32_t p_code1;
    uint32_t p_code2;
    uint32_t delta_code;
    uint16_t delta_p;
    
    if(p2>p1)
    {
        p_big = p2;
        p_small = p1;
        
        p_code1 = code1;
        p_code2 = code2;
    }
    else
    {
        p_big = p1;
        p_small = p2;
        
        p_code1 = code2;
        p_code2 = code1;       
    }
    
    delta_p = p_big - p_small;
    delta_code = p_code2 - p_code1;

	(*pressure_k) = (float)delta_code/(float)delta_p;						  
}
