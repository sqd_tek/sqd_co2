#ifndef LCD_H_
#define LCD_H_

#define LCD_CS_Set() 	  GPIO_SetBits(GPIOD,GPIO_Pin_4)
#define LCD_RS_Set() 	  GPIO_SetBits(GPIOD,GPIO_Pin_7) 
#define LCD_CLK_Set()	  GPIO_SetBits(GPIOD,GPIO_Pin_6)   
#define LCD_RST_Set()     GPIO_SetBits(GPIOB,GPIO_Pin_5) 
#define LCD_SI_Set()	  GPIO_SetBits(GPIOD,GPIO_Pin_5) 

#define LCD_CS_Reset() 	GPIO_ResetBits(GPIOD,GPIO_Pin_4)
#define LCD_RS_Reset() 	GPIO_ResetBits(GPIOD,GPIO_Pin_7) 
#define LCD_CLK_Reset()	GPIO_ResetBits(GPIOD,GPIO_Pin_6)   
#define LCD_RST_Reset() GPIO_ResetBits(GPIOB,GPIO_Pin_5) 
#define LCD_SI_Reset()	GPIO_ResetBits(GPIOD,GPIO_Pin_5) 

#define KEY_Enter_Input() GPIO_ReadInputDataBit(GPIOD,GPIO_Pin_6)
#define KEY_Down_Input()  GPIO_ReadInputDataBit(GPIOD,GPIO_Pin_7) 
#define KEY_Up_Input()    GPIO_ReadInputDataBit(GPIOD,GPIO_Pin_8) 
#define KEY_Menu_Input()  GPIO_ReadInputDataBit(GPIOD,GPIO_Pin_9) 

//#define Solenoid_on()     GPIO_SetBits(GPIOD,GPIO_Pin_2)  
//#define Solenoid_off()    GPIO_ResetBits(GPIOD,GPIO_Pin_2) 


#define Relay_1_off()     GPIO_SetBits(GPIOD,GPIO_Pin_14)  
#define Relay_1_on()    GPIO_ResetBits(GPIOD,GPIO_Pin_14)  

#define Relay_2_off()     GPIO_SetBits(GPIOD,GPIO_Pin_15)  
#define Relay_2_on()    GPIO_ResetBits(GPIOD,GPIO_Pin_15) 

#define Relay_3_off()     GPIO_SetBits(GPIOC,GPIO_Pin_6)  
#define Relay_3_on()    GPIO_ResetBits(GPIOC,GPIO_Pin_6)  

#define Relay_4_off()     GPIO_SetBits(GPIOC,GPIO_Pin_7)  
#define Relay_4_on()    GPIO_ResetBits(GPIOC,GPIO_Pin_7)

#define Liq_Out_on_1()		GPIO_ResetBits(GPIOD,GPIO_Pin_14)								//��Һ����������
#define Liq_Out_off_1()		GPIO_SetBits(GPIOD,GPIO_Pin_14)									//��Һ���رտ���	
#define Liq_Out_Close_OK	GPIO_ReadInputDataBit(GPIOD, GPIO_Pin_13) 
#define Liq_Out_Open_OK		GPIO_ReadInputDataBit(GPIOD, GPIO_Pin_12)

#define Liq_Out_off_2()		GPIO_ResetBits(GPIOC,GPIO_Pin_7)								//��Һ����������
#define Liq_Out_on_2()		GPIO_SetBits(GPIOC,GPIO_Pin_7)									//��Һ���رտ���

//#define Liq_In_on_1()		GPIO_ResetBits(GPIOC,GPIO_Pin_4)									//��ҹ����������
//#define Liq_In_off_1()		GPIO_SetBits(GPIOC,GPIO_Pin_4)									//��ҹ���رտ���

#define Liq_In_on_2()		GPIO_ResetBits(GPIOC,GPIO_Pin_6)									//��ҹ����������
#define Liq_In_off_2()		GPIO_SetBits(GPIOC,GPIO_Pin_6)									//��ҹ���رտ���

void Liq_Out_on(void);
void Liq_Out_off(void);



//void Liq_In_on(void);
//void Liq_In_off(void);



/***************************/




/***************************/ 
void LCD_IO_Deinit(void);
void KEY_Init(void);
void LCD_IOinit_OUT(void);
void LCD_Initial(void);
void LCD_Screen_Clear(void);
void LCD_Display_Char(unsigned char x,unsigned char y, char uchar);
void LCD_Display_Char_Reverse(unsigned char x,unsigned char y, char uchar);
void LCD_Display_String(unsigned char x,unsigned char y, char *uchar);
void LCD_Display_String_Reverse(unsigned char x,unsigned char y, char *uchar);	   //����ʾ�ַ���
void LCD_HanZi_display(unsigned char x,unsigned char y);
#endif

