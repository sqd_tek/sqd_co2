#include <stm32f10x.h>
#ifndef LCD_TASK_H_
#define LCD_TASK_H_

#define KEY_ENTER 0x0002
#define KEY_DOWN  0x0100
#define KEY_UP	  0x0200
#define KEY_MENU  0x0001
//#define KEY_LOAD  0x0001

#define SERVERIP 0x731C82F2 
#define PORT     8997
#define GASINDEX 3									//此农业设备默认为CO2
#define BOARDID  30									//设备ID，到时确定后设置
#define INTERVALINDEX 0
#define WEIGHTVALUE   80
#define WEIGHT1VALUE  0
#define WEIGHT2VALUE  80
#define WEIGHTZERO    0x8002DB
#define WEIGHTWEIGHT  0x8071CD
#define NETWEIGHT     0
#define TWEIGHTINDEX  3
#define MAXVOLUMEVALUE 4750
#define PRESSURE1VALUE 0
#define PRESSURE2VALUE 200
#define PRESSURE1CODE  8388608
#define PRESSURE2CODE  8672076
#define VERSION "SQD-W02-150213"
//#define PASSWARD "123456"
#define ROOTPASSWARD  "111111"
#define GUESTPASSWARD "000000"


/*一级菜单索引定义*/
#define HEAD_PAGE 0
#define MENU      1
#define SERVER    6
#define PRESSURE  3
#define WEIGHT    2
#define TIMER     4
#define BID       5
#define Temp_set   7
#define RH_set  8
#define ABOUT     9
#define LOGIN     13
#define NOMENU    255
/*预留四个设置选项扩展*/

/*二级菜单索引定义*/
#define SECOND_BASE 14
#define T_WEIGHT    0
#define	WEIGHT_STD  1
#define WEIGHT_ZERO 2 
#define WEIGHT_CALI 3 
#define WEIGHT_NET  4
#define MAXVOLUME   5

#define PRESSURE1   8
#define PRESSURE2	9
//#define  
//#define  
//#define  
//#define  
//#define  
//#define  
//#define  
//#define   

/*参数设置范围定义*/
#define MAXVOLUME_T      400000 //单位L
#define MAXVOLUME_B      200   //单位L
#define MAXVOLUME_STEP_L 100   //长按键步长
#define MAXVOLUME_STEP_M 10   //长按键步长
#define MAXVOLUME_STEP_S 1	   //段按键步长  

/*压差计算常亮*/
#define G  9.8  
#define PI 3.14 
#define MMH2OTOP 9.80665 

 typedef struct
{
  char name[5];
  uint32_t weight;
  uint32_t volume;
  uint32_t NM3;
  uint32_t KPA;
  float MPA;
  uint32_t BAR;
  uint32_t PSI;

  uint32_t percentage; 
  
} Gas_Type;

typedef struct
{
    unsigned char x;
	unsigned char y;
	uint32_t interval;
	char time[5];
} Time_Display;


typedef struct
{
    unsigned char x;
	unsigned char y;
	uint32_t range;
	char name[4];
}Weight_Range_Display;

typedef struct
{
  uint32_t serverIP;
  uint16_t port;
  uint16_t gasindex;
  uint16_t boardid;
  uint16_t intervalindex;

  uint16_t weightrangeindex;
  uint16_t weightvalue;
  //uint16_t netweight;
  int16_t netweight;
  uint16_t reserve;

  uint16_t weight1;
  uint16_t weight2;
  uint32_t weightzerocode;
  uint32_t weightweightcode;
  
  uint16_t pressure1;          //kpa 
  uint16_t pressure2;		   //kpa
  uint32_t pressure1code;
  uint32_t pressure2code;
  uint32_t maxvolume;
	
	uint32_t diffpressure1;          //kpa 
  uint32_t diffpressure2;		   //kpa
  uint32_t diffpressure1code;
  uint32_t diffpressure2code;

  char *rootpwd;
  char *guestpwd;
  char *version;
  
}Board_Type;

typedef struct
{
  uint8_t co2ID;
	uint16_t Vbat;
	uint8_t PG;
	uint16_t CO2_tem;
	uint8_t Soid_Status;
	uint8_t reserved1;
	uint8_t reserved2;
}CO2_data_Type;


extern uint8_t Freq_433_send_flag;
extern uint8_t Login_Flag;
extern uint32_t KEY_Waittime;
extern uint8_t  Menu;
extern float Weight_K;
extern float Pressure_K;
extern uint16_t  gas_index;
extern Gas_Type gas;
extern Gas_Type gas_table[5];
extern uint16_t  gas_index;
extern Time_Display timer_intervel[9];
extern Weight_Range_Display weight_range[6];
extern uint8_t Solenoid;
extern uint8_t Load_Flag;
extern uint32_t W_Load;
extern uint32_t W_Load_Before;
extern uint32_t W_Load_After;

extern uint32_t cur_weight;
extern uint32_t  total_weight;
extern uint32_t cur_add_weight;
extern uint32_t cur_full_weight;
extern uint32_t  cur_total_weight;
extern uint32_t start_weight;
extern uint32_t cur_total_volume;			 //总消耗流量

extern float Diameter_div4;
extern float PiDD;
extern float coff_ka;
extern float coff_kb;
extern float coff_b;
extern float diffpressure_K;

void LCD_KEY_Deinit(void);
void LCD_KEY_Init(void);
void LCD_Display_First(void);
void LCD_Display_Seting(void);
void LCD_Display_Login(void);
void KEY_Scan(void);
void Set_Deflaut(void);
void Config_Deflaut(void);
void Saveto_Deflaut(void);
void LCD_Display_int(uint8_t x, uint8_t y, uint32_t uint);
void coefficient_weight(uint16_t w1, uint16_t w2, uint32_t code1, uint32_t code2, float *weight_k);
void coefficient_pressure(uint16_t p1, uint16_t p2, uint32_t code1, uint32_t code2, float *pressure_k);
void caculate_weight(uint32_t wcode, uint16_t w1, uint32_t code1, float weight_k);

#endif

