#ifndef DELAY_H
#define DELAY_H
void my_systeminit(void);
void SysTick_Configuration(void);
void Delay_us(unsigned int nus);
void Delay_ms(unsigned int nms);
void Delay(unsigned int nTime);

void Timer2_Init(void);
void Timer3_Init(void);
void Timer4_Init(void);
void Timer5_Init(void);
#endif
