/**/
#include <stm32f10x.h>
#include <stdio.h>
#include <stdlib.h>
#include "config.h"
#include "stm32f10x_conf.h"
#include <stdarg.h>
#include <string.h>

#define CO2_TOP_default 900						//CO2浓度上限值
#define CLIENT_NUM 10					//从机总个数
#define Fresh_Time 10
#define GPRS_Send_Time  10

#define TURN_ON  1
#define TURN_OFF 0

#define MORNING_TIME  6
#define NIGHT_TIME   19

unsigned int AD7794Data[10];
unsigned int AD7794Read[9];
u16 CO2_Top = CO2_TOP_default;

/*板卡信息*/
extern Board_Type Board;



/*EEPROM用*/
FLASH_Status FlashStatus;

/*任务标志 1 LCD任务 非1 采集任务*/
uint8_t LCD_Task = 1;

/*按键唤醒标志 1 按键唤醒*/
uint8_t KEY_Wakeup_Flag = 0;

/*延时10s的计算变量*/
uint8_t Delay_10s;

/*serial number*/
uint32_t sn;

/*Battery voltage*/
uint32_t V_bat;

float Voltage=0;


/*整理发送数据的承载缓冲区*/
char gpsinformation[100];
char data[100];

/*气体计算公式*/
float gas_k[5][4] ={
	                 {1.42906, 0.001141 , 23.13948, 111.818},
					 {1.78402, 0.001393 , 28.25004, 136.514},                                          
                     {1.977  , 0.001101 , 22.32828, 107.898},
					 {1.25033, 0.000808 , 16.38624, 79.184 },
                     {0.717  , 0.000426 , 8.63928 , 41.748 }
	               };

								 
/**罐体参数设置及测量部分**/
void caculate_pressure(uint32_t pcode, uint16_t p1, uint32_t code1, float pressure_k);							//压力采集
void caculate_vo(uint32_t pcode, uint32_t p1, uint32_t code1, float pressure_k);										//压差采集重量								 
void caculate_weight(uint32_t wcode, uint16_t w1, uint32_t code1, float weight_k);
void data_collection(uint32_t *z,uint32_t *weight_buff);	
u8 Pressure_low_set_flag=0;					//压力低点校准标志
u8 Pressure_high_set_flag=0;					//压力高点校准标志
u8 Weight_low_set_flag=0;						//液位重量低点
u8 Weight_high_set_flag=0;					//液位重量高点
/***校准部分***/							 
void diffpressre_calibration_low(void);				//压差低点校准
void diffpressre_calibration_high(void);			//压差高点校准
void pressre_calibration_low(void);				//压力低点校准
void pressre_calibration_high(void);
u8 first_filter=1;
/****/
void sort(uint32_t z,uint32_t *a);								 

/*CO2传感器*/								 
uint8_t  CO2_INS[9]={0xff,0x01,0x86,0x00,0x00,0x00,0x00,0x00,0x79};
char  date[6];
uint8_t  data_rx_buf[9]={0x00};		//ID,Vbat,5V_status,CO2-data,soid_status,reserved,reserved
uint16_t CO2_tem,test;
u8 Out_TimeLong=3;

/*CO2数据协议*/
extern CO2_data_Type co2; 
u16 Co2_Sth_temp_buffer[10]={400};

/*温湿度传感器*/
uint32_t  Tem_RH_data;
uint16_t  tem1,RH1;
void Tem_RH_measure(void);

/*光照传感器*/
extern uint8_t I2C_Data[10],ret;
extern I2C_INIT_INFO I2C_Info;	///<I2C控制器相关信息
//光照强度
u32 light;
u32 light_measure(void);				//光照测量

/*风速 传感器*/
float wind_speed=0;
u16 wind_rank_display=0;			//显示变量
u8 wind_pulse_flag=0;					//脉冲型测量标志
u16 wind_pulse_counter=0;			//脉冲型计数
u8 wind_level(float wind_speed_temp);
void wind_config(void);
void wind_measure(void);				//风速测量

//中断向量配置函数
void NVIC_ALL_Configuration(void);

/**433**/
uint8_t Freq_433_Slave_Recv_Flag=0;						//从机 应答 标识
uint8_t Freq_433_Slave_data_Flag=0;						//从机数据传输是否正确 标识
void MastertoSlave_Send(u16 Slave_Id);							//主机向从机发送函数	
char Send_Cmd[4]={0};
u8 Disp_flag_Auto=0;								//自动刷新命令标志	
u8 Disp_flag_Manual=0;							//手动刷新标识
u8 DISP_Receive=0;
u8 Lock_flag=0;								//关闭命令标志
u8 Open_flag_Manual=0;								//正常释放条件判断	
u8 Open_flag_Auto=0;
u32 Send_counter=0;						//发送计数
u16 Slave_num=0;
u8 First_Send_Open_Flag=0;	
u16 Disp_time=0;		//刷新指令等待时间		
u8 Disp_time_New=0;
u8 open_status_update=2;					//OPEN指令状态标识
void SlavetoMaster_Receive(void);						//从机向主机发送数据
void Fresh_Func(void);
	
/**环境条件变量**/
volatile u16 temp_top=50;//温度上限		初始化10度
volatile u16 temp_below=10;//下限
volatile u16 hum_top=95;    	//湿度上限
volatile u16 hum_below=5;
volatile u16 con_top=CO2_TOP_default;		//浓度上限
volatile u16 con_below=300;
volatile u16 light_top=30000;	//光照上限
volatile u16 light_below=20;
volatile u16 wind_top=8;	//风速上限
volatile u16 wind_below;
volatile u16 pre_top;	//压力上限
volatile u16 pre_below;
volatile u16 weight_top;	//重量上限
volatile u16 weight_below;



//显示命令格式
u8 Huanjing_Disp_Ins[]={0x5A,0xA5,0x05,0x82,0x00,0x00,0x00,0x00};

//环境变量显示框地址
u8 Huanjing_Add[14]={0x60,0x61,0x62,0x63,0x64,0x65,0x66,0x67,0x68,0x69,0x6a,0x6b,0x6c,0x6d};

//环境变量初值数组，一共14个
u16 Huanjing_Value[14]={0};

//环境变量赋初值函数
void Huanjing_Default(void);

//环境变量初始化显示函数
void Huanjing_Disp_default(u8 *Var_Add,u16 *Var_Value);

u8 Out_Time_Add[6]={0x80,0x82,0x84,0x81,0x83,0x85};
void Time_Default_Disp(u8 *Out_Time_Add,u8 *Out_Time_H_Value,u8 *Out_Time_M_Value);



/**实时时钟部分**/
volatile u8 RTC_Hour=0;
volatile u8 RTC_Min=0;
volatile u8 RTC_Month=0;
volatile u8 RTC_Day=0;
//u8 Month_MCU=0;
//u8 Day_MCU=0;

u8 Outtime_Hour[3]={8,13,0};					//释放预设时间小时
u8 Outtime_Min[3]={30,0,0};					//分钟

u8 Time_Enable;							//预设时间使能标志
u8 Time_flag=0;								//时间符合标志位
u8 Time_get_flag=0;
void RTC_Get(void);
//u16 Open_time_counter=0;					//正常释放时间内的时间计数

volatile uint8_t Recv_client_flag;
volatile u8 client_buff[32];							//debug
volatile u8 wind_buff[7];
void uart_show(u16 *client_id);					//发送节点信息
void uart_hostshow(void);								//发送主机采集的数据
void uart3_config(void);


/**总流量变量**/
extern uint32_t cur_weight;
extern uint32_t  total_weight;
extern uint32_t cur_add_weight;
extern uint32_t cur_full_weight;
extern uint32_t  cur_total_weight;
extern uint32_t start_weight;
extern uint32_t cur_total_volume;			 //总消耗流量

void write_Poweroff(void);
void Volume_Poweroff(void);

/**串口屏操作指令**/
u8 TFT_LCD_RTC_Read_Calender_Ins[]={0x5a,0xa5,0x03,0x81,0x20,0x07};
u8 TFT_LCD_RTC_Read_YMD_Ins[6]={0x5a,0xa5,0x03,0x81,0x24,0x03};
u8 Login_Succ_flag =0;					//登陆成功标志
u8 jiaozhun_disp[8] ={0x5A,0xA5,0x05,0x82,0x00,0x90,0x00,0x00};				//压力、压差校准显示	 

extern void diffpressure1_adj_active(void);
extern void diffpressure2_adj_active(void);
extern void pressure1_adj_active(void);
extern void pressure2_adj_active(void);

void diff_low_disp(void);
void diff_high_disp(void);
void pre_low_disp(void);
void pre_high_disp(void);

void Get_Adjust_Data(void);//校准值处理任务
u8 Valve_Task(u8 State_Of_Task);//出气阀打开、关闭任务，0：阀门关闭；1：阀门打开
u8 Compare_All_Day_Time(void);//全天时间条件判断，夜间判断，0：夜间；1：白天
u8 Is_Time_To_Let_Gas_Out(void);//时间释放条件判断，0：时间点不满足；1：时间点满足

//void Timer3_Init(void);

/**总流量所用变量**/
extern uint32_t W_Load;
/**GPRS发送任务**/
	char ipstr[15];
	char portstr[5];
void IP_Set(void);
void GPRS_PWR_ON(void);
void GPRS_TCP_IP(void);
void GPS_Process(void);
u8 GPRS_Send_data(u8 Send_flag);
extern Gps_Info_TypeDef GPS_info;

/**释放条件判断**/
u8 Co2_Out_Panduan(void);
void Open_Cmd_Set(u8 tem_flag);				//释放命令赋值
u8 CO2_Out_flag=0;										//出液阀标志位
u8 CO2_Sth_panduan(void);

extern u8 TH_DATA_FRESH_TIME_FLAG;


int main(void)
{  
	
	static uint32_t weight_buff[20];
	static u32 exchange1 = 0;
	static u32 exchange =0;
//	static u8 open_status=0;				//OPEN指令发送指令状态
	
    /******************系统平台初始化********************/	
	u8 i;
	uint32_t j;
	uint32_t z=0;
	u8 hehe[7] = {0x5A,0xA5,0x04,0x80,0x03,0x00,0x01};
	
	
	uint8_t Checksum;
//	my_systeminit();
	SysTick_Configuration();
	FLASH_Unlock();									//flash初始化
  /* EEPROM Init */
  EE_Init();
	/***********************参数初始化部分************************/	
	Set_Deflaut();
	
	PlatForm_Init();
	__disable_irq();              
	Uart_TFTLCD();									//彩色串口显示屏
	__enable_irq();
	
	//GPS_DeInit();

	//GPRS_DeInit();
	//AD7794_DeInit();
	
	LPM_Init();											//内部RTC时钟初始化
	
	
	/* Unlock the Flash Program Erase controller */
  
	
	
	
	NVIC_ALL_Configuration();				//中断向量初始化

	gas_index = 2;
	gas = gas_table[gas_index];
	
	Login_Flag = 0;
  Timer2_Init();						//1s定时器中断
//	Timer3_Init();						//20s定时中断，用于读取串口屏RTC时间	
	Timer4_Init();
	IWDG_Init();
	
	//调试打印
	UART4_Init();   
	
	Uart5_Config_Init();			//433通信串口
    BH1750_Init();									//光照传感器初始化
    AM2301_Port_Config();						//温湿度传感器初始化
	wind_config();									//风速传感器配置

//	GPRS_PWR_ON();	//GPRS上电
	Menu = HEAD_PAGE;       //切换到登陆页面
		
		/**传感器部分初始化**/
		Power_Sensorboard_On();						//压差及压力传感器初始化
	    AD7794_Init();
	    Delay(200);

		__disable_irq();              
	    AD7794_SoftwareReset();
		__enable_irq();  

		//Delay(200);
		Huanjing_Default();//设置环境变量初始值
		Huanjing_Disp_default(Huanjing_Add,Huanjing_Value);
		
		Time_Default_Disp(Out_Time_Add,Outtime_Hour,Outtime_Min);	

		diff_low_disp();
		diff_high_disp();
		pre_low_disp();
		pre_high_disp();
		
		Beep_On();
		Delay_ms(100);
		Beep_Off();
		Delay_ms(100);
		
	while(1)
	{
        UART4_printf(UART4,"\r\nStart\r\n");
		//校准程序
		Get_Adjust_Data();
		IWDG_ReloadCounter();
		
		/**串口RTC**/
		RTC_Get();		
		IWDG_ReloadCounter();
		
        if(TH_DATA_FRESH_TIME_FLAG)
        {
            /**温湿度测量**/
            Tem_RH_measure();
            TH_DATA_FRESH_TIME_FLAG = 0;
            Core_LED_Rate();
        }
		IWDG_ReloadCounter();

        /**光照测量**/
        light =light_measure();
		IWDG_ReloadCounter();
	
		/**风速测量**/					
		RTC_WaitForLastTask();
		KEY_Waittime = RTC_GetCounter();		
		IWDG_ReloadCounter();
//		Core_LED_Rate();
		
		SlavetoMaster_Receive();
		if(KEY_Waittime>10)
		{
			wind_measure();
			KEY_Waittime=0;
			RTC_SetCounter(0);
			Disp_time++;
		}
		
		if(((Disp_time>GPRS_Send_Time)&&(!Load_Flag))||(Load_Flag==1)||(Load_Flag==3))										//??30·??óí3??ò?′?
		{ 
            Beep_On();
            Delay_ms(100);
            Beep_Off();
            Delay_ms(100);
			Power_GPS_On();
            GPRS_PWR_ON();
			for(i=0;i<10;i++)
			{
				Delay_ms(150);//延时10s，搜星
				IWDG_ReloadCounter();
			}
			Disp_time =0;
			if(cur_total_volume < exchange)
			{
					cur_total_volume = exchange;
			}
			if(Load_Flag==1)
			{
				cur_total_volume =exchange;				//3?òoê±±￡′?×üá÷á?
				exchange1 = exchange;
				exchange =0;
			}
		
			if(Load_Flag==3)
			{
				exchange =exchange1;
				write_Poweroff();
				Volume_Poweroff();
			}
			IP_Set();				//IPéè??
			GPRS_TCP_IP();	//GPRSTCP/IPá??ó
			GPS_Process();
			GPRS_Send_data(1);
            Power_GPRS_Off();
			exchange = cur_total_volume;
			Disp_time=0;
		}
		//GPRS数据发送部分
//		if(((Disp_time>GPRS_Send_Time)&&(!Load_Flag))||(Load_Flag==1)||(Load_Flag==3))										//每30分钟统计一次
//		{ 
//			Disp_time =0;
//			if(cur_total_volume < exchange)
//			{
//					cur_total_volume = exchange;
//			}
//			if(Load_Flag==1)
//			{
//				exchange1 = cur_total_volume;				//充液时保存总流量
//				exchange =0;
//			}
//		
//			if(Load_Flag==3)
//			{
//				exchange =exchange1;
//				write_Poweroff();
//				Volume_Poweroff();
//			}
//			IP_Set();				//IP设置
//			GPRS_TCP_IP();	//GPRSTCP/IP连接
//			GPS_Process();
//			GPRS_Send_data(1);
//			Disp_time=0;
//		}
		
		/*采集重量、压力*/
		data_collection(&z,weight_buff);
		if(z > 14 )
		{
			z = 0;
			memset(weight_buff,0,sizeof(weight_buff));
		}
		//总流量计算
		cur_total_weight = total_weight + cur_full_weight - gas.weight;
		if(total_weight + cur_full_weight<gas.weight)
		{
			cur_total_weight = 0;
		}
		cur_total_volume =cur_total_weight/(gas_k[Board.gasindex][1]*1000);	
		
		if(Compare_All_Day_Time())//白天
		{
			TIM_Cmd(TIM4,ENABLE);//开放发送定时器
			/**检测环境变量、系统时间是否满足释放要求**/
			Open_flag_Auto = Co2_Out_Panduan();
			if((Disp_time_New%20)==0)//定时自动刷新
			{
				if(!Disp_flag_Auto)
				{
					Disp_flag_Auto=1;		//自动刷新标识
				}						
				Disp_flag_Manual=0;		//屏蔽手动刷新
				Disp_time_New=1;			//强制使置位次数为一次							
			}
			
			if(Open_flag_Auto)	//自动或者手动打开出气总阀门
			{
				Valve_Task(TURN_ON);//打开总阀
			}
			else
			{
				Valve_Task(TURN_OFF);//关闭总阀
			}
			SlavetoMaster_Receive();
			IWDG_ReloadCounter();	
			
			
			if(Lock_flag)							//关闭球阀指令
			{		
				Valve_Task(TURN_OFF);//关闭总阀
			}					
			IWDG_ReloadCounter();
		}
		else//夜间
		{
			TIM_Cmd(TIM4,DISABLE);
			Valve_Task(TURN_OFF);//关闭总阀
		}		
		SlavetoMaster_Receive();
	}
}

u8 Co2_Out_Panduan(void)
{
	if((Is_Time_To_Let_Gas_Out()) && (((RH1/10) < hum_top)||((RH1/10) == hum_top)) && (((tem1/10) > temp_below)||(tem1/10) == temp_below) && (light > light_below) && ((wind_speed/10 < wind_top) || wind_speed/10 == wind_top))//温湿度、光照、风速、时间判断
	{
			return 1;
	}
	else
	{
			return 0;
	}
}

/**433主机发送函数**/
void MastertoSlave_Send(u16 Slave_Id)
{

	Freq_433_Send_data(Slave_Id,Send_Cmd);
						
}

//433接收从机数据函数
void SlavetoMaster_Receive(void)
{
	u16 client_id = 0;
	u16 client_co2 = 0;
	//static u16 temp=0;
	IWDG_ReloadCounter();
	//temp = client_co2;
	if(Recv_client_flag == 0)
	{
				//记录没有回应的从机ID,没有回应的从机直接显示0
				//client_co2 =0;
				client_co2 =0;
				co2.Soid_Status=0;
				co2.Vbat =0;
				client_id =Slave_num;
				//uart_show(&client_id);
	}
	else
	{
			Recv_client_flag = 0;
			client_co2 = (client_buff[3] << 8) + client_buff[2];//从机co2浓度
			client_id = (client_buff[9] << 8) + client_buff[8];//从机id
			if(client_co2 >500 && (!(client_co2>600)))
			{
				client_co2 = client_co2 * 8/10;
				client_buff[2] =client_co2&0xff;
				client_buff[3] =client_co2>>8;
				
			}
			else if(client_co2 >600 && (!(client_co2>800)))
			{
				client_co2 = client_co2 *7/10;
				client_buff[2] =client_co2&0xff;
				client_buff[3] =client_co2>>8;
				
			}
			
			
			co2.PG = client_buff[5];		//5V状态
			co2.Soid_Status =client_buff[6];//球阀状态
			co2.Vbat =(client_buff[5]<<8) + client_buff[4];//电压
			co2.Vbat = co2.Vbat/10;
			client_buff[5] = co2.Vbat>>8;
			client_buff[4] = co2.Vbat;
			
			Co2_Sth_temp_buffer[client_id]=client_co2;
	  	if(client_co2 > 0 && client_co2 < 5000)
			{
				uart_show(&client_id);
			}
	}
							
}

void diffpressre_calibration_low(void)
{
		//u8 j;
		diffpressure1_adj_active();
		diff_low_disp();
		
}

void diffpressre_calibration_high(void)
{
		//u8 j;
		diffpressure2_adj_active();
		diff_high_disp();
		
}

void pressre_calibration_low(void)
{
		//u8 j;
		pressure1_adj_active();
		pre_low_disp();	
}

void pressre_calibration_high(void)
{
		//u8 j;
		pressure2_adj_active();
		pre_high_disp();
}

void diff_low_disp(void)
{
	u8 j;	
	//显示，表示该点校准完成
		jiaozhun_disp[5] =0x90;
		jiaozhun_disp[6] =Board.diffpressure1>>8;
		jiaozhun_disp[7] =Board.diffpressure1;
		for(j = 0;j<8;j++)
		{
					USART_SendData(USART1,jiaozhun_disp[j]);
					while(USART_GetFlagStatus(USART1, USART_FLAG_TC)==RESET);
		}
}

void diff_high_disp(void)
{
	u8 j;			
	//显示，表示该点校准完成
		jiaozhun_disp[5] =0x92;
		jiaozhun_disp[6] =Board.diffpressure2>>8;
		jiaozhun_disp[7] =Board.diffpressure2;
			for(j = 0;j<8;j++)
			{
						USART_SendData(USART1,jiaozhun_disp[j]);
						while(USART_GetFlagStatus(USART1, USART_FLAG_TC)==RESET);
			}
}
void pre_low_disp(void)
{
		u8 j;	
		//显示，表示该点校准完成
		//显示，表示该点校准完成
		jiaozhun_disp[5] =0x94;
		jiaozhun_disp[6] =Board.pressure1>>8;
		jiaozhun_disp[7] =Board.pressure1;
		for(j = 0;j<8;j++)
		{
					USART_SendData(USART1,jiaozhun_disp[j]);
					while(USART_GetFlagStatus(USART1, USART_FLAG_TC)==RESET);
		}
}
void pre_high_disp(void)
{
		u8 j;	
		//显示，表示该点校准完成
			//显示，表示该点校准完成
		jiaozhun_disp[5] =0x96;
		jiaozhun_disp[6] =Board.pressure2>>8;
		jiaozhun_disp[7] =Board.pressure2;
			for(j = 0;j<8;j++)
			{
						USART_SendData(USART1,jiaozhun_disp[j]);
						while(USART_GetFlagStatus(USART1, USART_FLAG_TC)==RESET);
			}
}


void caculate_pressure(uint32_t pcode, uint16_t p1, uint32_t code1, float pressure_k)
{        
    if(pcode >code1)
    {
        gas.KPA = p1 + ((float)(pcode - code1)/pressure_k + 0.5);
    }
    else
    {
        gas.KPA = p1 - ((float)(code1 - pcode)/pressure_k + 0.5);
    }
   
	if(gas.KPA >6000)
	{
	    gas.KPA = 6000;
	}
	
	//pressure_disp = gas.KPA/10.0;
	gas.PSI = ((float)(gas.KPA)/6.894757) + 0.5;
	gas.BAR = (gas.KPA + 50)/100;
  gas.MPA = (float)(gas.KPA)/1000.0;
}




/**压差方式测量重量**/
void caculate_vo(uint32_t pcode, uint32_t p1, uint32_t code1, float pressure_k)
{ 
	
  uint32_t cur_pressure,vo;
	float h;
	if(pcode >code1)
    {
        cur_pressure = p1 + ((float)(pcode - code1)/pressure_k+0.5);//单位pa
    }
    else
    {
        cur_pressure = p1 - ((float)(code1 - pcode)/pressure_k+0.5);//单位pa
    }
   
	if(cur_pressure >70000)
	{
	    cur_pressure = 70000;
	}

	/*调试用代码*/
	//cur_pressure = 200;
	/************/

	h =  (float)cur_pressure/gas_k[Board.gasindex][3];//单位cm
	if(h>Diameter_div4)
	{
		vo = (coff_kb*h - coff_b + 0.5);//cm3
	}
	else
	{
		vo = (coff_ka*h + 0.5); //cm3
	}

	gas.volume = (vo+500)/1000;//单位L,四舍五入
	gas.weight = 	((float)vo*gas_k[Board.gasindex][1]+0.5);//单位kg
	gas.NM3 = (float)gas.weight/gas_k[Board.gasindex][0]+0.5;//
	gas.percentage = (vo+500)/5000;//百分比*10，方便传输
	
	if(gas.percentage>1000)
	{
		gas.percentage = 1000;
	}
	/*
	sprintf(data,"diffp:%d\r\nh:%fcm\r\nvo:%dL\r\nweight:%dkg\r\nNM3:%d\r\nper:%2.1f%%\r\n",cur_pressure, h, gas.volume, gas.weight, gas.NM3, (float)gas.percentage/10.0);
	
	#ifdef SIM900A_USE
	    printf(data);
	#endif
	*/
}
void data_collection(uint32_t *z,uint32_t *weight_buff)//数据采集
{
		/******测试代码***********/
	//	u32 w = 0;
		
	//static u8  value = 0;
	u8 i;
	uint32_t weight_center;				
	static uint32_t avg_buff[10];
	uint32_t avg_temp=0;
	static u8 PWR_UP=1;

	AD7794_Channel_Configration(CHANNEL_AIN6, BIPOLAR, GAIN_128, MD_SINGLE_CONVERSION);			//采集罐体液位重量
	AD7794_ReadResultForSingleConversion(0,AD7794Data);			
	weight_buff[(*z)] = AD7794Data[0];

	IWDG_ReloadCounter();
	
	/*采集压力*/
	AD7794_Channel_Configration(CHANNEL_AIN1, BIPOLAR, GAIN_64, MD_SINGLE_CONVERSION);				/*采集罐体压力*/
	AD7794_ReadResultForSingleConversion(5, AD7794Data);

	IWDG_ReloadCounter();		
	
	caculate_pressure(AD7794Data[5], Board.pressure1, Board.pressure1code, Pressure_K);					//计算罐体压力
	
	(*z)++;

	if((*z) == 15)
	{		 
				//weight_end = 1;
				//sort((*z),temp_buff);
			sort((*z),weight_buff);
	
			weight_center = weight_buff[((*z)+1)/2];
			if(first_filter==1)
			{
				for(i=0;i<10;i++)
				{
					avg_buff[i]=weight_center;
				}
				first_filter = 0;
			}
			else
			{		
				avg_buff[9]=avg_buff[8];
				avg_buff[8]=avg_buff[7];
				avg_buff[7]=avg_buff[6];
				avg_buff[6]=avg_buff[5];
				avg_buff[5]=avg_buff[4];
				avg_buff[4]=avg_buff[3];
				avg_buff[3]=avg_buff[2];
				avg_buff[2]=avg_buff[1];
				avg_buff[1]=avg_buff[0];
				avg_buff[0]=weight_center;
			}
			
			for(i=0;i<10;i++)
			{
				avg_temp =avg_temp+avg_buff[i];
			}
			
			weight_center=avg_temp/10;
			avg_temp=0;
		
			caculate_vo(weight_center,Board.diffpressure1, Board.diffpressure1code, diffpressure_K);				//压差传感器计算重量
			//caculate_pressure(AD7794Data[9], Board.pressure3, Board.pressure3code, Pressure_K2,2);					//计算出口压力
//								sprintf(data, "Weight=%04dkg  ",gas.weight);
//								printf(data);

			if(PWR_UP==1)
			{
					cur_full_weight = gas.weight;
					total_weight =0;
					PWR_UP++;
			}
	}
	
	IWDG_ReloadCounter();		

}

void sort(uint32_t z,uint32_t *a)								//数据排序函数
{
		uint32_t i,j,n;
		for(i = 0;i < z-1;i++)
		{
					for(j = 0;j<z-1-i;j++)
					{
								if(a[j] > a[j+1])
								{
												n = a[j];
												a[j] = a[j+1];
												a[j+1] = n;
									}
					}
										
		}

}

void wind_config(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	
	EXTI_InitTypeDef EXTI_InitStructure;
    
	RCC_APB2PeriphClockCmd( RCC_APB2Periph_GPIOD|RCC_APB2Periph_AFIO, ENABLE);
	
	
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;									
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
	//GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOD, &GPIO_InitStructure);												 
	
	GPIO_EXTILineConfig(GPIO_PortSourceGPIOD,GPIO_PinSource10);
	EXTI_InitStructure.EXTI_Line = EXTI_Line10;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
	
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStructure);

}

void uart_show(u16 *client_id)				//节点数据信息显示
{
		
		u8 i;
		u8 buff[8] = {0x5A,0xA5,0x05,0x82,0x00};
		/**节点电压显示**/
		buff[5] = (0x10 + *client_id);
//		client_buff[5] =0x01;
//		client_buff[4] =0xc2;
		
		buff[6] =client_buff[5];				//电压高字节
		buff[7] =client_buff[4];				//电压低字节
		for(i = 0;i < 8;i++)//包头
		{
			USART_SendData(USART1,buff[i]);
			while(USART_GetFlagStatus(USART1, USART_FLAG_TC)==RESET);
		}
		/**节点浓度显示**/
		buff[5] = (0x20 + *client_id);
		buff[6] =client_buff[3];				//浓度高字节
		buff[7] =client_buff[2];				//浓度低字节
		for(i = 0;i < 8;i++)//包头
		{
			USART_SendData(USART1,buff[i]);
			while(USART_GetFlagStatus(USART1, USART_FLAG_TC)==RESET);
		}
		/*节点状态*/
		buff[5] = (0x40 + *client_id);
		if(co2.Soid_Status)
		{				
			buff[6] =0x00;
			buff[7] =0x01;				
		}
		else
		{
		buff[6] =0x00;	
		buff[7] =0x00;
		}
		for(i = 0;i < 8;i++)//包头
		{
			USART_SendData(USART1,buff[i]);
			while(USART_GetFlagStatus(USART1, USART_FLAG_TC)==RESET);
		}

}

void uart_hostshow(void)//发送主机采集的数据
{
		u8 i;
		u8 j;
		u8 buff[10] = {0x5A,0xA5,0x05,0x82,0x00,0x05};
		u8 buff1[10] = {0x5A,0xA5,0x07,0x82,0x00,0x05};
	//	u16 buff_a[4] = {RH1,wind_rank,tem1,ret};//湿度，光照,温度,风速
//		u32 buff_b[3] = {gas.weight,gas.KPA,0};//重量，压力，总消耗量
	typedef union work
	{
			u8 ch[2];
			u16 a;
	}data;
	data center;
	
	typedef union work1
	{
			u8 ch[4];
			u32 a1;
	}data1;
	data1 center1;
	//发送buff_a的前三个内容

	{
		for(i = 0;i < 5;i++)
		{
			USART_SendData(USART1,buff[i]);
			while(USART_GetFlagStatus(USART1, USART_FLAG_TC)==RESET);
		}
		buff[5] = 0x05 + 1;
		USART_SendData(USART1,buff[5]);
		while(USART_GetFlagStatus(USART1, USART_FLAG_TC)==RESET);
//		if(RH1>900)
//		{
//				RH1 =900;
//		}
		center.a = RH1;
		j = 1;
		for(i = 0;i < 2;i++)
		{
			USART_SendData(USART1,center.ch[j]);
			while(USART_GetFlagStatus(USART1, USART_FLAG_TC)==RESET);
			j--;
		}
		
		for(i = 0;i < 5;i++)
		{
			USART_SendData(USART1,buff[i]);
			while(USART_GetFlagStatus(USART1, USART_FLAG_TC)==RESET);
		}
		buff[5] = 0x05 + 2;
		USART_SendData(USART1,buff[5]);
		while(USART_GetFlagStatus(USART1, USART_FLAG_TC)==RESET);
		center.a = wind_speed;
		j = 1;
		for(i = 0;i < 2;i++)
		{
			USART_SendData(USART1,center.ch[j]);
			while(USART_GetFlagStatus(USART1, USART_FLAG_TC)==RESET);
			j--;
		}
		
		for(i = 0;i < 5;i++)
		{
			USART_SendData(USART1,buff[i]);
			while(USART_GetFlagStatus(USART1, USART_FLAG_TC)==RESET);
		}
		buff[5] = 0x05;
		USART_SendData(USART1,buff[5]);
		while(USART_GetFlagStatus(USART1, USART_FLAG_TC)==RESET);
		center.a = tem1;
		j = 1;
		for(i = 0;i < 2;i++)
		{
			USART_SendData(USART1,center.ch[j]);
			while(USART_GetFlagStatus(USART1, USART_FLAG_TC)==RESET);
			j--;
		}
		
	}
	{//光照
		for(i = 0;i < 5;i++)
		{
			USART_SendData(USART1,buff[i]);
			while(USART_GetFlagStatus(USART1, USART_FLAG_TC)==RESET);
		}
		buff[5] = 0x0c;//光照
		USART_SendData(USART1,buff[5]);
		while(USART_GetFlagStatus(USART1, USART_FLAG_TC)==RESET);
		center.a = light;
		j = 1;
		for(i = 0;i < 2;i++)
		{
			USART_SendData(USART1,center.ch[j]);
			while(USART_GetFlagStatus(USART1, USART_FLAG_TC)==RESET);
			j--;
		}
		
	}
	
	//重量，压力，总消耗量
	
		for(i = 0;i < 5;i++)
		{
			USART_SendData(USART1,buff1[i]);
			while(USART_GetFlagStatus(USART1, USART_FLAG_TC)==RESET);
		}
		buff1[5] = 0x08;
		USART_SendData(USART1,buff1[5]);
		while(USART_GetFlagStatus(USART1, USART_FLAG_TC)==RESET);
		center1.a1 = gas.KPA;
		j = 3;
		for(i = 0;i < 4;i++)
		{
			USART_SendData(USART1,center1.ch[j]);
			while(USART_GetFlagStatus(USART1, USART_FLAG_TC)==RESET);
			j--;
		}
		
		for(i = 0;i < 5;i++)
		{
			USART_SendData(USART1,buff1[i]);
			while(USART_GetFlagStatus(USART1, USART_FLAG_TC)==RESET);
		}
		buff1[5] = 0x0a;
		USART_SendData(USART1,buff1[5]);
		while(USART_GetFlagStatus(USART1, USART_FLAG_TC)==RESET);
		center1.a1 =gas.weight;
		j = 3;
		for(i = 0;i < 4;i++)
		{
			USART_SendData(USART1,center1.ch[j]);
			while(USART_GetFlagStatus(USART1, USART_FLAG_TC)==RESET);
			j--;
		}
		
		for(i = 0;i < 5;i++)
		{
			USART_SendData(USART1,buff1[i]);
			while(USART_GetFlagStatus(USART1, USART_FLAG_TC)==RESET);
		}
		buff1[5] = 0x0d;
		USART_SendData(USART1,buff1[5]);
		while(USART_GetFlagStatus(USART1, USART_FLAG_TC)==RESET);
		center1.a1 = cur_total_volume;
		j = 3;
		for(i = 0;i < 4;i++)
		{
			USART_SendData(USART1,center1.ch[j]);
			while(USART_GetFlagStatus(USART1, USART_FLAG_TC)==RESET);
			j--;
		}
		
}




void wind_measure(void)
{
		/**风速测量**/
		
		wind_speed = wind_pulse_counter/10;
		//wind_rank_display =wind_level(wind_speed);
		wind_pulse_flag =0;
		wind_pulse_counter =0;
}

u32 light_measure(void)
{
	u32 light_temp;
	/*光照测量*/;
	I2C_Data[0]=0x01;
    VT_I2C_HardWriteNByte(I2C1,0,I2C_Data,1);//传感器芯片上电命令
    Delay_us(10);
    I2C_Data[0]=0x11;
    ret = VT_I2C_HardWriteNByte(I2C1,0,I2C_Data,1);
    UART4_printf(UART4,"\r\n%d\r\n", ret);
    Delay_ms(5);
	ret = VT_I2C_HardReadNByte(I2C1,0,I2C_Data,2);//读取光照强度值
	light_temp = (((I2C_Data[0]<<8)+I2C_Data[1])/(1.2*2))*10;
    UART4_printf(UART4,"\r\n%d   %d\r\n", ret, light_temp);
	if(ret)
	{
		BH1750_Init();
		I2C_Data[0]=0x10;
		ret = VT_I2C_HardWriteNByte(I2C1,0,I2C_Data,1);//发送测量命令
		Delay_ms(5);//等待大于120ms，必须加这个延时，否则传感器没准备好数据会出现读数据错误
		ret = VT_I2C_HardReadNByte(I2C1,0,I2C_Data,2);//读取光照强度值
		light_temp = (((I2C_Data[0]<<8)+I2C_Data[1])/12)*10;
	}
	Delay_ms(10);
	return light_temp;
}

void Tem_RH_measure(void)
{
		/*温湿度测量*/
	uint16_t RH1_temp,tem1_temp;
	Tem_RH_data = Get_AM2301_Data();
	
	if(Tem_RH_data != 0)
	{
		RH1_temp = (uint16_t)(Tem_RH_data>>16);				//湿度
		tem1_temp = (uint16_t)Tem_RH_data;							//温度
	}	
	if(RH1_temp > 0 && RH1_temp < 1000)
	{
		RH1 = RH1_temp;
	}	
	if(tem1_temp < 850)
	{
		tem1 = tem1_temp;
	}
	
}

void RTC_Get(void)
{
	u8 i;
	for(i=0;i<6;i++)																					//小时、分钟获取
	{
		USART_SendData(USART1,TFT_LCD_RTC_Read_YMD_Ins[i]);
		while(USART_GetFlagStatus(USART1, USART_FLAG_TC)==RESET);
	}
}

u8 Compare_All_Day_Time(void)
{
	if((((RTC_Hour < MORNING_TIME)|| (RTC_Hour == MORNING_TIME)) && (!(RTC_Hour < 0))) || (((RTC_Hour > NIGHT_TIME) || (RTC_Hour == NIGHT_TIME)) && (RTC_Hour < 24)))//夜间判断
	{
		return 0;
	}
	else
	{
		return 1;
	}
}

u8 Is_Time_To_Let_Gas_Out(void)
{
	/**释放时间判断部分**/	
		
	if((Outtime_Hour[0])||(Outtime_Min[0]))
	{
		if(((RTC_Hour==Outtime_Hour[0])&&(RTC_Min==Outtime_Min[0]))/*小时、分钟都相等*/
		|| ((RTC_Hour==Outtime_Hour[0])&&(RTC_Min >Outtime_Min[0]))/*小时相等，分钟大于*/
		|| ((RTC_Hour >Outtime_Hour[0])&&((RTC_Hour<(Outtime_Hour[0]+Out_TimeLong))))	/*小时大于，分钟相等，并且不超过释放时长*/
		|| ((RTC_Hour==(Outtime_Hour[0]+Out_TimeLong))&&(RTC_Min<Outtime_Min[0]))	)/*小时大于，分钟大于，并且不超过释放时长*/
		{
			Time_flag = 1;
			Time_Enable =0;
			return Time_flag;
		}
		else
		{
			Time_flag = 0;
		}
	}
	if((Outtime_Hour[1])||(Outtime_Min[1]))
	{
		if(((RTC_Hour==Outtime_Hour[1])&&(RTC_Min==Outtime_Min[1]))/*小时、分钟都相等*/
		|| ((RTC_Hour==Outtime_Hour[1])&&(RTC_Min >Outtime_Min[1]))/*小时相等，分钟大于*/
		|| ((RTC_Hour >Outtime_Hour[1])&&((RTC_Hour<(Outtime_Hour[1]+Out_TimeLong))))	/*小时大于，分钟相等，并且不超过释放时长*/
		|| ((RTC_Hour==(Outtime_Hour[1]+Out_TimeLong))&&(RTC_Min<Outtime_Min[1]))	)/*小时大于，分钟大于，并且不超过释放时长*/
		{
			Time_flag = 1;
			Time_Enable =1;
			return Time_flag;
		}
		else
		{
			Time_flag = 0;
		}
	}
	if((Outtime_Hour[2])||(Outtime_Min[2]))
	{
		if(((RTC_Hour==Outtime_Hour[2])&&(RTC_Min==Outtime_Min[2]))/*小时、分钟都相等*/
		|| ((RTC_Hour==Outtime_Hour[2])&&(RTC_Min >Outtime_Min[2]))/*小时相等，分钟大于*/
		|| ((RTC_Hour >Outtime_Hour[2])&&((RTC_Hour<(Outtime_Hour[2]+Out_TimeLong))))	/*小时大于，分钟相等，并且不超过释放时长*/
		|| ((RTC_Hour==(Outtime_Hour[2]+Out_TimeLong))&&(RTC_Min<Outtime_Min[2]))	)/*小时大于，分钟大于，并且不超过释放时长*/
		{
			Time_flag = 1;
			return Time_flag;
		}
		else
		{
			Time_flag = 0;
		}
	}
	return Time_flag;	
	
}

u8 wind_level(float wind_rank_temp)
{
		u8 wind_rank;
		if(wind_rank_temp<0.2)
		{
			wind_rank =0;
		}
		else if((wind_rank_temp<1.5)&&(wind_rank_temp>0.3))
		{
			wind_rank =1;
		}
		else if((wind_rank_temp<3.3)&&(wind_rank_temp>1.6))
		{
			wind_rank =2;
		}
		else if((wind_rank_temp<5.4)&&(wind_rank_temp>3.4))
		{
			wind_rank =3;
		}
		else if((wind_rank_temp<7.9)&&(wind_rank_temp>5.5))
		{
			wind_rank =4;
		}
		else if((wind_rank_temp<10.7)&&(wind_rank_temp>8.0))
		{
			wind_rank =5;
		}
		else if((wind_rank_temp<13.8)&&(wind_rank_temp>10.8))
		{
			wind_rank =6;
		}
		else if((wind_rank_temp<17.1)&&(wind_rank_temp>13.9))
		{
			wind_rank =7;
		}
		else if((wind_rank_temp<20.7)&&(wind_rank_temp>17.2))
		{
			wind_rank =8;
		}
		return wind_rank;
}

//GPRS发送任务
void IP_Set(void)
{
	sprintf(ipstr, "%d.%d.%d.%d",((Board.serverIP)>>24)&0xFF,((Board.serverIP)>>16)&0xFF,((Board.serverIP)>>8)&0xFF,((Board.serverIP) & 0xFF));
	sprintf(portstr, "%d", Board.port);
}

void GPRS_PWR_ON(void)
{
	/*GPRS开机*/
		u8 i;
			
			i = 0;
			do
			{	
					i++;
		      Power_GPRS_On();
			    GPRS_Init();
			    Delay(200);
			    GPRS_PWERKEY_Low();
		      Delay(1300);
			    GPRS_PWERKEY_High();
		      Delay(2500);
					IWDG_ReloadCounter();

			}while((GPRS_STATUS_OK() == 0)&&(i<3));
			for(i=0;i<10;i++)
			{
				Delay_ms(150);//延时10s
			}
}

void GPRS_TCP_IP(void)
{
		//			/*建立TCPIP连接，连续三次不成功取消任务*/
	u8 i,j;
	i=0;
	TCPIP_Status = 0;
	do
	{
		i++;
		GPRS_Cmd_status = NOCMD;
		GPRS_ERR_Status = GPRS_ERR_NOERR;
		GPRS_Send_ATCmd("ATE0\r\n", ATE, GPRS_ERR_ATE, 200); //127
		GPRS_TCP_Connect(ipstr, portstr);
		for(j=0;j<10;j++)
		{
		Delay(300);//3614ms
		}
		IWDG_ReloadCounter();

	}while((TCPIP_Status == 0)&&(i<3));
}

void GPS_Process()
{
		u8 i;
		GPS_Init();
			i = 0;
			do
			{
						i++;
						GPRMC_Flag = 0;
						while(GPRMC_Flag == 0);
			}while((GPS_info.Status != 'A')&&(i<3));
			GPRMC_Flag = 0;
			
			if(GPS_info.Status != 'A')
			{
				
					for(i=0; i<11; i++)
					{
						GPS_info.Longitude[i] = '0';
					}
					GPS_info.Longitude[i] = '\0';	
					GPS_info.Longitude_EW = '0';
					for(i=0; i<10; i++)
					{
						GPS_info.Latitude[i] = '0';
					}
					GPS_info.Latitude[i] = '\0';
					GPS_info.Latitude_NS = '0';
					
			}
			Power_GPS_Off();
			GPS_DeInit();
//		for(i=0; i<11; i++)
//		{
//			GPS_info.Longitude[i] = '0';
//		}
//		GPS_info.Longitude[i] = '\0';	
//		GPS_info.Longitude_EW = '0';
//		for(i=0; i<10; i++)
//		{
//			GPS_info.Latitude[i] = '0';
//		}
//		GPS_info.Latitude[i] = '\0';
//		GPS_info.Latitude_NS = '0';
}

u8 GPRS_Send_data(u8 Send_flag)
{
	u8 i;
	u32 temp_GPRS;
	temp_GPRS =	(u32)((float)(tem1/10)+273.1);
	
	sprintf(data, "=%08d %04d %01d %05d %04d %05d %04d %s %c %s %c %1d %1d %05d %05d %05d %04d %010d\n", Board.boardid, sn, Board.gasindex, gas.weight, gas.percentage, gas.KPA*10, V_bat,
																																														 GPS_info.Longitude, GPS_info.Longitude_EW, GPS_info.Latitude, GPS_info.Latitude_NS,
																																								(uint32_t)Solenoid, Load_Flag, cur_add_weight, cur_full_weight, cur_full_weight-cur_add_weight,(uint32_t)(temp_GPRS*10),(uint32_t)cur_total_volume);

	
	//printf(data);
	/*发送数据*/
	
	i = 0;
	SendOK = 0;
	do
	{	
		
		i++;
		GPRS_Send(data);
		Delay(4000);
		IWDG_ReloadCounter();
		
	}while((SendOK == 0)&&(i<3));
	////			if(SendOK == 1)
	{
		 if(Load_Flag == 1)
		{
			 Load_Flag = 2;

		}
		else if(Load_Flag == 3)
		{
			 Load_Flag = 0;
			 W_Load_Before = 0;
			 W_Load_After = 0;
			 W_Load = 0;
		}
	}
				/*关闭TCP连接*/			
	GPRS_TCP_Close();
	return SendOK;			
}

void write_Poweroff(void)
{
	
			EE_WriteVariable(VirtAddVarTab[51], (cur_full_weight>>16)&0xffff);
			EE_WriteVariable(VirtAddVarTab[52], cur_full_weight&0xffff);
			
	
}
void Volume_Poweroff(void)
{
	
			EE_WriteVariable(VirtAddVarTab[41], (total_weight>>16)&0xffff);
			EE_WriteVariable(VirtAddVarTab[42], total_weight&0xffff);
}

void NVIC_ALL_Configuration(void)
{
	NVIC_InitTypeDef NVIC_InitStructure; 
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	/**风速**/
	NVIC_InitStructure.NVIC_IRQChannel = EXTI15_10_IRQn;				
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=2 ;//抢占优先级1
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;		//响应优先级0
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;			//IRQ通道使能
	NVIC_Init(&NVIC_InitStructure);	//根据指定的参数初始化VIC寄存器
	
	/**串口显示**/
	NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn; 
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;				
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;         
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;         
	NVIC_Init(&NVIC_InitStructure); 
	
	/*读取串口RTC*/
	NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn; 
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;         
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;         
	NVIC_Init(&NVIC_InitStructure); 
	
	//正常释放10s
	NVIC_InitStructure.NVIC_IRQChannel = TIM4_IRQn; 
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;         
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;         
	NVIC_Init(&NVIC_InitStructure); 
	
	/**433接收**/
	NVIC_InitStructure.NVIC_IRQChannel = UART5_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure); 
	
	//配置串口屏
	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
	
			
			//串口4		调试用
//			NVIC_InitStructure.NVIC_IRQChannel = UART4_IRQn;
//			NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
//			NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
//			NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
//			NVIC_Init(&NVIC_InitStructure);
					
}

//获取校准数据
void Get_Adjust_Data(void)
{
	if(Weight_low_set_flag==1)										//液位低点校准
	{
		Weight_low_set_flag =0;	
		diffpressre_calibration_low();			
	}
	
	if(Weight_high_set_flag==1)								//液位高点校准
	{
		Weight_high_set_flag =0;	
		diffpressre_calibration_high();
	}
	
	if(Pressure_low_set_flag==1)							//压力校准
	{
		Pressure_low_set_flag =0;	
		pressre_calibration_low();
	}
	
	if(Pressure_high_set_flag==1)					//压力高点校准
	{
		Pressure_high_set_flag =0;	
		pressre_calibration_high();	
	}
}


u8 Valve_Task(u8 State_Of_Task)
{
	u8 i;
	switch(State_Of_Task)
	{
		case TURN_ON:								//打开阀门
			//while(Liq_Out_Open_OK)				//阀门处于未完全打开状态
			//{
				Liq_Out_on();	
				IWDG_ReloadCounter();//打开阀门
			//}
			if(!Liq_Out_Open_OK)			//阀门处于完全打开状态
			{
				Solenoid=1;							//阀门开状态
//				for(i=0;i<3;i++)
//				{
//					Beep_On();
//					Delay_ms(100);
//					Beep_Off();
//					Delay_ms(100);
//				}
			}
			break;
		case TURN_OFF:							//关闭阀门
			//while(Liq_Out_Close_OK)			//阀门处于未完全关闭状态
			//{
				Liq_Out_off();
				IWDG_ReloadCounter();				//关闭阀门
			//}
			if(!Liq_Out_Close_OK)			//阀门处于完全关闭状态
			{
				Solenoid=0;
//				for(i=0;i<3;i++)
//				{
//					Beep_On();
//					Delay_ms(10);
//					Beep_Off();
//					Delay_ms(100);
//				}				//阀门关状态
			}
			
			break;
		default:
			break;
	}
	return Solenoid;		//返回阀门状态
}	


void Huanjing_Default(void)
{
	Huanjing_Value[0]=temp_top;
	Huanjing_Value[1]=temp_below;
	Huanjing_Value[2]=hum_top;
	Huanjing_Value[3]=hum_below;
	Huanjing_Value[4]=CO2_TOP_default;
	Huanjing_Value[5]=con_below;
	Huanjing_Value[6]=light_top;
	Huanjing_Value[7]=light_below;
	Huanjing_Value[8]=wind_top;
	Huanjing_Value[9]=wind_below;
	Huanjing_Value[10]=pre_top;
	Huanjing_Value[11]=pre_below;
	Huanjing_Value[12]=weight_top;
	Huanjing_Value[13]=weight_below;
}

void Huanjing_Disp_default(u8 *Var_Add,u16 *Var_Value)
{
		u8 i,j;
		for(i=0;i<14;i++)
		{
			Huanjing_Disp_Ins[5]= Var_Add[i];
			Huanjing_Disp_Ins[6]= Var_Value[i]>>8;
			Huanjing_Disp_Ins[7]= Var_Value[i];
			for(j = 0;j<8;j++)//
			{
					USART_SendData(USART1,Huanjing_Disp_Ins[j]);
					while(USART_GetFlagStatus(USART1, USART_FLAG_TC)==RESET);
			}
		}
}


void Time_Default_Disp(u8 *Out_Time_Add,u8 *Out_Time_H_Value,u8 *Out_Time_M_Value)
{
		u8 i,j;
		for(i=0;i<3;i++)
		{
			Huanjing_Disp_Ins[5]= Out_Time_Add[i];
			Huanjing_Disp_Ins[6]= Out_Time_H_Value[i]>>8;
			Huanjing_Disp_Ins[7]= Out_Time_H_Value[i];
			for(j = 0;j<8;j++)//
			{
					USART_SendData(USART1,Huanjing_Disp_Ins[j]);
					while(USART_GetFlagStatus(USART1, USART_FLAG_TC)==RESET);
			}
		}
		for(i=3;i<6;i++)
		{
			Huanjing_Disp_Ins[5]= Out_Time_Add[i];
			Huanjing_Disp_Ins[6]= Out_Time_M_Value[i-3]>>8;
			Huanjing_Disp_Ins[7]= Out_Time_M_Value[i-3];
			for(j = 0;j<8;j++)//
			{
					USART_SendData(USART1,Huanjing_Disp_Ins[j]);
					while(USART_GetFlagStatus(USART1, USART_FLAG_TC)==RESET);
			}
		}
}



