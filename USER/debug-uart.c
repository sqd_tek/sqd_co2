#include <debug-uart.h>
#include <string.h>
#include <stm32f10x.h>

void Uart5_Config_Init(void)														//433通信
{
	
		GPIO_InitTypeDef GPIO_InitStructure;
	  USART_InitTypeDef USART_InitStructure;
	  //NVIC_InitTypeDef NVIC_InitStructure; 
	  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC |RCC_APB2Periph_GPIOD |RCC_APB2Periph_AFIO, ENABLE); 
	  RCC_APB1PeriphClockCmd(RCC_APB1Periph_UART5, ENABLE);  //????5?? 
            
	  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;		//TX	
	  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	  GPIO_Init(GPIOC, &GPIO_InitStructure);
	  
	  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;		//RX
	  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	  GPIO_Init(GPIOD, &GPIO_InitStructure);
	  
	  USART_InitStructure.USART_BaudRate = 9600;
    USART_InitStructure.USART_WordLength = USART_WordLength_8b;
    USART_InitStructure.USART_StopBits = USART_StopBits_1;
    USART_InitStructure.USART_Parity = USART_Parity_No;
		USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
    USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
    USART_Init(UART5, &USART_InitStructure);
   
			  
    USART_ITConfig(UART5, USART_IT_RXNE, ENABLE); 
    USART_Cmd(UART5, ENABLE);



}
void	Uart_TFTLCD(void)								//串口屏
{
	  GPIO_InitTypeDef GPIO_InitStructure;
	  USART_InitTypeDef USART_InitStructure;
	  //NVIC_InitTypeDef NVIC_InitStructure;
	  
	  RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_AFIO |
	                         RCC_APB2Periph_USART1 , ENABLE);
	                        
	  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
	  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	  GPIO_Init(GPIOA, &GPIO_InitStructure);  
	  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
	  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	  GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	  
	  USART_InitStructure.USART_BaudRate = 115200;
      USART_InitStructure.USART_WordLength = USART_WordLength_8b;
      USART_InitStructure.USART_StopBits = USART_StopBits_1;
      USART_InitStructure.USART_Parity = USART_Parity_No;
      USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
      USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
      USART_Init(USART1, &USART_InitStructure);
			//(USART2, &USART_InitStructure);  // 初始化串口2结构体
    
     // #ifdef SIM900A_USE
		
    	  
      USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
	 // #endif
      USART_Cmd(USART1, ENABLE);	
	
}
void uart3_config(void)
{
	  GPIO_InitTypeDef GPIO_InitStructure;
	  USART_InitTypeDef USART_InitStructure;
	  //NVIC_InitTypeDef NVIC_InitStructure;
	  
	 RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB , ENABLE); //??UART3??GPIOB???
   RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);
	                        
	  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
   GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;
   GPIO_Init(GPIOB, &GPIO_InitStructure);

   // Configure USART2 Tx (PB.10) as alternate function push-pull
   GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
   GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
   GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
   GPIO_Init(GPIOB, &GPIO_InitStructure);
	
	  
	  USART_InitStructure.USART_BaudRate = 115200;
      USART_InitStructure.USART_WordLength = USART_WordLength_8b;
      USART_InitStructure.USART_StopBits = USART_StopBits_1;
      USART_InitStructure.USART_Parity = USART_Parity_No;
      USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
      USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
      USART_Init(USART3, &USART_InitStructure);
    
     // #ifdef SIM900A_USE
		//配置串口1中断
	  
    	  
      USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);
	 // #endif
      //USART_Cmd(USART3, ENABLE);	
	
}


int sendchar(int ch)
{
    
	UART5->SR;
	USART_SendData(UART5, (uint8_t) ch);
 
    /* Loop until the end of transmission */
    while(USART_GetFlagStatus(UART5, USART_FLAG_TC) == RESET)
    {
    }

    return ch;	
}

//int getkey(void)
//{ 
//	
//}

