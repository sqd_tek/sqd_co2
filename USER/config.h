
#ifndef __PROJECT_CONFIG_H
#define __PROJECT_CONFIG_H

#include "GPS_dev.h"
#include "GPRS_dev.h"

#include "GPS_Task.h"
#include "GPRS_Task.h"
#include "Delay.h"
#include "AD7794.h"
#include "LowPower.h"
#include "Platform.h"
#include "LCD_Task.h"
#include "LCD.h"
#include "eeprom.h"
#include "ADXL345_Dev.h"
#include <debug-uart.h>

#include "BH1750.h"
#include "DHT22.h"
#include "433.h"
//#include "wind_speed.h"
#include "Uart_debug.h"

/*通讯方式配置
  定义SIM900A_USE 使用sim900a通讯，调试串口可用
  否则采用串口0通讯，调试功能不可用
*/
//#define SIM900A_USE 1

#endif
