/* Includes ------------------------------------------------------------------*/
#include "stm32f10x_it.h"
#include "Platform.h"
#include "LowPower.h"
#include "LCD_Task.h"
#include "LCD.h"
#include <string.h>
#include "eeprom.h"
#include "433.h"

#define  ADDR 0x0800F000
#define FLASH_PAGE_SIZE   2048 //如果一页为2K大小
#define PACK_LEN 25
#define SYS_Admin 111      //设置系统管理员密码
#define VALVE_Admin 222			//设置阀门管理员密码
#define CLIENT_NUM 10					//从机总个数
#define	Fresh_Time 3					//刷新时间
#define Time_Per_One 8
#define TH_DATA_FRESH_TIME  3

u8 Recv1_data(void);
u8 Recv5_data(void);

unsigned short CRC16Table[] = 
{  
0x0, 0x1021, 0x2042, 0x3063, 0x4084, 0x50A5, 0x60C6, 0x70E7,  
0x8108, 0x9129, 0xA14A, 0xB16B, 0xC18C, 0xD1AD, 0xE1CE, 0xF1EF,  
0x1231, 0x210, 0x3273, 0x2252, 0x52B5, 0x4294, 0x72F7, 0x62D6,  
0x9339, 0x8318, 0xB37B, 0xA35A, 0xD3BD, 0xC39C, 0xF3FF, 0xE3DE,  
0x2462, 0x3443, 0x420, 0x1401, 0x64E6, 0x74C7, 0x44A4, 0x5485,  
0xA56A, 0xB54B, 0x8528, 0x9509, 0xE5EE, 0xF5CF, 0xC5AC, 0xD58D,  
0x3653, 0x2672, 0x1611, 0x630, 0x76D7, 0x66F6, 0x5695, 0x46B4,  
0xB75B, 0xA77A, 0x9719, 0x8738, 0xF7DF, 0xE7FE, 0xD79D, 0xC7BC,  
0x48C4, 0x58E5, 0x6886, 0x78A7, 0x840, 0x1861, 0x2802, 0x3823,  
0xC9CC, 0xD9ED, 0xE98E, 0xF9AF, 0x8948, 0x9969, 0xA90A, 0xB92B,  
0x5AF5, 0x4AD4, 0x7AB7, 0x6A96, 0x1A71, 0xA50, 0x3A33, 0x2A12,  
0xDBFD, 0xCBDC, 0xFBBF, 0xEB9E, 0x9B79, 0x8B58, 0xBB3B, 0xAB1A,  
0x6CA6, 0x7C87, 0x4CE4, 0x5CC5, 0x2C22, 0x3C03, 0xC60, 0x1C41,  
0xEDAE, 0xFD8F, 0xCDEC, 0xDDCD, 0xAD2A, 0xBD0B, 0x8D68, 0x9D49,  
0x7E97, 0x6EB6, 0x5ED5, 0x4EF4, 0x3E13, 0x2E32, 0x1E51, 0xE70,  
0xFF9F, 0xEFBE, 0xDFDD, 0xCFFC, 0xBF1B, 0xAF3A, 0x9F59, 0x8F78,  
0x9188, 0x81A9, 0xB1CA, 0xA1EB, 0xD10C, 0xC12D, 0xF14E, 0xE16F,  
0x1080, 0xA1, 0x30C2, 0x20E3, 0x5004, 0x4025, 0x7046, 0x6067,  
0x83B9, 0x9398, 0xA3FB, 0xB3DA, 0xC33D, 0xD31C, 0xE37F, 0xF35E,  
0x2B1, 0x1290, 0x22F3, 0x32D2, 0x4235, 0x5214, 0x6277, 0x7256,  
0xB5EA, 0xA5CB, 0x95A8, 0x8589, 0xF56E, 0xE54F, 0xD52C, 0xC50D,  
0x34E2, 0x24C3, 0x14A0, 0x481, 0x7466, 0x6447, 0x5424, 0x4405,  
0xA7DB, 0xB7FA, 0x8799, 0x97B8, 0xE75F, 0xF77E, 0xC71D, 0xD73C,  
0x26D3, 0x36F2, 0x691, 0x16B0, 0x6657, 0x7676, 0x4615, 0x5634,  
0xD94C, 0xC96D, 0xF90E, 0xE92F, 0x99C8, 0x89E9, 0xB98A, 0xA9AB,  
0x5844, 0x4865, 0x7806, 0x6827, 0x18C0, 0x8E1, 0x3882, 0x28A3,  
0xCB7D, 0xDB5C, 0xEB3F, 0xFB1E, 0x8BF9, 0x9BD8, 0xABBB, 0xBB9A,  
0x4A75, 0x5A54, 0x6A37, 0x7A16, 0xAF1, 0x1AD0, 0x2AB3, 0x3A92,  
0xFD2E, 0xED0F, 0xDD6C, 0xCD4D, 0xBDAA, 0xAD8B, 0x9DE8, 0x8DC9,  
0x7C26, 0x6C07, 0x5C64, 0x4C45, 0x3CA2, 0x2C83, 0x1CE0, 0xCC1,  
0xEF1F, 0xFF3E, 0xCF5D, 0xDF7C, 0xAF9B, 0xBFBA, 0x8FD9, 0x9FF8,  
0x6E17, 0x7E36, 0x4E55, 0x5E74, 0x2E93, 0x3EB2, 0xED1, 0x1EF0  
} ;

//环境变量

extern u16 temp_top;//温度上限
extern u16 temp_below;//下限
extern u16 hum_top;     	//湿度上限
extern u16 hum_below;
extern u16 con_top;		//浓度上限
extern u16 con_below;
extern u16 light_top;	//光照上限
extern u16 light_below;
extern u16 wind_top;	//风速上限
extern u16 wind_below;
extern u16 pre_top;	//压力上限
extern u16 pre_below;
extern u16 weight_top;	//重量上限
extern u16 weight_below;
extern u8 Login_Succ_flag;
extern u8 RTC_Hour;
extern u8 RTC_Min;
u8 RTC_Hour_temp;
u8 RTC_Min_temp;
//extern u8 RTC_Month;
//extern u8 RTC_Day;
extern u8 Outtime_Hour[3];
extern u8 Outtime_Min[3];
extern u8 Outtime_Sec[3];
extern u16 Disp_time;
extern u8 Disp_time_New;

extern u8 Pressure_low_set_flag;					//压力低点校准标志
extern u8 Pressure_high_set_flag;					//压力高点校准标志
extern u8 Weight_low_set_flag;						//液位重量低点
extern u8 Weight_high_set_flag;

extern Board_Type Board;

extern u8 Out_TimeLong;

extern char Send_Cmd[4];
extern u8 Open_flag_Manual;
extern u8 Open_flag_Auto;
extern u8 Disp_flag_Auto;								//刷新命令标志	
extern u8 Disp_flag_Manual;
extern u8 Lock_flag;								//关闭命令标志	
extern u8 Send_counter;

extern uint32_t cur_weight;
extern uint32_t  total_weight;
extern uint32_t cur_add_weight;
extern uint32_t cur_full_weight;
extern uint32_t  cur_total_weight;
extern uint32_t start_weight;
extern uint32_t cur_total_volume;			 //总消耗流量
extern u32 exchange1,exchange;				//总流量保存变量

void USART1_IRQHandler(void)//串口屏通信中断
{
		static u8 buff[11] = {0};
		static u8 state = 0;
		u8 j;
		u16 tmp = 0;
		u16 tmp1 = 0;
	  u32 fuck = 0;
		u8 exchang = 0;
		static u16 chg_passwd = 0;//修改系统密码标识
		static u32 new_passwd = 0;//系统新密码
		static u32 new_passwd1 = 1;//系统重新输入的密码
		
		static u16 chg_passwd1 = 0;//修改入口阀密码标识
		static u32 new_paswd = 0;//系统新密码
		static u32 new_paswd1 = 1;//系统重新输入的密码
		
		u8 hehe[7] = {0x5A,0xA5,0x04,0x80,0x03,0x00,0x01};//切换界面1
		//u8 diffpressure_low_display[]
		u8 valve_light[8] = {0x5A,0xA5,0x05,0x82,0x00,0x50,0x00,0x01};
		u8 log_flag[8] = {0x5A,0xA5,0x05,0x82,0x00,0x50,0x00,0x01}; //绿色
		u8 time_disp[8]={0x5A,0xA5,0x05,0x82,0x00,0x80,0x00,0x00};
		if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)
    {

						
						buff[state] = Recv1_data();
						if(buff[0] != 0x5A)
						{
								return;
						}
						state++;
						
						
						/**罐体液位重量、与压力参数校准**/

						if((state>8)&&(buff[2] == 0x06)&&(buff[4] == 0) &&(buff[5] > 0x97)&&(buff[5] < 0x9f))					//
								{
										state =0;
										if(buff[5]==0x98)
										{
											if(!Weight_low_set_flag)
											{
											Weight_low_set_flag =1;				//液位低点校准
											Board.diffpressure1= (buff[7]<<8) + buff[8];
											}
										}
										
										if(buff[5]==0x9a)							//液位高点校准
										{
											if(!Weight_high_set_flag)
											{
											Weight_high_set_flag =1;
											Board.diffpressure2= (buff[7]<<8) + buff[8];
											}
										}
										
										if(buff[5]==0x9c)							//压力低点校准
										{
										if(!Pressure_low_set_flag)
										{
											Pressure_low_set_flag =1;
											Board.pressure1 = (buff[7]<<8) + buff[8];
										}
										}
										
										if(buff[5]==0x9e)							//压力高点校准
										{
											if(!Pressure_high_set_flag)
											{
											Pressure_high_set_flag =1;
											Board.pressure2 = (buff[7]<<8) + buff[8];
											}
										}

								}
						
								
						if((state>8)&&(buff[2] == 0x06)&&(buff[3] == 0x83) &&(buff[5]==0xc0))								//手动刷新功能
						{
											if((!Disp_flag_Manual)&&(!Open_flag_Manual)&&(!Open_flag_Auto)&&(!Disp_flag_Auto))
											{
											Disp_flag_Manual =1;
											Disp_flag_Auto =0;
											}
						}
						
						if((state>8)&&(buff[2] == 0x06)&&(buff[3] == 0x83) &&(buff[5]==0xc2))							//手动发送正常释放指令
						{
											
											Open_flag_Manual =1;
											Disp_flag_Auto =0;
											Disp_flag_Manual=0;
											Lock_flag=0;
											
						}
						
					  if((state>8)&&(buff[2] == 0x06)&&(buff[3] == 0x83) &&(buff[5]==0xc1))								//关闭功能
						{

								if(!Lock_flag)
								{
									Lock_flag =1;
									Open_flag_Auto =0;
									Open_flag_Manual =0;
								}
								Disp_flag_Auto =0;
								Disp_flag_Manual =0;

						}
						
						
						if((state>8)&&(buff[2] == 0x06) && (buff[3] == 0x81)&& (buff[4] == 0x24)&& (buff[5] == 0x03))// 获取实时时钟	-----小时、分钟
							{
									state =0;
									RTC_Hour_temp	=buff[6];			//小时  
									
									RTC_Min_temp	=buff[7];			//分钟
									
									RTC_Hour = (RTC_Hour_temp>>4)*10+(RTC_Hour_temp%16);					//十六进制转化为十进制
									RTC_Min = (RTC_Min_temp>>4)*10+(RTC_Min_temp%16);
									Disp_time_New = RTC_Min;
								
									for(j = 0;j<11;j++)
									{
										buff[j]=0;
									}
							}		
							
//							if((state>8)&&(buff[2] == 0x06) && (buff[3] == 0x81)&& (buff[4] == 0x20)&& (buff[5] == 0x07))				//------月、日
//							{
//								state =0;
//									RTC_Month	=buff[7];			//月
//									
//									RTC_Day	=buff[7];			//日
//									//RTC_Min	=buff[8];			//秒
//									for(j = 0;j<11;j++)
//										{
//										buff[j]=0;
//										}
//							
//							}
						

						if((state>8)&&(buff[2] == 0x06)&&(buff[4] == 0) &&(buff[5] > 0x85)&&((buff[5] < 0x8b)||(buff[5] == 0x8b)))				//获取设置的时间
						{
								state =0;
								if(buff[5]==0x86)				//预设释放时间1时
								{
									Outtime_Hour[0] =buff[8];
									//1时显示函数
									time_disp[7] = buff[8];
									time_disp[5] = 0x80;
									for(j = 0;j<8;j++)//识别密码
										{
													USART_SendData(USART1,time_disp[j]);
													while(USART_GetFlagStatus(USART1, USART_FLAG_TC)==RESET);
										}
								}
								if(buff[5]==0x87)
								{
									Outtime_Min[0] =buff[8];
									//1分显示函数
									time_disp[7] = buff[8];
									time_disp[5] = 0x81;
									for(j = 0;j<8;j++)//识别密码
										{
													USART_SendData(USART1,time_disp[j]);
													while(USART_GetFlagStatus(USART1, USART_FLAG_TC)==RESET);
										}
								}
								if(buff[5]==0x88)				//预设释放时间2时
								{
									Outtime_Hour[1] =buff[8];
									//2时显示函数
									time_disp[7] = buff[8];
									time_disp[5] = 0x82;
									for(j = 0;j<8;j++)//识别密码
										{
													USART_SendData(USART1,time_disp[j]);
													while(USART_GetFlagStatus(USART1, USART_FLAG_TC)==RESET);
										}
								}
								if(buff[5]==0x89)
								{
									Outtime_Min[1] =buff[8];
									//2分显示
									time_disp[7] = buff[8];
									time_disp[5] = 0x83;
									for(j = 0;j<8;j++)//识别密码
										{
													USART_SendData(USART1,time_disp[j]);
													while(USART_GetFlagStatus(USART1, USART_FLAG_TC)==RESET);
										}
								}
								if(buff[5]==0x8a)				//预设释放时间3时
								{
									Outtime_Hour[2] =buff[8];
									//3时显示函数
									time_disp[7] = buff[8];
									time_disp[5] = 0x84;
									for(j = 0;j<8;j++)//识别密码
										{
													USART_SendData(USART1,time_disp[j]);
													while(USART_GetFlagStatus(USART1, USART_FLAG_TC)==RESET);
										}
								}
								if(buff[5]==0x8b)
								{
									Outtime_Min[2] =buff[8];
									//3分显示
									time_disp[7] = buff[8];
									time_disp[5] = 0x85;
									for(j = 0;j<8;j++)//识别密码
										{
													USART_SendData(USART1,time_disp[j]);
													while(USART_GetFlagStatus(USART1, USART_FLAG_TC)==RESET);
										}
								}
								for(j = 0;j<11;j++)
								{
									buff[j]=0;
								}
						}
					
						if((state == (buff[2] + 3))&&(state > 2))
						{
								
								state = 0;
//								if((chg_passwd == 0) ||(chg_passwd1 == 0))
//								{
//									
//											log_flag[7] = 0;
//											log_flag[3] = 0x80;
//											for(j = 0;j<8;j++)//识别密码
//											{
//																USART_SendData(USART1,log_flag[j]);//关指示灯
//																while(USART_GetFlagStatus(USART1, USART_FLAG_TC)==RESET);
//											}		
//											log_flag[3] = 0x82;
//											log_flag[7] = 0;
//											for(j = 0;j<8;j++)//识别密码 
//											{
//																
//														USART_SendData(USART1,log_flag[j]);//关指示灯
//														while(USART_GetFlagStatus(USART1, USART_FLAG_TC)==RESET);
//																
//											}		
//								
//								}
								if((buff[2] == 8)&&(buff[5] == 0))//登陆
								{
										chg_passwd = 0;	
										chg_passwd1 = 0;
										fuck = ((buff[7] << 24) + (buff[8] << 16) + (buff[9] << 8) +buff[10]);		
										//写flash,并且在特定位置进行flash标记是否是第一次操作
										EE_ReadVariable(VirtAddVarTab[32], &tmp);
										if(tmp != 5)
										{
													
													FLASH_Unlock();
													//EE_Init();
//												Flash_Erase(ADDR);99
//												FLASH_Lock();
//												FLASH_ProgramWord((ADDR+10),0);//入口阀密码
//												FLASH_ProgramWord(ADDR,0);//登陆密码
//												FLASH_ProgramWord((ADDR+20),5);//位置标记
													EE_WriteVariable(VirtAddVarTab[30], (u16)0);//登陆密码
													EE_WriteVariable(VirtAddVarTab[31], (u16)0);
													EE_WriteVariable(VirtAddVarTab[32], (u16)5);//位置标记
													EE_WriteVariable(VirtAddVarTab[33], (u16)0);
													EE_WriteVariable(VirtAddVarTab[34], (u16)0);//入口阀密码
													
										}
										else
										{
										EE_ReadVariable(VirtAddVarTab[30], &tmp);
										EE_ReadVariable(VirtAddVarTab[31], &tmp1);
										}
										if(((tmp<<16 )+ tmp1) == fuck)
										{
											
												for(j = 0;j<7;j++)//识别密码
												{
														USART_SendData(USART1,hehe[j]);//切换屏幕到页面1
														while(USART_GetFlagStatus(USART1, USART_FLAG_TC)==RESET);
												}		
										
										}
										if(fuck == SYS_Admin)//管理员权限
										{
											
												for(j = 0;j<7;j++)//识别密码
												{
														USART_SendData(USART1,hehe[j]);//切换屏幕到页面1
														while(USART_GetFlagStatus(USART1, USART_FLAG_TC)==RESET);
												}		
										
										}
										
								}
								EE_ReadVariable(VirtAddVarTab[33], &tmp);
								EE_ReadVariable(VirtAddVarTab[34], &tmp1);
								if((buff[2] == 8) && (buff[5] == 0x52))//入口阀密码打开阀门
								{
									
										chg_passwd = 0;	
										chg_passwd1 = 0;
										//读取flash与当前的数据比较，进行入口阀的开启
										fuck = ((buff[7] << 24) + (buff[8] << 16) + (buff[9] << 8) +buff[10]);
										if(((tmp<<16)+ tmp1) == fuck) //验证密码正确
										{
											
												//发送命令打开阀门
												Liq_In_on_2();
												
												for(j = 0;j<8;j++)//识别密码
												{
														USART_SendData(USART1,valve_light[j]);//打开指示灯
														while(USART_GetFlagStatus(USART1, USART_FLAG_TC)==RESET);
												}		
										
										}
										if(fuck == VALVE_Admin)//管理员权限
										{
											
												//发送命令打开阀门
												Liq_In_on_2();
											
												//总流量计算
												cur_add_weight = gas.weight;				//采集充液开始时重量
												total_weight = total_weight + cur_full_weight + start_weight - cur_add_weight;
												start_weight =0;
												//TIM_Cmd(TIM2,DISABLE);					//充液开始时，先关定时计数器，等待充液完成
												Load_Flag =1;

												
											
												for(j = 0;j<8;j++)//识别密码
												{
														USART_SendData(USART1,valve_light[j]);//切换屏幕到页面1
														while(USART_GetFlagStatus(USART1, USART_FLAG_TC)==RESET);
												}		
										
										}
										
								}
								if(buff[5] == 0x54)//点击关闭入口阀按钮
								{
									//关闭阀门
									Liq_In_off_2();
									
							cur_full_weight = gas.weight;				//充装结束，采集充液完成时重量
							Load_Flag =3;
									
									
									valve_light[7] = 0;
									for(j = 0;j<8;j++)//识别密码
									{
												USART_SendData(USART1,valve_light[j]);//切换屏幕到页面1
												while(USART_GetFlagStatus(USART1, USART_FLAG_TC)==RESET);
									}		
									
								}
								
								if((buff[2] == 6) &&(buff[5] > 0x59))//设置环境变量
								{
									if(buff[5] < 0x76)
									{
										chg_passwd = 0;	
										chg_passwd1 = 0;
									}
									exchang = buff[5] - 0x60;
									switch (exchang)
									{
										
										case 0:
										//温度上限
										temp_top = ((buff[7] <<8) + buff[8]);
										//temp_top = temp_top*10;
										break;
										case 1:
										//下限
										temp_below = (buff[7] <<8) + buff[8];
										//temp_below =temp_below*10;
										break;
										case 2:
										//湿度上限
										hum_top =  (buff[7] <<8) + buff[8];
										//hum_top =hum_top*10;
										break;
										case 3:
										//湿度下限
										hum_below = (buff[7] <<8) + buff[8];
										//hum_below =hum_below*10;
										break;
										case 4:
										//浓度上限
										con_top = (buff[7] <<8) + buff[8];
										
										//con_top = con_top*10;
//										if((con_top<800)||(con_top==800))
//										{
//											Out_TimeLong =2;
//										}
//										if(con_top>800)
//										{
//											Out_TimeLong =3;
//										}
										break;
										case 5:
										//浓度下限
										con_below = (buff[7] <<8) + buff[8];
										//con_below = con_below*10;
										break;
										case 6:
										//光照上限
										light_top = (buff[7] <<8) + buff[8];
										//light_top = light_top*10;
										break;
										case 7:
										//光照下限
										light_below = (buff[7] <<8) + buff[8];
										//light_below =light_below*10;
										break;
										case 8:
										//风速上限
										wind_top = (buff[7] <<8) + buff[8];
										//wind_top =wind_top*10;
										break;
										case 9:
											//风速下限
										 wind_below = (buff[7] <<8) + buff[8];
										//wind_below = wind_below*10;
										break;
										case 10:
										//压力上限
										pre_top = (buff[7] <<8) + buff[8];
										
										break;
										case 11:
										//压力下限
										pre_below = (buff[7] <<8) + buff[8];
										break;
										case 12:
										//重量上限
										weight_top = (buff[7] <<8) + buff[8];

										break;
										case 13:
										//重量下限
										weight_below = (buff[7] <<8) + buff[8];
										break;
										
									}
								
								}
								
								
								
								if((buff[5] == 0x70)||(buff[5] == 0x72)||(buff[5] == 0x74))//修改系统密码
								{		
									
										EE_ReadVariable(VirtAddVarTab[30], &tmp);
										EE_ReadVariable(VirtAddVarTab[31], &tmp1);
										fuck = ((buff[7] << 24) + (buff[8] << 16) + (buff[9] << 8) +buff[10]);
										if(buff[5] == 0x70)
										{
											
												fuck = ((buff[7] << 24) + (buff[8] << 16) + (buff[9] << 8) +buff[10]);
												if(((tmp<<16 )+ tmp1) == fuck)
												{
														log_flag[3] = 0x80;
														log_flag[7] = 1;
														chg_passwd = chg_passwd + 1;
//														for(j = 0;j<8;j++)//识别密码
//														{
//																USART_SendData(USART1,log_flag[j]);//打开指示灯
//																while(USART_GetFlagStatus(USART1, USART_FLAG_TC)==RESET);
//														}		
												}
												
										}
										if(buff[5] == 0x72)
										{
												new_passwd = ((buff[7] << 24) + (buff[8] << 16) + (buff[9] << 8) +buff[10]);fuck = ((buff[7] << 24) + (buff[8] << 16) + (buff[9] << 8) +buff[10]);
												chg_passwd = chg_passwd + 1;
										}
										if(buff[5] == 0x74)
										{
												new_passwd1 = ((buff[7] << 24) + (buff[8] << 16) + (buff[9] << 8) +buff[10]);fuck = ((buff[7] << 24) + (buff[8] << 16) + (buff[9] << 8) +buff[10]);
												chg_passwd = chg_passwd + 1;
										}
										
								}
								if(buff[5] == 0x7c)//点击确定
								{
									
									if(chg_passwd == 3)//存储密码
									{
										
											if(new_passwd == new_passwd1)
											{
												
													EE_WriteVariable(VirtAddVarTab[30],(new_passwd>>16)&0xFFFF);//登陆密码
													EE_WriteVariable(VirtAddVarTab[31], (new_passwd)&0xFFFF);
													chg_passwd = 0;
												  //确定指示等变色
													for(j = 0;j<7;j++)//识别密码
													{
														
														  USART_SendData(USART1,hehe[j]);//切换屏幕到页面1
														  while(USART_GetFlagStatus(USART1, USART_FLAG_TC)==RESET);
														
													}							
												
											}
											else
											{
													chg_passwd = 0;	
											}
									}
									else
									{
												chg_passwd = 0;	
									}
									
							}
								
								if((buff[5] == 0x76)||(buff[5] == 0x78)||(buff[5] == 0x7A))//修改入口阀密码
								{		
										
										if(buff[5] == 0x76)
										{
											
												EE_ReadVariable(VirtAddVarTab[33], &tmp);
												EE_ReadVariable(VirtAddVarTab[34], &tmp1);
												fuck = ((buff[7] << 24) + (buff[8] << 16) + (buff[9] << 8) +buff[10]);
												if(((tmp<<16 )+ tmp1) == fuck)
												{
													
														chg_passwd1 = chg_passwd1 + 1;
														//密码指示灯亮
															log_flag[3] = 0x82;
															log_flag[7] = 1;
															for(j = 0;j<8;j++)//识别密码
															{
																
																USART_SendData(USART1,log_flag[j]);//打开指示灯
																while(USART_GetFlagStatus(USART1, USART_FLAG_TC)==RESET);
																
															}		
															
												}
												
										}
										if(buff[5] == 0x78)
										{
											
												new_paswd = ((buff[7] << 24) + (buff[8] << 16) + (buff[9] << 8) +buff[10]);fuck = ((buff[7] << 24) + (buff[8] << 16) + (buff[9] << 8) +buff[10]);
												chg_passwd1 = chg_passwd1 + 1;
											
										}
										if(buff[5] == 0x7A)
										{
												new_paswd1 = ((buff[7] << 24) + (buff[8] << 16) + (buff[9] << 8) +buff[10]);fuck = ((buff[7] << 24) + (buff[8] << 16) + (buff[9] << 8) +buff[10]);
												chg_passwd1 = chg_passwd1 + 1;
										}
										
								}
								if(buff[5] == 0x7D)//点击确定
								{
									
									if(chg_passwd1 == 3)//存储密码
									{
										
											if(new_paswd == new_paswd1)
											{
												
													EE_WriteVariable(VirtAddVarTab[33],(new_paswd>>16)&0xFFFF);//登陆密码
													EE_WriteVariable(VirtAddVarTab[34], (new_paswd)&0xFFFF);
													chg_passwd1 = 0;
												  //确定指示灯变色
													for(j = 0;j<7;j++)//识别密码
													{
														
														  USART_SendData(USART1,hehe[j]);//切换屏幕到页面1
														  while(USART_GetFlagStatus(USART1, USART_FLAG_TC)==RESET);
														
													}							
												
											}
											else
											{
													chg_passwd1 = 0;	
											}
											
									}
									else
									{
												chg_passwd1 = 0;	
									}
									
							}
								
							if((buff[5] == 0x55) && (buff[2] == 6))// 出口阀打开
							{
									//打开出口阀

									Liq_Out_on();
									Solenoid =1;
								
									valve_light[5] = 0x51;
									valve_light[7] = 1;
									for(j = 0;j<8;j++)//识别密码
									{
											USART_SendData(USART1,valve_light[j]);//打开指示灯
											while(USART_GetFlagStatus(USART1, USART_FLAG_TC)==RESET);
									}		
									
									
							}
							if((buff[5] == 0x56) && (buff[2] == 6))// 关闭出口阀
							{
								
									//关闭出口阀	

									Liq_Out_off();
									Solenoid =0;						//出口阀标志
									
									valve_light[5] = 0x51;
									valve_light[7] = 0;
									for(j = 0;j<8;j++)//识别密码
									{
										
											USART_SendData(USART1,valve_light[j]);//打开指示灯
											while(USART_GetFlagStatus(USART1, USART_FLAG_TC)==RESET);
										
									}		
									
							}	
							
							
							
							
						
					}
						
					
		
		}

}

extern char  date[6];
extern CO2_data_Type co2;
extern char data_rx_buf[9];
extern uint8_t Freq_433_Slave_Recv_Flag;
extern uint8_t Freq_433_Slave_data_Flag;
extern uint8_t Recv_client_flag;
int cal_crc_false(unsigned char * dat, int count);//??crc

extern u8 client_buff[32];
extern uint8_t Menu;
void UART5_IRQHandler(void)										//433无线通信中断
{
   // static char state;
		u16 crc;
		static u8 state=0;
		if(USART_GetITStatus(UART5, USART_IT_RXNE) != RESET)
    {
			//USART_ClearITPendingBit(UART5,USART_IT_RXNE);
			IWDG_ReloadCounter();
			client_buff[state] = Recv5_data();
//			if(client_buff[0] != '@' )
//			{
//						state = 0;
//						return ;
//			}
			state++;
			if((client_buff[0] != '@' )&&(client_buff[1] != '@')&&(state != 0))
			{
						state = 0;
						return ;
			}		
			
			if(state > 26)
			{
					
					crc = (client_buff[26] << 8) + client_buff[25];
					if(crc == cal_crc_false(client_buff,PACK_LEN))
					{		
							Recv_client_flag = 1;
							//Menu=Disp_status;
					}
					else
					{
							memset(client_buff,0,sizeof(client_buff));
							//Menu=HEAD_PAGE;
					}
					state = 0;
			
			}
		
	  }
}

extern unsigned int TimingDelay;
void SysTick_Handler(void)
{
	if(TimingDelay != 0)
	{
	    TimingDelay--;
	}
}

/**
   * @brief  This function handles RTC Alarm interrupt request.
   * @param  None
   * @retval None
   */
void RTCAlarm_IRQHandler(void)
{
    if(RTC_GetITStatus(RTC_IT_ALR) != RESET)
    {

        /* Clear EXTI line17 pending bit */
        EXTI_ClearITPendingBit(EXTI_Line17);
 
        /* Check if the Wake-Up flag is set */
        if(PWR_GetFlagStatus(PWR_FLAG_WU) != RESET)
        {
            /* Clear Wake Up flag */
            PWR_ClearFlag(PWR_FLAG_WU);
        }
        
        /* Wait until last write operation on RTC registers has finished */
        RTC_WaitForLastTask();   
        /* Clear RTC Alarm interrupt pending bit */
        RTC_ClearITPendingBit(RTC_IT_ALR);
        /* Wait until last write operation on RTC registers has finished */
        RTC_WaitForLastTask();
    }
}

extern uint8_t  LCD_Task;
extern uint32_t KEY_Waittime;
extern uint8_t KEY_Wakeup_Flag;
extern uint8_t Delay_10s;
extern uint8_t Load_Flag;
extern u8 wind_pulse_flag;
void uart_hostshow(void);//发送主机采集的数据

extern u8 DISP_Receive;
extern void SlavetoMaster_Receive(void);
u8 th_data_fresh_time = 0;
u8 TH_DATA_FRESH_TIME_FLAG = 0;
void TIM2_IRQHandler(void)
{
    if(TIM_GetITStatus(TIM2, TIM_IT_Update) != RESET)
	{
	    TIM_ClearITPendingBit(TIM2 , TIM_FLAG_Update);
		
        if(th_data_fresh_time > TH_DATA_FRESH_TIME)
        {
           TH_DATA_FRESH_TIME_FLAG = 1;
            th_data_fresh_time = 0;
        }
        else
        {
            th_data_fresh_time ++;
        }
		/* Reload IWDG counter */
		IWDG_ReloadCounter(); 
        
		SlavetoMaster_Receive();
		uart_hostshow();				//串口显示部分
		//SlavetoMaster_Receive();
	}
}


//extern u8 Time_get_flag;
//extern u16 Open_time_counter;

//void TIM3_IRQHandler(void)
//{
//  if(TIM_GetITStatus(TIM3, TIM_IT_Update) != RESET)
//	{
//	    TIM_ClearITPendingBit(TIM3 , TIM_FLAG_Update);

//		/* Reload IWDG counter */
//		IWDG_ReloadCounter();
//		
//		if(!Time_get_flag)
//		Time_get_flag =1;	 
//	}
//}
extern u8 Outtime_Hour[3];					//释放预设时间小时
extern u8 Outtime_Min[3];					//分钟
extern void MastertoSlave_Send(u16 Slave_Id);
extern u8 Open_flag;
extern u8 Lock_flag;
extern u8 open_status_update;					//OPEN状态变化标识
extern u8 Time_Enable;
void TIM4_IRQHandler(void)
{
  static u8 open_num =0;
	static u8 lock_num =0;
	static u8 lock_Counter=0;
	static u8 Disp_Counter=0;
	static u8 Disp_num=0;
	if(TIM_GetITStatus(TIM4, TIM_IT_Update) != RESET)
	{
	    TIM_ClearITPendingBit(TIM4 , TIM_FLAG_Update);
		/* Reload IWDG counter */
		IWDG_ReloadCounter();
		if(Open_flag_Auto)
		{		
			
			if(open_num<CLIENT_NUM)
			{
				if(Time_Enable== 0)
				{
					if(!(((RTC_Hour*60+RTC_Min)-(Outtime_Hour[0]*60+Outtime_Min[0]))>12))
					{
						if(open_num == 1 || open_num == 5 || open_num == 7)
						{
							Send_Cmd[0] = 'O';
							Send_Cmd[1] = 'P';
							Send_Cmd[2] = 'E';
							Send_Cmd[3] = 'N';
							MastertoSlave_Send(open_num);
						}
						else
						{
							Send_Cmd[0] = 'L';
							Send_Cmd[1] = 'O';
							Send_Cmd[2] = 'C';
							Send_Cmd[3] = 'K';
							MastertoSlave_Send(open_num);
						}
					}
					else if(((RTC_Hour*60+RTC_Min)-(Outtime_Hour[0]*60+Outtime_Min[0]))>12 && (!(((RTC_Hour*60+RTC_Min)-(Outtime_Hour[0]*60+Outtime_Min[0]))>24)))
					{
						if(open_num == 4)
						{
							Send_Cmd[0] = 'O';
							Send_Cmd[1] = 'P';
							Send_Cmd[2] = 'E';
							Send_Cmd[3] = 'N';
							MastertoSlave_Send(open_num);
						}
						else
						{
							Send_Cmd[0] = 'L';
							Send_Cmd[1] = 'O';
							Send_Cmd[2] = 'C';
							Send_Cmd[3] = 'K';
							MastertoSlave_Send(open_num);
						}
					}
					else if(((RTC_Hour*60+RTC_Min)-(Outtime_Hour[0]*60+Outtime_Min[0]))>24 && (!(((RTC_Hour*60+RTC_Min)-(Outtime_Hour[0]*60+Outtime_Min[0]))>36)))
					{
						if(open_num == 3)
						{
							Send_Cmd[0] = 'O';
							Send_Cmd[1] = 'P';
							Send_Cmd[2] = 'E';
							Send_Cmd[3] = 'N';
							MastertoSlave_Send(open_num);
						}
						else
						{
							Send_Cmd[0] = 'L';
							Send_Cmd[1] = 'O';
							Send_Cmd[2] = 'C';
							Send_Cmd[3] = 'K';
							MastertoSlave_Send(open_num);
						}
					}
					else if(((RTC_Hour*60+RTC_Min)-(Outtime_Hour[0]*60+Outtime_Min[0]))>36 && (!(((RTC_Hour*60+RTC_Min)-(Outtime_Hour[0]*60+Outtime_Min[0]))>48)))
					{
						if(open_num == 2)
						{
							Send_Cmd[0] = 'O';
							Send_Cmd[1] = 'P';
							Send_Cmd[2] = 'E';
							Send_Cmd[3] = 'N';
							MastertoSlave_Send(open_num);
						}
						else
						{
							Send_Cmd[0] = 'L';
							Send_Cmd[1] = 'O';
							Send_Cmd[2] = 'C';
							Send_Cmd[3] = 'K';
							MastertoSlave_Send(open_num);
						}
					}
					else if(((RTC_Hour*60+RTC_Min)-(Outtime_Hour[0]*60+Outtime_Min[0]))>48 && (!(((RTC_Hour*60+RTC_Min)-(Outtime_Hour[0]*60+Outtime_Min[0]))>60)))
					{
						if(open_num == 9)
						{
							Send_Cmd[0] = 'O';
							Send_Cmd[1] = 'P';
							Send_Cmd[2] = 'E';
							Send_Cmd[3] = 'N';
							MastertoSlave_Send(open_num);
						}
						else
						{
							Send_Cmd[0] = 'L';
							Send_Cmd[1] = 'O';
							Send_Cmd[2] = 'C';
							Send_Cmd[3] = 'K';
							MastertoSlave_Send(open_num);
						}
					}
					else if(((RTC_Hour*60+RTC_Min)-(Outtime_Hour[0]*60+Outtime_Min[0]))>60 && (!(((RTC_Hour*60+RTC_Min)-(Outtime_Hour[0]*60+Outtime_Min[0]))>72)))
					{
						if(open_num == 8)
						{
							Send_Cmd[0] = 'O';
							Send_Cmd[1] = 'P';
							Send_Cmd[2] = 'E';
							Send_Cmd[3] = 'N';
							MastertoSlave_Send(open_num);
						}
						else
						{
							Send_Cmd[0] = 'L';
							Send_Cmd[1] = 'O';
							Send_Cmd[2] = 'C';
							Send_Cmd[3] = 'K';
							MastertoSlave_Send(open_num);
						}
					}
					else if(((RTC_Hour*60+RTC_Min)-(Outtime_Hour[0]*60+Outtime_Min[0]))>72 && (!(((RTC_Hour*60+RTC_Min)-(Outtime_Hour[0]*60+Outtime_Min[0]))>84)))
					{
						if(open_num == 0 || open_num == 6)
						{
							Send_Cmd[0] = 'O';
							Send_Cmd[1] = 'P';
							Send_Cmd[2] = 'E';
							Send_Cmd[3] = 'N';
							MastertoSlave_Send(open_num);
						}
						else
						{
							Send_Cmd[0] = 'L';
							Send_Cmd[1] = 'O';
							Send_Cmd[2] = 'C';
							Send_Cmd[3] = 'K';
							MastertoSlave_Send(open_num);
						}
					}
					else if(((RTC_Hour*60+RTC_Min)-(Outtime_Hour[0]*60+Outtime_Min[0]))>84 && (!(((RTC_Hour*60+RTC_Min)-(Outtime_Hour[0]*60+Outtime_Min[0]))>96)))
					{
						if(open_num == 1 || open_num == 5 || open_num == 7)
						{
							Send_Cmd[0] = 'O';
							Send_Cmd[1] = 'P';
							Send_Cmd[2] = 'E';
							Send_Cmd[3] = 'N';
							MastertoSlave_Send(open_num);
						}
						else
						{
							Send_Cmd[0] = 'L';
							Send_Cmd[1] = 'O';
							Send_Cmd[2] = 'C';
							Send_Cmd[3] = 'K';
							MastertoSlave_Send(open_num);
						}
					}
					else if(((RTC_Hour*60+RTC_Min)-(Outtime_Hour[0]*60+Outtime_Min[0]))>96 && (!(((RTC_Hour*60+RTC_Min)-(Outtime_Hour[0]*60+Outtime_Min[0]))>108)))
					{
						if(open_num == 4)
						{
							Send_Cmd[0] = 'O';
							Send_Cmd[1] = 'P';
							Send_Cmd[2] = 'E';
							Send_Cmd[3] = 'N';
							MastertoSlave_Send(open_num);
						}
						else
						{
							Send_Cmd[0] = 'L';
							Send_Cmd[1] = 'O';
							Send_Cmd[2] = 'C';
							Send_Cmd[3] = 'K';
							MastertoSlave_Send(open_num);
						}
					}
					else if(((RTC_Hour*60+RTC_Min)-(Outtime_Hour[0]*60+Outtime_Min[0]))>108 && (!(((RTC_Hour*60+RTC_Min)-(Outtime_Hour[0]*60+Outtime_Min[0]))>120)))
					{
						if(open_num == 3)
						{
							Send_Cmd[0] = 'O';
							Send_Cmd[1] = 'P';
							Send_Cmd[2] = 'E';
							Send_Cmd[3] = 'N';
							MastertoSlave_Send(open_num);
						}
						else
						{
							Send_Cmd[0] = 'L';
							Send_Cmd[1] = 'O';
							Send_Cmd[2] = 'C';
							Send_Cmd[3] = 'K';
							MastertoSlave_Send(open_num);
						}
					}
					else if(((RTC_Hour*60+RTC_Min)-(Outtime_Hour[0]*60+Outtime_Min[0]))>120 && (!(((RTC_Hour*60+RTC_Min)-(Outtime_Hour[0]*60+Outtime_Min[0]))>132)))
					{
						if(open_num == 2)
						{
							Send_Cmd[0] = 'O';
							Send_Cmd[1] = 'P';
							Send_Cmd[2] = 'E';
							Send_Cmd[3] = 'N';
							MastertoSlave_Send(open_num);
						}
						else
						{
							Send_Cmd[0] = 'L';
							Send_Cmd[1] = 'O';
							Send_Cmd[2] = 'C';
							Send_Cmd[3] = 'K';
							MastertoSlave_Send(open_num);
						}
					}
					else if(((RTC_Hour*60+RTC_Min)-(Outtime_Hour[0]*60+Outtime_Min[0]))>132 && (!(((RTC_Hour*60+RTC_Min)-(Outtime_Hour[0]*60+Outtime_Min[0]))>144)))
					{
						if(open_num == 9)
						{
							Send_Cmd[0] = 'O';
							Send_Cmd[1] = 'P';
							Send_Cmd[2] = 'E';
							Send_Cmd[3] = 'N';
							MastertoSlave_Send(open_num);
						}
						else
						{
							Send_Cmd[0] = 'L';
							Send_Cmd[1] = 'O';
							Send_Cmd[2] = 'C';
							Send_Cmd[3] = 'K';
							MastertoSlave_Send(open_num);
						}
					}
					else if(((RTC_Hour*60+RTC_Min)-(Outtime_Hour[0]*60+Outtime_Min[0]))>144 && (!(((RTC_Hour*60+RTC_Min)-(Outtime_Hour[0]*60+Outtime_Min[0]))>156)))
					{
						if(open_num == 8)
						{
							Send_Cmd[0] = 'O';
							Send_Cmd[1] = 'P';
							Send_Cmd[2] = 'E';
							Send_Cmd[3] = 'N';
							MastertoSlave_Send(open_num);
						}
						else
						{
							Send_Cmd[0] = 'L';
							Send_Cmd[1] = 'O';
							Send_Cmd[2] = 'C';
							Send_Cmd[3] = 'K';
							MastertoSlave_Send(open_num);
						}
					}
					else if(((RTC_Hour*60+RTC_Min)-(Outtime_Hour[0]*60+Outtime_Min[0]))>156 && (!(((RTC_Hour*60+RTC_Min)-(Outtime_Hour[0]*60+Outtime_Min[0]))>168)))
					{
						if(open_num == 0 || open_num == 6)
						{
							Send_Cmd[0] = 'O';
							Send_Cmd[1] = 'P';
							Send_Cmd[2] = 'E';
							Send_Cmd[3] = 'N';
							MastertoSlave_Send(open_num);
						}
						else
						{
							Send_Cmd[0] = 'L';
							Send_Cmd[1] = 'O';
							Send_Cmd[2] = 'C';
							Send_Cmd[3] = 'K';
							MastertoSlave_Send(open_num);
						}
					}
				}
				else if(Time_Enable== 1)
				{
					if(!(((RTC_Hour*60+RTC_Min)-(Outtime_Hour[1]*60+Outtime_Min[1]))>12))
					{
						if(open_num == 1 || open_num == 5 || open_num == 7)
						{
							Send_Cmd[0] = 'O';
							Send_Cmd[1] = 'P';
							Send_Cmd[2] = 'E';
							Send_Cmd[3] = 'N';
							MastertoSlave_Send(open_num);
						}
						else
						{
							Send_Cmd[0] = 'L';
							Send_Cmd[1] = 'O';
							Send_Cmd[2] = 'C';
							Send_Cmd[3] = 'K';
							MastertoSlave_Send(open_num);
						}
					}
					else if(((RTC_Hour*60+RTC_Min)-(Outtime_Hour[1]*60+Outtime_Min[1]))>12 && (!(((RTC_Hour*60+RTC_Min)-(Outtime_Hour[1]*60+Outtime_Min[1]))>24)))
					{
						if(open_num == 4)
						{
							Send_Cmd[0] = 'O';
							Send_Cmd[1] = 'P';
							Send_Cmd[2] = 'E';
							Send_Cmd[3] = 'N';
							MastertoSlave_Send(open_num);
						}
						else
						{
							Send_Cmd[0] = 'L';
							Send_Cmd[1] = 'O';
							Send_Cmd[2] = 'C';
							Send_Cmd[3] = 'K';
							MastertoSlave_Send(open_num);
						}
					}
					else if(((RTC_Hour*60+RTC_Min)-(Outtime_Hour[1]*60+Outtime_Min[1]))>24 && (!(((RTC_Hour*60+RTC_Min)-(Outtime_Hour[1]*60+Outtime_Min[1]))>36)))
					{
						if(open_num == 3)
						{
							Send_Cmd[0] = 'O';
							Send_Cmd[1] = 'P';
							Send_Cmd[2] = 'E';
							Send_Cmd[3] = 'N';
							MastertoSlave_Send(open_num);
						}
						else
						{
							Send_Cmd[0] = 'L';
							Send_Cmd[1] = 'O';
							Send_Cmd[2] = 'C';
							Send_Cmd[3] = 'K';
							MastertoSlave_Send(open_num);
						}
					}
					else if(((RTC_Hour*60+RTC_Min)-(Outtime_Hour[1]*60+Outtime_Min[1]))>36 && (!(((RTC_Hour*60+RTC_Min)-(Outtime_Hour[1]*60+Outtime_Min[1]))>48)))
					{
						if(open_num == 2)
						{
							Send_Cmd[0] = 'O';
							Send_Cmd[1] = 'P';
							Send_Cmd[2] = 'E';
							Send_Cmd[3] = 'N';
							MastertoSlave_Send(open_num);
						}
						else
						{
							Send_Cmd[0] = 'L';
							Send_Cmd[1] = 'O';
							Send_Cmd[2] = 'C';
							Send_Cmd[3] = 'K';
							MastertoSlave_Send(open_num);
						}
					}
					else if(((RTC_Hour*60+RTC_Min)-(Outtime_Hour[1]*60+Outtime_Min[1]))>48 && (!(((RTC_Hour*60+RTC_Min)-(Outtime_Hour[1]*60+Outtime_Min[1]))>60)))
					{
						if(open_num == 9)
						{
							Send_Cmd[0] = 'O';
							Send_Cmd[1] = 'P';
							Send_Cmd[2] = 'E';
							Send_Cmd[3] = 'N';
							MastertoSlave_Send(open_num);
						}
						else
						{
							Send_Cmd[0] = 'L';
							Send_Cmd[1] = 'O';
							Send_Cmd[2] = 'C';
							Send_Cmd[3] = 'K';
							MastertoSlave_Send(open_num);
						}
					}
					else if(((RTC_Hour*60+RTC_Min)-(Outtime_Hour[1]*60+Outtime_Min[1]))>60 && (!(((RTC_Hour*60+RTC_Min)-(Outtime_Hour[1]*60+Outtime_Min[1]))>72)))
					{
						if(open_num == 8)
						{
							Send_Cmd[0] = 'O';
							Send_Cmd[1] = 'P';
							Send_Cmd[2] = 'E';
							Send_Cmd[3] = 'N';
							MastertoSlave_Send(open_num);
						}
						else
						{
							Send_Cmd[0] = 'L';
							Send_Cmd[1] = 'O';
							Send_Cmd[2] = 'C';
							Send_Cmd[3] = 'K';
							MastertoSlave_Send(open_num);
						}
					}
					else if(((RTC_Hour*60+RTC_Min)-(Outtime_Hour[1]*60+Outtime_Min[1]))>72 && (!(((RTC_Hour*60+RTC_Min)-(Outtime_Hour[1]*60+Outtime_Min[1]))>84)))
					{
						if(open_num == 0 || open_num == 6)
						{
							Send_Cmd[0] = 'O';
							Send_Cmd[1] = 'P';
							Send_Cmd[2] = 'E';
							Send_Cmd[3] = 'N';
							MastertoSlave_Send(open_num);
						}
						else
						{
							Send_Cmd[0] = 'L';
							Send_Cmd[1] = 'O';
							Send_Cmd[2] = 'C';
							Send_Cmd[3] = 'K';
							MastertoSlave_Send(open_num);
						}
					}
					else if(((RTC_Hour*60+RTC_Min)-(Outtime_Hour[1]*60+Outtime_Min[1]))>84 && (!(((RTC_Hour*60+RTC_Min)-(Outtime_Hour[1]*60+Outtime_Min[1]))>96)))
					{
						if(open_num == 1 || open_num == 5 || open_num == 7)
						{
							Send_Cmd[0] = 'O';
							Send_Cmd[1] = 'P';
							Send_Cmd[2] = 'E';
							Send_Cmd[3] = 'N';
							MastertoSlave_Send(open_num);
						}
						else
						{
							Send_Cmd[0] = 'L';
							Send_Cmd[1] = 'O';
							Send_Cmd[2] = 'C';
							Send_Cmd[3] = 'K';
							MastertoSlave_Send(open_num);
						}
					}
					else if(((RTC_Hour*60+RTC_Min)-(Outtime_Hour[1]*60+Outtime_Min[1]))>96 && (!(((RTC_Hour*60+RTC_Min)-(Outtime_Hour[1]*60+Outtime_Min[1]))>108)))
					{
						if(open_num == 4)
						{
							Send_Cmd[0] = 'O';
							Send_Cmd[1] = 'P';
							Send_Cmd[2] = 'E';
							Send_Cmd[3] = 'N';
							MastertoSlave_Send(open_num);
						}
						else
						{
							Send_Cmd[0] = 'L';
							Send_Cmd[1] = 'O';
							Send_Cmd[2] = 'C';
							Send_Cmd[3] = 'K';
							MastertoSlave_Send(open_num);
						}
					}
					else if(((RTC_Hour*60+RTC_Min)-(Outtime_Hour[1]*60+Outtime_Min[1]))>108 && (!(((RTC_Hour*60+RTC_Min)-(Outtime_Hour[1]*60+Outtime_Min[1]))>120)))
					{
						if(open_num == 3)
						{
							Send_Cmd[0] = 'O';
							Send_Cmd[1] = 'P';
							Send_Cmd[2] = 'E';
							Send_Cmd[3] = 'N';
							MastertoSlave_Send(open_num);
						}
						else
						{
							Send_Cmd[0] = 'L';
							Send_Cmd[1] = 'O';
							Send_Cmd[2] = 'C';
							Send_Cmd[3] = 'K';
							MastertoSlave_Send(open_num);
						}
					}
					else if(((RTC_Hour*60+RTC_Min)-(Outtime_Hour[1]*60+Outtime_Min[1]))>120 && (!(((RTC_Hour*60+RTC_Min)-(Outtime_Hour[1]*60+Outtime_Min[1]))>132)))
					{
						if(open_num == 2)
						{
							Send_Cmd[0] = 'O';
							Send_Cmd[1] = 'P';
							Send_Cmd[2] = 'E';
							Send_Cmd[3] = 'N';
							MastertoSlave_Send(open_num);
						}
						else
						{
							Send_Cmd[0] = 'L';
							Send_Cmd[1] = 'O';
							Send_Cmd[2] = 'C';
							Send_Cmd[3] = 'K';
							MastertoSlave_Send(open_num);
						}
					}
					else if(((RTC_Hour*60+RTC_Min)-(Outtime_Hour[1]*60+Outtime_Min[1]))>132 && (!(((RTC_Hour*60+RTC_Min)-(Outtime_Hour[1]*60+Outtime_Min[1]))>144)))
					{
						if(open_num == 9)
						{
							Send_Cmd[0] = 'O';
							Send_Cmd[1] = 'P';
							Send_Cmd[2] = 'E';
							Send_Cmd[3] = 'N';
							MastertoSlave_Send(open_num);
						}
						else
						{
							Send_Cmd[0] = 'L';
							Send_Cmd[1] = 'O';
							Send_Cmd[2] = 'C';
							Send_Cmd[3] = 'K';
							MastertoSlave_Send(open_num);
						}
					}
					else if(((RTC_Hour*60+RTC_Min)-(Outtime_Hour[1]*60+Outtime_Min[1]))>144 && (!(((RTC_Hour*60+RTC_Min)-(Outtime_Hour[1]*60+Outtime_Min[1]))>156)))
					{
						if(open_num == 8)
						{
							Send_Cmd[0] = 'O';
							Send_Cmd[1] = 'P';
							Send_Cmd[2] = 'E';
							Send_Cmd[3] = 'N';
							MastertoSlave_Send(open_num);
						}
						else
						{
							Send_Cmd[0] = 'L';
							Send_Cmd[1] = 'O';
							Send_Cmd[2] = 'C';
							Send_Cmd[3] = 'K';
							MastertoSlave_Send(open_num);
						}
					}
					else if(((RTC_Hour*60+RTC_Min)-(Outtime_Hour[1]*60+Outtime_Min[1]))>156 && (!(((RTC_Hour*60+RTC_Min)-(Outtime_Hour[1]*60+Outtime_Min[1]))>168)))
					{
						if(open_num == 0 || open_num == 6)
						{
							Send_Cmd[0] = 'O';
							Send_Cmd[1] = 'P';
							Send_Cmd[2] = 'E';
							Send_Cmd[3] = 'N';
							MastertoSlave_Send(open_num);
						}
						else
						{
							Send_Cmd[0] = 'L';
							Send_Cmd[1] = 'O';
							Send_Cmd[2] = 'C';
							Send_Cmd[3] = 'K';
							MastertoSlave_Send(open_num);
						}
					}
				}
//				if(((RTC_Hour*60+RTC_Min)-(Outtime_Hour[0]*60+Outtime_Min[0]))<60 
//			  ||(( RTC_Hour*60+RTC_Min)-(Outtime_Hour[1]*60+Outtime_Min[1]))<60
//			  ||(( RTC_Hour*60+RTC_Min)-(Outtime_Hour[2]*60+Outtime_Min[2]))<60 )
//				{
//					if(open_num == 0 || open_num == 1 || open_num== 5 || open_num == 7)
//					{
//						Send_Cmd[0] = 'O';
//						Send_Cmd[1] = 'P';
//						Send_Cmd[2] = 'E';
//						Send_Cmd[3] = 'N';
//						MastertoSlave_Send(open_num);
//					}
//					else
//					{
//						Send_Cmd[0] = 'L';
//						Send_Cmd[1] = 'O';
//						Send_Cmd[2] = 'C';
//						Send_Cmd[3] = 'K';
//						MastertoSlave_Send(open_num);
//					}
//				}
//				else if(((((RTC_Hour*60+RTC_Min)-(Outtime_Hour[0]*60+Outtime_Min[0]))>60) && (((RTC_Hour*60+RTC_Min)-(Outtime_Hour[0]*60+Outtime_Min[0]))<120))
//			        ||((((RTC_Hour*60+RTC_Min)-(Outtime_Hour[1]*60+Outtime_Min[1]))>60) && (((RTC_Hour*60+RTC_Min)-(Outtime_Hour[1]*60+Outtime_Min[1]))<120))
//			        ||((((RTC_Hour*60+RTC_Min)-(Outtime_Hour[2]*60+Outtime_Min[2]))>60) && (((RTC_Hour*60+RTC_Min)-(Outtime_Hour[2]*60+Outtime_Min[2]))<120)) )
//				{
//					if(open_num == 2 || open_num== 3 || open_num == 4 || open_num == 9)
//					{
//						Send_Cmd[0] = 'O';
//						Send_Cmd[1] = 'P';
//						Send_Cmd[2] = 'E';
//						Send_Cmd[3] = 'N';
//						MastertoSlave_Send(open_num);
//					}
//					else
//					{
//						Send_Cmd[0] = 'L';
//						Send_Cmd[1] = 'O';
//						Send_Cmd[2] = 'C';
//						Send_Cmd[3] = 'K';
//						MastertoSlave_Send(open_num);
//					}
//				}
//				else if(((((RTC_Hour*60+RTC_Min)-(Outtime_Hour[0]*60+Outtime_Min[0]))>120) && (((RTC_Hour*60+RTC_Min)-(Outtime_Hour[0]*60+Outtime_Min[0]))<180))
//			        ||((((RTC_Hour*60+RTC_Min)-(Outtime_Hour[1]*60+Outtime_Min[1]))>120) && (((RTC_Hour*60+RTC_Min)-(Outtime_Hour[1]*60+Outtime_Min[1]))<180))
//			        ||((((RTC_Hour*60+RTC_Min)-(Outtime_Hour[2]*60+Outtime_Min[2]))>120) && (((RTC_Hour*60+RTC_Min)-(Outtime_Hour[2]*60+Outtime_Min[2]))<180)))
//				{
//					if(open_num== 6 || open_num == 8)
//					{
//						Send_Cmd[0] = 'O';
//						Send_Cmd[1] = 'P';
//						Send_Cmd[2] = 'E';
//						Send_Cmd[3] = 'N';
//						MastertoSlave_Send(open_num);
//					}
//					else
//					{
//						Send_Cmd[0] = 'L';
//						Send_Cmd[1] = 'O';
//						Send_Cmd[2] = 'C';
//						Send_Cmd[3] = 'K';
//						MastertoSlave_Send(open_num);
//					}
//				}
			open_num++;
			}
			else
			{
				open_num=0;
			}
		}
		
		else if(Disp_flag_Manual || Disp_flag_Auto)//手动刷新	或自动刷新	
		{
			Send_Cmd[0] = 'D';
			Send_Cmd[1] = 'I';
			Send_Cmd[2] = 'S';
			Send_Cmd[3] = 'P';
			if(Disp_Counter<2)
			{
				if(Disp_num<CLIENT_NUM)
				{
				MastertoSlave_Send(Disp_num);
				Disp_num++;
				}
				else
				{
					Disp_num =0;
					Disp_Counter++;
				}
			}
			else
			{
				Disp_Counter=0;
				Disp_flag_Manual=0;
				Disp_flag_Auto=0;
				//TIM_Cmd(TIM4,DISABLE);
			}
			
		}
		
		else if(Lock_flag)
		{
			Send_Cmd[0] = 'L';
			Send_Cmd[1] = 'O';
			Send_Cmd[2] = 'C';
			Send_Cmd[3] = 'K';
			if(lock_Counter<2)
			{
				if(lock_num<CLIENT_NUM)
				{
					MastertoSlave_Send(lock_num);
					lock_num++;
				}
				else
				{
					lock_num =0;
					lock_Counter++;
				}
			}
			else
			{
			lock_Counter =0;
			//TIM_Cmd(TIM4,DISABLE);
			open_status_update =2;						//由OPEN状态转化为非OPEN状�		�
			Lock_flag=0;
			}
		}
		TIM_Cmd(TIM4,DISABLE);
	}
}

 /**
   * @brief  This function handles External lines 9 to 5 interrupt request.
   * @param  None
   * @retval None
   */




extern uint8_t Freq_433_Slave_Recv_Flag;
extern uint8_t Freq_433_Slave_data_Flag;
extern u16 wind_pulse_counter;
void EXTI15_10_IRQHandler(void)
{
		if(EXTI_GetITStatus(EXTI_Line10) != RESET)
    { 
			wind_pulse_counter++;
	    /* Clear the Key Button EXTI line pending bit */  
        EXTI_ClearITPendingBit(EXTI_Line10);
    }
}

int cal_crc_false(unsigned char * dat, int count)  //??crc
{

	int crc = 0xFFFF;
	unsigned char crctemp;
	int i = 0;
	while (count > 0)				   
	{
	
	    count -= 1;
	    crctemp =crc >> 8;
	    crc = (crc << 8) & 0xFFFF;
	    crc = (CRC16Table[crctemp ^ dat[i]] ^ crc) & 0xFFFF;
	    i += 1;
	
	}
	return crc;

}

u8 Recv5_data(void)
{

	   u8 Uart5_data;
	   while(USART_GetFlagStatus (UART5,USART_FLAG_RXNE) == RESET); //  ????????
	   Uart5_data =  USART_ReceiveData(UART5);
	   return Uart5_data;
 
}
u8 Recv1_data(void)
{

	   u8 Uart1_data;
	   while(USART_GetFlagStatus (USART1,USART_FLAG_RXNE) == RESET); //  ????????
	   Uart1_data =  USART_ReceiveData(USART1);
	   return Uart1_data;
 
}
