#include <stm32f10x.h>
#include "Delay.h"

extern uint32_t KEY_Waittime;

/*精确延时辅助变量*/
unsigned int TimingDelay;

void my_systeminit(void)
{
	unsigned char temp=0x00;
	FlagStatus Flag;
	ErrorStatus HSEStartUpstatus;
	RCC_DeInit();	//时钟默认初始化    
	RCC_HSEConfig(RCC_HSE_ON);	//开启外部高速时钟
	HSEStartUpstatus=RCC_WaitForHSEStartUp();
	while(HSEStartUpstatus!=SUCCESS);//等待外部时钟准备成功
	RCC_HCLKConfig(RCC_SYSCLK_Div1);//AHB时钟预分频系数为0，不分频，最高72MHz
	RCC_PCLK1Config(RCC_HCLK_Div2);//APB1不分频，最高36MHz
	RCC_PCLK2Config(RCC_HCLK_Div1);//APB2不分频，最高72MHz

	RCC_PLLConfig(RCC_PLLSource_HSE_Div1,RCC_PLLMul_9);//9倍频
	RCC_PLLCmd(ENABLE);
	Flag=RCC_GetFlagStatus(RCC_FLAG_PLLRDY);
	RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);
	FLASH_PrefetchBufferCmd(FLASH_PrefetchBuffer_Enable);
	FLASH_SetLatency(FLASH_Latency_2);
	while(Flag!=SET)Flag=RCC_GetFlagStatus(RCC_FLAG_PLLRDY);
	temp=RCC_GetSYSCLKSource();
	while(temp!=0x08)temp=RCC_GetSYSCLKSource();
	/*
	RCC_DeInit();//将外设 RCC寄存器重设为缺省值
 
  RCC_HSICmd(ENABLE);//使能HSI  
  while(RCC_GetFlagStatus(RCC_FLAG_HSIRDY) == RESET)//等待HSI使能成功
  {
  }
 
  if(1)
  {
    //FLASH_PrefetchBufferCmd(FLASH_PrefetchBuffer_Enable);
    //FLASH_SetLatency(FLASH_Latency_2);
   
    RCC_HCLKConfig(RCC_SYSCLK_Div1);   
    RCC_PCLK1Config(RCC_HCLK_Div2);
    RCC_PCLK2Config(RCC_HCLK_Div1);
    
    //设置 PLL 时钟源及倍频系数
    RCC_PLLConfig(RCC_PLLSource_HSI_Div2, RCC_PLLMul_2);                
    //使能或者失能 PLL,这个参数可以取：ENABLE或者DISABLE
    RCC_PLLCmd(ENABLE);//如果PLL被用于系统时钟,那么它不能被失能
    //等待指定的 RCC 标志位设置成功 等待PLL初始化成功
    while(RCC_GetFlagStatus(RCC_FLAG_PLLRDY) == RESET)
    {
    }
 
    //设置系统时钟（SYSCLK） 设置PLL为系统时钟源
    RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK);  
    //等待PLL成功用作于系统时钟的时钟源
    //  0x00：HSI 作为系统时钟 
    //  0x04：HSE作为系统时钟 
    //  0x08：PLL作为系统时钟  
    while(RCC_GetSYSCLKSource() != 0x08)
    {
    }
  }*/
}	


/*不准确延时nus*/
void Delay_us(unsigned int nus)
{
	unsigned int i;

	while(nus--)
	{
	    i = 10;
		while(i--);
	}
}
/*不准确延时nms*/
void Delay_ms(unsigned int nms)
{
	unsigned int i;

	while(nms--)
	{
	    i = 12000;
		while(i--);
	}
}

/**
  * @brief  Configures the SysTick to generate an interrupt each 1 millisecond.
  * @param  None
  * @retval None
  */
void SysTick_Configuration(void)
{
    /* Setup SysTick Timer for 1 msec interrupts  */
    if (SysTick_Config(SystemCoreClock / 1000))
    { 
        /* Capture error */ 
        while (1);
    }
    /* Set SysTick Priority to 3 */
    NVIC_SetPriority(SysTick_IRQn, 0x0C);
}
 
 /**
   * @brief  Inserts a delay time.
   * @param  nTime: specifies the delay time length, in milliseconds.
   * @retval None
   */
extern uint8_t KEY_Wakeup_Flag;
void Delay(unsigned int nTime)
{
	TimingDelay = nTime;	 
	while(TimingDelay != 0);
}

 /**风速传感器、串口屏显示中断**/
void Timer2_Init(void)
{
    TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	//NVIC_InitTypeDef NVIC_InitStructure;

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
	
    /* Time base configuration */
    TIM_TimeBaseStructure.TIM_Period = 222;
    TIM_TimeBaseStructure.TIM_Prescaler = 36000-1;
    TIM_TimeBaseStructure.TIM_ClockDivision = 0;
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
 
    TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure);

	TIM_ClearFlag(TIM2,TIM_FLAG_Update);
	TIM_ARRPreloadConfig(TIM2,DISABLE);
	TIM_ITConfig(TIM2,TIM_IT_Update,ENABLE);
	
	TIM_Cmd(TIM2,ENABLE);  

}

///*串口屏RTC读取中断*/
//void Timer3_Init(void)
//{
//    TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
//	//NVIC_InitTypeDef NVIC_InitStructure;

//	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);

//	

//    /* Time base configuration */
//    TIM_TimeBaseStructure.TIM_Period = 40000;					//20s
//    TIM_TimeBaseStructure.TIM_Prescaler = 36000-1;
//    TIM_TimeBaseStructure.TIM_ClockDivision = 0;
//    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
// 
//    TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);

//	TIM_ClearFlag(TIM3,TIM_FLAG_Update);
//	TIM_ARRPreloadConfig(TIM3,DISABLE);
//	TIM_ITConfig(TIM3,TIM_IT_Update,ENABLE);
//	
//	TIM_Cmd(TIM3,ENABLE);  
//}

void Timer4_Init(void)
{
    TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	//NVIC_InitTypeDef NVIC_InitStructure;

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);

    /* Time base configuration */
    TIM_TimeBaseStructure.TIM_Period = 2700;					//15s
    TIM_TimeBaseStructure.TIM_Prescaler = 36000-1;
    TIM_TimeBaseStructure.TIM_ClockDivision = 0;
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
 
    TIM_TimeBaseInit(TIM4, &TIM_TimeBaseStructure);

	TIM_ClearFlag(TIM4,TIM_FLAG_Update);
	TIM_ARRPreloadConfig(TIM4,DISABLE);
	TIM_ITConfig(TIM4,TIM_IT_Update,ENABLE);
	
	TIM_Cmd(TIM4,DISABLE);  
}

